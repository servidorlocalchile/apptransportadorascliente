import React from "react";
import {
  BrowserRouter,
  Switch
} from "react-router-dom";
import {connect} from 'react-redux';
import Home from './Pages/Home';
import SignUp from './Pages/Signup';
import SignUpSuper from './Pages/SignupSuper';
import SignIn from './Pages/SignIn';
import Dashboard from './PrivateRoutes/Dashboard';
import PrivateRoute from './PrivateRoutes/PrivateRoute';
import PublicRoute from './Pages/PublicRoute';
import AppBarUserInfo from './Molecules/AppBarUserInfo';
import Header from './Molecules/Header';
import CotizarEnvio from './PrivateRoutes/CotizarEnvio';
import ListadoGuias from './PrivateRoutes/ListadoGuias';
import RelacionGuias from './PrivateRoutes/RelacionGuias';
import CrearAgente from './PrivateRoutes/CrearAgente';
import Usuarios from './PrivateRoutes/Usuarios';
import DownloadApp from './Pages/DownloadApp';
import ComisionHorenvios from './PrivateRoutes/ComisionHorenvios';
import Notfound from './Pages/NotFound';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { makeStyles } from '@material-ui/core/styles';
import './App.css';
import SubirDocumentos from "./PrivateRoutes/SubirDocumentos";


const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#a0d468',
    },
  },
});

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  links : {
    color : 'white',
    textDecoration : 'none',
    "&:hover": {
      color: "gray",
  }
  }
}));
 function App(props) {
  const classes = useStyles();
  let routes = [];
  if(props.userInfo === undefined){}
  else if(props.userInfo.CuentaHabilitada && props.userInfo.CodigoSuperUsuario === undefined){
    routes = <div><PrivateRoute component={CotizarEnvio} path="/Cotizar Envio" exact />
    <PrivateRoute component={ListadoGuias} path="/Listado de Guías" exact />
    <PrivateRoute component={RelacionGuias} path="/Relación de Guías" exact />
    <PrivateRoute component={CrearAgente} path="/Crear Agente" exact /></div>;
} else if(props.userInfo.CuentaHabilitada && props.userInfo.CodigoSuperUsuario !== undefined){
  //se trata de un super usuario
  routes = <div><PrivateRoute component={CotizarEnvio} path="/Cotizar Envio" exact />
  <PrivateRoute component={ListadoGuias} path="/Listado de Guías" exact />
  <PrivateRoute component={RelacionGuias} path="/Relación de Guías" exact />
  <PrivateRoute component={CrearAgente} path="/Crear Agente" exact />
  <PrivateRoute component={Usuarios} path="/Usuarios" exact />
  <PrivateRoute component={ComisionHorenvios} path="/Editar Comisión" exact />
  </div>;
} 
else if(!props.userInfo.CuentaHabilitada) {
  routes = <PrivateRoute component={SubirDocumentos} path="/Subir Documentos" exact />;
  }
  return (
    <BrowserRouter>
        <ThemeProvider theme={theme}>
      <div className={classes.root}>
        {props.layout ? <AppBarUserInfo layout={props.layout} userInfo={props.userInfo} /> : null}
       
        <Header  layout={props.layout}   /> 
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
       <Switch>
          <PublicRoute restricted={false} component={Home} path="/" exact />
          <PublicRoute restricted={true} component={SignIn} path="/ingresar"  exact  />
          <PublicRoute restricted={true} component={SignUp} path="/registro" exact />
          <PublicRoute restricted={true} component={SignUpSuper} path="/registroSuperUsuario" exact />
          <PublicRoute restricted={false} component={DownloadApp} path="/app" exact />    
          <PrivateRoute component={Dashboard} path="/Mi cuenta" exact />
            {routes}
          <PublicRoute restricted={false} component={Notfound} />
        </Switch>
      </div>
      </ThemeProvider>
    </BrowserRouter>  
  );
}


function mapStateToProps(state) { 
  return {
     layout : state.changeLayout.hidePublicState,
     userInfo : state.setUserInfo.userInfo,
  };
};
export default connect(mapStateToProps)(App)
