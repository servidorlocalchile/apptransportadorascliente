 const changeLayout = (bool) => {
    return {
      type : 'change_layout',
      payload : bool
    }
  }
  const userInfo = (type, userInfo) => {
    return {
      type : type,
      payload : userInfo
    }
  }
  const crearGuias = (type, guiasInfo) => {
    return {
      type : type,
      payload : guiasInfo
    }
  }
  const dataInfo = (type, dataInfo) => {
    return {
      type : type,
      payload : dataInfo
    }
  }
 

  
  export {changeLayout, userInfo, crearGuias, dataInfo}