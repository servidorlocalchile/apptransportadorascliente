import { createStore } from "redux";
import {reducer} from "./reducers";
import {loadState, saveState} from './usePersistedState';

const initialData = loadState() || {}
const store = createStore(reducer,initialData,
window.devToolsExtension && window.devToolsExtension());
store.subscribe( function () {
    saveState(store.getState());
  })
  
export default store;