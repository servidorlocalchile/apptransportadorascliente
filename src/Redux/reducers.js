import { combineReducers } from 'redux'
//this is the initial redux state
const initialState = {
  hidePublicState : false,//usePersistedState('hidePublicState', false),
  userInfo : {},//usePersistedState('userInfo', {})
  crearGuias : [],
  dataInfo : []
}
let changeLayout = (state = initialState.hidePublicState, action) => {
  switch(action.type){
    case 'change_layout' :
    return Object.assign({}, state, {
        hidePublicState : action.payload
    });
    default :
    return state;
  }
}

let setUserInfo = (state = initialState.userInfo, action) => {
  switch(action.type){
    case 'set_user_info':
    return Object.assign({}, state, {
        userInfo : action.payload
    });
    case 'reset' : 
    return {
      userInfo : {}
    };

    default :
    return state;
  }
}
let setDataInfo = (state = initialState.dataInfo, action) => {
  switch(action.type){
    case 'set_data_info':
    return Object.assign({}, state, {
        dataInfo : action.payload
    });
    case 'reset' : 
    return {
      dataInfo : {}
    };

    default :
    return state;
  }
}
/*  
let setCrearGuias = (state = initialState, action) => {
  switch(action.type){
    case 'set_guias_info':
      console.log(state, state.crearGuias) 
      //en produccion state es un arreglo vacio y state.crearGuias es undefined
    return  {
      ...state, 
        crearGuias :  [...state.crearGuias, ...action.payload]//state.concat(action.payload)
    }
    case 'reset' :
      return {
        crearGuias : []
      };

    default :
    return state;
  }
}
*/
export const reducer = combineReducers({
  changeLayout, setUserInfo, setDataInfo
})