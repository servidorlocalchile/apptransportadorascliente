const dimensiones =   {
    "cantidad": 1,
    "largo": 26,
    "ancho": 23,
    "alto": 7,
    "peso": 2
  }

  const paqueteTerrestreCubicar = 222;
  const mercanciaTerrestreCubicar = 444;

  const calculoCubicar = (alto, largo, ancho, constante, division)=>{
   const resultado = (alto / division) * (largo / division) * (ancho / division) * constante;
   return resultado; 
  }

  //si el resultado es mayor a 8 kilos el envio debe hacerse por medio de mercancia terrestre (codigo 3 en API de envia)
  //si es menor a 8 kilos el envio debe hacerse por medio de Paquete Terrestre (codigo 12 en Api de envia)

  export default calculoCubicar;

