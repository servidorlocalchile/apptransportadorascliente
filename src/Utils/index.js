import {changeLayout, crearGuias, userInfo, dataInfo} from '../Redux/actions';
import store from '../Redux/store';
const TOKEN_KEY = 'jwt';
const STATE_KEY = 'state';

export const login = () => {
    localStorage.setItem(TOKEN_KEY, 'Login');
    store.dispatch(changeLayout(true))
}

export const logout = () => {
    //eliminar todos los datos del store
    store.dispatch(changeLayout())
    store.dispatch(userInfo('reset'))
    store.dispatch(crearGuias('reset'))
    store.dispatch(dataInfo('reset'))
        localStorage.removeItem(TOKEN_KEY);
        localStorage.removeItem(STATE_KEY);   
}

export const isLogin = () => {
    if (localStorage.getItem(TOKEN_KEY)) {
        return true;
    }

    return false;
}