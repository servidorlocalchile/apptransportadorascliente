 useEffect(()=>{
    //el efecto se activa al renderizar por primera vez, filtrar ese renderizado si el arreglo esta vacio
   
      //si ya termino de cotizar con aveonline y enviaResponseStatus es true, cotizar con envia
      const valorRecaudoData = Number.isNaN(parseInt(recaudo)) ? 0 : parseInt(recaudo);
      //cotizar con envia
        //si las cuidades se pudieron mapear para obtener el codigo dane
        //el codigo 478 es cuando el horenvios paga a credito, es decir no se cobra ningun valor contraentrega
        //el codigo 479 es cuando el reparto es con recaudo, es decir cuando se agrega valorRecaudo
       //valor asegurado no debe ser menor a 50 mil
         //si no selecciona ninguna opcion, codigo 4
        //si selecciona destinatario paga el flete unicamente codigo 6 (el destinatario paga el flete)
        //si seleccionla destinagario paga el flete y agrega valorrecaudo codigo 7 (contraentrega paga flete y totalrecaudo) o selecciona todo

      const codCuenta = valorRecaudoData === 0 ? "478" : "479";
      const enviaCotizacionBody = {
        "ciudad_origen": ciudadesDane.Origen,
        "ciudad_destino": ciudadesDane.Destino,
        "cod_formapago": 4,
        "cod_servicio": "12",
        "mca_nosabado": 1,
        "mca_docinternacional": 0,
        "cod_regional_cta": 17,
        "cod_oficina_cta": 1,
        "cod_cuenta": codCuenta,
        "con_cartaporte": "0",
        "info_cubicacion": [
            {
                "cantidad": 1,
                "largo": largo,
                "ancho": ancho,
                "alto": alto,
                "peso": kilos,
                "declarado": parseInt(seguro)
            }
        ],
        "info_origen": {
            "nom_remitente": "HECTOR FABIO ARISTIZABAL",
            "dir_remitente": "cra 10 # 57 - 61",
            "tel_remitente": "3045570384",
            "ced_remitente": ""
        },
        "info_destino": {
            "nom_destinatario": "LILIANA MOR",
            "dir_destinatario": "cr 48 150 a 40 Int 14 ap 104",
            "tel_destinatario": "3102834397",
            "ced_destinatario": ""
        },
        "info_contenido": {
            "dice_contener": "cosmeticos",
            "texto_guia": "",
            "accion_notaguia": "Productos cosméticos",
            "num_documentos": "",
            "centrocosto": ""
        },
        "numero_guia": ""
    }
    if(valorRecaudoData !== 0){
      enviaCotizacionBody.info_contenido.valorproducto = valorRecaudoData;
    }
      const enviaPromise = aveonline(process.env.REACT_APP_ENVIA_HTTPS_BASE_URL + '/ServicioLiquidacionREST/Service1.svc/Liquidacion/', enviaCotizacionBody)
      enviaPromise.then(resp=>{
        //envia puede responder de las siguientes formas
        //1  "respuesta": "", cuando la respuesta es exitosa
        //2    "respuesta": "Ciudad Destino sin Cubrimiento"
        //3  "respuesta": "La Cuenta no tiene Forma de Pago registrado", (cuando uso 7 forma de pago contraentrega)
        //4 "respuesta": "Cubrimiento no autorizado para recaudo de valor de producto."
        //5 "respuesta": "Ciudad sin cubrimiento o valor producto supera el valor maximo para proceso recaudos.",
        if(resp.data.respuesta === ""){
          //envia respondio con una cotizacion, agregar la cotizacion a transportadorasCoberturas
          const copyTransportadorasCoberturas = transportadorasCoberturas.map(cotizacion=>{
            if(cotizacion.nombreTransportadora.startsWith("E")){
              //establecer si se debe recotizar o no
              //establezco una copia del total para compararlo despues en la recotizacion si es necessario
              cotizacion.totalCopy = cotizacion.total;
              //se establece la comision de envia
              const fletePorKilo = resp.data.valor_flete + parseInt(props.comisiones.envia.fletePorKilo, 10);
              cotizacion.fletexkilo = fletePorKilo;

            }
            return cotizacion;
          });
          //setTransportadorasCoberturasCopy(Object.values(copyTransportadorasCoberturas));
        } else {
          //no se pudo cotizar
        }
      })
      .catch(err=>{
        console.log('hubo error en envia')
      })
    
  }, [transportadorasCoberturas])
