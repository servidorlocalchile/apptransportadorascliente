         //aca antes de mostrar el total al usuario es necesario volver a cotizar si el usuario eligio 
            //flete, el destinatario paga recaudo o las dos
            //establecer si el usuario agrego una o las dos opciones
            /**Si el usuario elije contraentrega (flete) y idasumecosto (el destiantario paga recaudo)
             * entonces al cliente no se le cobra nada y el total se le cobra a la persona que recibe el paquete
             */
           
            //se establece la diferencia entre el total original y el total con las comisiones
            if(destinatarioPagaTransporteFlete === 1 || destinatarioPagaRecaudo === 1){
                //si elije  flete, aplica para cuando las dos opciones tambien son true
                 
                setRecaudoRecalculadoBool(true);
                const transportadorasCoberturaValues = Object.values(transportadorasCobertura); 
                transportadorasCoberturaValues.forEach(el=>{
                  const diferencia = el.total - el.totalCopy;
                  data.valorrecaudo = parseInt(recaudo, 10) + diferencia;
                  //establecer si la diferencia debe ser del total con el fletepor recaudo y flete envio o solo flete envio
  
                  if(destinatarioPagaTransporteFlete === 1 || destinatarioPagaRecaudo === 1){
                    //se resta el 1% del flete recaudo para que coincida con la factura final
                      data.valorrecaudo = data.valorrecaudo - el.porcentajeRecaudoTotal;
                  }
                  //con la diferencia de cada transportadora se debe realizar otra cotizacion para obtener el total que se le va a mostrar al cliente
                  //se realiza la recotizacion
                  const aveonlinePromise = aveonline('https://aveonline.co/api/nal/v1.0/generarGuiaTransporteNacional.php', data);
                  aveonlinePromise.then(answer=>{
                      answer.data.cotizaciones.forEach(ele=>{
                        ele.flete = destinatarioPagaTransporteFlete;
                        ele.pagaRecaudo = destinatarioPagaRecaudo;
                        ele.totalRecaudar =  recaudo !== "" ? true : false;
                                const sinCoberturaResponse = '000';
                                if(ele.total != sinCoberturaResponse){
                                  //se itera sobre la respuesta final de cada transportadora, esta es la que va al final
  
                                    //establecer que transportadora es para agregar las comisiones
              if(ele.nombreTransportadora.startsWith("E")){
                ele.fletexkilo = ele.fletexkilo + parseInt(props.comisiones.envia.fletePorKilo, 10);
              }
              if(ele.nombreTransportadora.startsWith("T")){
                ele.fletexkilo = ele.fletexkilo + parseInt(props.comisiones.tcc.fletePorKilo, 10);
              }
              if(ele.nombreTransportadora.startsWith("M")){
                ele.fletexkilo = ele.fletexkilo + parseInt(props.comisiones.mensajerosUrbanos.fletePorKilo, 10);
              }
                                  //y se recalcula las comisiones
                                  let porcentajeRecaudoHorenvios;
                                  //se cobra el 1% del valor del recaudo
                                  if(ele.nombreTransportadora.startsWith("E")){
                                     porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.envia.porcentajeRecaudo, 10);
                                  }
                                  if(ele.nombreTransportadora.startsWith("T")){
                                    porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.tcc.porcentajeRecaudo, 10);
                                 }
                                 if(ele.nombreTransportadora.startsWith("M")){
                                  porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.mensajerosUrbanos.porcentajeRecaudo, 10);
                               }
                               ele.valorOtrosRecaudos = parseInt(ele.valorOtrosRecaudos, 10) + porcentajeRecaudoHorenvios;
                               //se calcula el total
                ele.total = ele.fletexkilo + ele.valorOtrosRecaudos + ele.costoManejo;
                //agregar un nuevo valor que contenga el valorrecaudo recalculado
                //y establecer en datosRemitenteDestinatario si existe, si existe entonces no usar el recaudo original sino el recalculado para
                //generar la guia correctamente
  
               // ele.recaudoRecalculado = data.valorrecaudo;
                //se sobreescribe transportadorasCobertura para que contengan los nuevos valores de la cotizacion
                transportadorasCobertura[ele.nombreTransportadora] = ele;
                setTransportadorasCoberturas(Object.values(transportadorasCobertura));
                //cotizar con envia
                
                setErrorMessage(<div></div>);
                setCotizar(true);
                setTimeout(function(){
                  const getDivView = document.getElementById('cotizacionIdView');
                  setSpinner(false);
                  getDivView.scrollIntoView({behavior: "smooth"});
                 }, 250);
                 
                                }
                      })
                  })
                })
                setTransportadorasCoberturas(Object.values(transportadorasCobertura));
                //cotizar con envia
                setErrorMessage(<div></div>);
                setCotizar(true);
                setTimeout(function(){
                  const getDivView = document.getElementById('cotizacionIdView');
                  setSpinner(false);
                  getDivView.scrollIntoView({behavior: "smooth"});
                 }, 250);
  
              } /*else if( destinatarioPagaTransporteFlete === 0 && destinatarioPagaRecaudo === 1  ){
                //si solo elijge pagarecaudo (el precio cambia)
              }*/
              else
              {
                console.log('esto no debe ejecutarse')
                setRecaudoRecalculadoBool(false);
                console.log(transportadorasCobertura);
                setTransportadorasCoberturas(Object.values(transportadorasCobertura));
                //cotizar con envia
                
                setErrorMessage(<div></div>);
                setCotizar(true);
                setTimeout(function(){
                  const getDivView = document.getElementById('cotizacionIdView');
                  setSpinner(false);
                  getDivView.scrollIntoView({behavior: "smooth"});
                 }, 250);
              }
            
            