const doc = new jsPDF();
import htmlPage from './pdfMaker.html';
doc.html(htmlPage, {
   callback: function (doc) {
     doc.save();
   },
   x: 10,
   y: 10
});