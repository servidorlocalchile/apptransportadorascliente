 import aveonline from '../Apis/aveonline';
 const recotizacionEnvia = async (state, recaudo, copyTransportadorasCoberturas, enviaCotizacionBody)=>{
    const transportadoraCoberturaEnvia = copyTransportadorasCoberturas.filter((transportadora)=>{
        if(transportadora.nombreTransportadora.startsWith("E")){
            return transportadora;
        } 
    })
    //si recaudo no se coloco y si se puso destinatario paga el flete
    if(state.checkedA && recaudo === ""){
        //recotizar con el total para saber cuanto va a cobrar envia por el valorRecaudo (el que se muestra en la guia)
        //ese total es el que se le muestra al cliente y con el que se va a generar la guia en caso de que el usuario escoja
        //este valor
       const totalParaRecotizar = transportadoraCoberturaEnvia[0].total;
       //se modifica el codigo cuenta y el total para la recotizacion
       enviaCotizacionBody.cod_cuenta = "479";
       enviaCotizacionBody.info_contenido.valorproducto = totalParaRecotizar;
       const enviaPromise = aveonline(process.env.REACT_APP_ENVIA_HTTPS_BASE_URL + '/ServicioLiquidacionREST/Service1.svc/Liquidacion/', enviaCotizacionBody);
    await enviaPromise.then((resp)=>{
         return resp;
    })
    .catch(err=>{
        console.log('hubo un error en la recotizacion', err);
        return err;
    });
    return enviaPromise;
}
    }
    //
    //si el usuario elije todas las opciones
   /* if(state.checkedA && state.checkedB && recaudo !== ""){
        const totalParaRecotizar = transportadoraCoberturaEnvia[0].total + enviaCotizacionBody.info_contenido.valorproducto;
       //se modifica el codigo cuenta y el total para la recotizacion
       enviaCotizacionBody.cod_cuenta = "479";
       enviaCotizacionBody.info_contenido.valorproducto = totalParaRecotizar;
    }*/

    
    

export default recotizacionEnvia;