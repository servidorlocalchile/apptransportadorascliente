const registrarHoraActual = () => {
    const horaActual = new Date();
    const year = horaActual.getFullYear();
    const month = horaActual.getMonth() + 1;
    const today = horaActual.getDate();
    const hours = horaActual.getHours();
    const minutes = (horaActual.getMinutes()<10?'0':'') + horaActual.getMinutes();
    const horaActualLegible = `${today}/${month}/${year} ${hours}:${minutes}`;
    return {horaActualLegible : horaActualLegible , horaActual : horaActual.toString()}
}

const generarNumeroGuia = (number)=>{
    const numeroFacturaGuia = Math.floor(number + Math.random() * 900000);
    return numeroFacturaGuia;

}
const validateEmail = (email)=> 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
export {registrarHoraActual, generarNumeroGuia, validateEmail}