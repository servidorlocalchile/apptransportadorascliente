import axios from 'axios';

const enviaConsultaGuia = async(ip)=>{
  return await axios.get(ip)
      .then(function (response) {
        return response;    
      })
      .catch(function (err) {
          return err;
      });
}

export default enviaConsultaGuia;