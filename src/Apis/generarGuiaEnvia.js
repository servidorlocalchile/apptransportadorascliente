import axios from 'axios';

const generarGuiaEnvia = async(ip, data)=>{
  return await axios.post(ip, data , {
    headers: {
      'Authorization': 'Basic NTVGMkhPUlQ6NTVGMkRGM0U='
    }
  })
      .then(function (response) {
        return response;    
      })
      .catch(function (err) {
          return err;
      });
}

export default generarGuiaEnvia;