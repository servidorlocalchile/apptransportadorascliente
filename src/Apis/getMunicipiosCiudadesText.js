import codigoDaneCiudades from '../Data/codigoDaneCiudades.text';

async function getMunicipiosCiudadesText(origen, target) {
    try {
      const response = await fetch(codigoDaneCiudades);
      const data = await response.text();
      //bogota es el unico municipio con caracteres especiales (BOGOTA, D.C.)
      //asi que si se trata de bogota, normalizarlo
      if(origen === "BOGOTA D.C."){
          origen = "BOGOTA, D.C.";
      }
      if(target === "BOGOTA D.C."){
        target = "BOGOTA, D.C.";
      }
      const indexOrigen = data.search(origen);
      const indexDestino = data.search(target);
      if(indexOrigen === -1 || indexDestino === -1){
          //no se encuentra en la lista
          return -1;
      } else {
          //extraer el municipio con el codigo dane
          const codigoDaneOrigen = data.substring((indexOrigen - 7), (indexOrigen - 2));
          const codigoDaneDestino = data.substring((indexDestino - 7), (indexDestino - 2));
        return {"Origen" : codigoDaneOrigen, "Destino" : codigoDaneDestino};
      }
     
    } catch (err) {
      console.error(err);
    }
  }
  
  export default getMunicipiosCiudadesText;
