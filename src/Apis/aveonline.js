import axios from 'axios';

const aveonline = async(ip, data)=>{
  return await axios.post(ip, data)
      .then(function (response) {
        return response;    
      })
      .catch(function (err) {
          return err;
      });
}

export default aveonline;