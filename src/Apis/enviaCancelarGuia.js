import axios from 'axios';

const enviaCancelarGuia  = async(ip, body)=>{
  return await axios.post(ip, body)
      .then(function (response) {
        return response;    
      })
      .catch(function (err) {
          return err;
      });
}

export default enviaCancelarGuia;