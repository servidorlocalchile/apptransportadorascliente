import React from "react";
import {connect} from 'react-redux';
import { logout } from '../Utils/index';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Link} from "react-router-dom";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Logo from '../Resources/Images/LogoHorenvios.png';
import Grid from '@material-ui/core/Grid';
import DashboardActionButtons from '../Atoms/DashboardActionButtons';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      [theme.breakpoints.down('xs')]: {
        flexDirection: 'column',
      }
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      marginTop : -10,
    },
    links : {
      color : theme.palette.common.white,
      textDecoration : 'none'
  },
  gridContainer : {
    width : 'auto',
    margin : '10px'
  },
  logoStyles : {
    width : '60px',
    marginRight: 'inherit'
  },
  buttonStyles : {
    height : 'min-content'
  }

  }));
function Header(props) {
 // const [isItLogin, setIsLogin] = useState({isLogin: isLogin()})
    const classes = useStyles();
    const history = useHistory();
    const matches = useMediaQuery('(max-width:440px)');

    const  handleLogout = () => {
      logout();
      history.push("/");
  }
let buttons = null;
if(props.layout){
buttons = <Button className={classes.buttonStyles}variant="contained" color="primary" size='small' onClick={() => handleLogout()}><Link className={classes.links} to="/">Salir</Link></Button>
} else {
  buttons = <ButtonGroup className={classes.root} variant="contained" color="primary" aria-label="contained primary button group">
  <Button><Link className={classes.links} to="/">Inicio</Link></Button>
  <Button><Link className={classes.links}  to="/registro">registro</Link></Button>
  <Button><Link className={classes.links}   to="/ingresar">Ingresar</Link></Button>
  </ButtonGroup>;
}

    return (
        <div className={classes.root}>
        <AppBar position="static">  
          <Toolbar style={{justifyContent : 'space-between'}}>
            <Grid container style={{width : 'auto'}}>
            <Grid container direction='column' alignItems='center'  justify='center' className={classes.gridContainer} >
            {<img className={classes.logoStyles} src={Logo}  alt='Logo Horenvios' />}
            <Typography variant="h6" className={classes.title}>
              Horenvios
            </Typography>
            </Grid>
            
            </Grid>
            <Grid container style={{width : 'auto'}} >
          <Grid container direction='row' justify="flex-end" wrap={matches ? 'wrap' : 'nowrap'} alignItems='center'>
          {props.layout ? <DashboardActionButtons /> : null}
        {buttons}
          </Grid>
       
        </Grid>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
  
  function mapStateToProps(state) { 
    return {
       userInfo : state.setUserInfo.userInfo
    };
  };
  export default connect(mapStateToProps)(Header)
  
