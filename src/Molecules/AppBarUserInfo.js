import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import UserInfo from '../Atoms/UserInfo';
import { uuid } from 'uuidv4';

const useStyles = makeStyles((theme) => ({
  root: {
    
    flexGrow: 1,
    backgroundColor : theme.palette.primary.dark
  }
}));

export default function ButtonAppBar(props) {
  const classes = useStyles();
  return (
    <Grid container  className={classes.root} direction='row' wrap='nowrap' justify='center' alignItems='center' color='primary'>
     {Object.keys(props.userInfo).map((keyName)=>{
       if(keyName === 'Nombre' ){
        return <UserInfo key={uuid()} Title={keyName} Data={props.userInfo[keyName] + ' ' + props.userInfo['Apellido']}/>
       }
        if(keyName === 'Celular' || keyName === 'Email'){
         return <UserInfo  key={uuid()} Title={keyName} Data={props.userInfo[keyName]}/>
        }
        if(keyName === 'HORCODE'){
          return <UserInfo  key={uuid()} Title={keyName} Data={props.userInfo[keyName]}/>
         }
     }
     )}
        
    </Grid>
   
  );
}
//