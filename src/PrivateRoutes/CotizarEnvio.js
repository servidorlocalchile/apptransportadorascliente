import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import SelectInput from '../Atoms/SelectInput';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { FormGroup } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TablaCotizacion from '../Atoms/TablaCotizacion';
import InputAdornment from '@material-ui/core/InputAdornment';
import { makeStyles } from '@material-ui/core/styles';
import aveonline from '../Apis/aveonline';
import Alert from '@material-ui/lab/Alert';
import Spinner from '../Atoms/Spinner';
import CiudadesAutoComplete from '../Atoms/CiudadesAutoComplete';
import getMunicipiosCiudadesText from '../Apis/getMunicipiosCiudadesText';
import {connect} from 'react-redux';
import recotizacionEnvia from '../Utils/recotizacionEnvia';
const useStyles = makeStyles((theme) => ({
    control: {
        padding: theme.spacing(1),
      },
      gridWidth : {
          width : 'auto'
      },
      formGroup : {
        marginLeft : 5
      }
  }));

const CotizarEnvio = (props)=>{

    const classes = useStyles();
    const [agent, setAgent] = useState({nombreCompleto : ''})
    const [city, setCity] = useState("");
    const [targetCity, setTargetCity] = useState('');
    const [units, setUnits] = useState('');
    const [kilos, setKilos] = useState('');
    const [ancho, setAncho] = useState('');
    const [alto, setAlto] = useState('');
    const [largo, setLargo] = useState('');
    const [seguro, setSeguro] = useState('');
    const [contenido, setContenido] = useState('');
    const [recaudo, setRecaudo] = useState('');
    const [state, setState] = useState({
        checkedA: false,
        checkedB: false
      });
    const [cotizar, setCotizar] = useState(false);
    const [results, setResults] = useState({
      fleteEnvia : 0,
      manejo : 0,
      fletePorRecuado : 0,
      total : 0

    })
    const [mainButtonBool, setMainButtonBool] = useState(false);
    const [transportadorasCoberturas, setTransportadorasCoberturas] = useState([]);
    const [transportadorasCoberturasCopy, setTransportadorasCoberturasCopy] = useState([]);
    const [errorMessage, setErrorMessage] = useState(<div></div>);
    const [spinner, setSpinner] = useState(false);
    const [recaudoRecalculadoBool, setRecaudoRecalculadoBool] = useState(false);
    const [municipioOrigen, setMunicipioOrigen] = useState('');
    const [municipioTarget, setMunicipioTarget] = useState('');
    const [enviaResponseStatus, setEnviaResponseStatus] = useState(false);
    const [ciudadesDane, setCiudadesDane] = useState({});

    useEffect(()=>{
      
      const fleteEnvia =  (seguro / 100 ) * 5 ; //5% simulado, esta es la cifra que llega de envia
      const manejo = state.checkedA ? seguro / 100 : 0; //1% del valor asegurado
      const fletePorRecaudo = state.checkedB ? (seguro / 100) / 2 : 0; //0,5% del valor asegurado
      let total = (parseInt(recaudo, 10) + fleteEnvia + manejo + fletePorRecaudo);//.toLocaleString('es-CO');
     // console.log(fleteEnvia, manejo, fletePorRecaudo, recaudo)
     // console.log(total)
      if(Number.isNaN(total)){
        //console.log(total)
       // total = 0;
      } 
      setResults({
        fleteEnvia, manejo, fletePorRecaudo, total
      })
    }, [seguro,  recaudo, state])
  useEffect(()=>{
    
    //aveces city aparece como "" cuando ya se ha elegido
    if( city.length > 0 && targetCity.length > 0 && units.length > 0 &&
      kilos.length > 0 && seguro.length > 0  && contenido.length > 0 && parseInt(seguro) >= 50000){
        setMainButtonBool(false);
    } else {
      setMainButtonBool(true);
    }
  },[agent, city, targetCity,units, kilos, seguro, recaudo , contenido]);
  useEffect(()=>{
    if(agent !== undefined && agent.nombreCompleto.length > 0){
      //se ha seleccionado un agente en la lista de agentes
      setCity(agent.ciudadRemitente);
    } 
  }, [agent])
  useEffect(()=>{
      const getDivView = document.getElementById('spinnerId');
      if(getDivView !== null){
        getDivView.scrollIntoView({behavior: "smooth"});
      }
     
  }, [spinner])

  useEffect(()=>{
    //se activa este useEffect al renderizar por primera vez, filtrar el renderizado con el length de ciudades Dane
    if(Object.keys(ciudadesDane).length > 0){
    

      
    }
    

  }, [ciudadesDane])

 //cotiza con envia despues de cotizar con aveonline
  useEffect(()=>{

      //cotizar con envia
      if(enviaResponseStatus){
            //si ya termino de cotizar con aveonline y enviaResponseStatus es true (se mapearon las ciudades), cotizar con envia 
            //revisar si se va a recaudar dinero, es decir si el destinatario paga el flete, el recaudo o hay recaudo.
       let codCuenta = "478";
       if(recaudo !== "" ){
        //se va a recaudar dinero en el momento de la entrega
        codCuenta = "479";
       }     
      //cotizar con envia
        //si las cuidades se pudieron mapear para obtener el codigo dane
        //el codigo 478 es cuando el horenvios paga a credito, es decir no se cobra ningun valor contraentrega
        //el codigo 479 es cuando el reparto es con recaudo, es decir cuando se agrega valorRecaudo
       //valor asegurado no debe ser menor a 50 mil
         //si no selecciona ninguna opcion, codigo 4
        //si selecciona destinatario paga el flete unicamente codigo 6 (el destinatario paga el flete)
        //si seleccionla destinagario paga el flete y agrega valorrecaudo codigo 7 (contraentrega paga flete y totalrecaudo) o selecciona todo

      const enviaCotizacionBody = {
        "ciudad_origen": ciudadesDane.Origen,
        "ciudad_destino": ciudadesDane.Destino,
        "cod_formapago": 4,
        "cod_servicio": "12",
        "mca_nosabado": 1,
        "mca_docinternacional": 0,
        "cod_regional_cta": 17,
        "cod_oficina_cta": 1,
        "cod_cuenta": codCuenta,
        "con_cartaporte": "0",
        "info_cubicacion": [
            {
                "cantidad": 1,
                "largo": largo,
                "ancho": ancho,
                "alto": alto,
                "peso": kilos,
                "declarado": parseInt(seguro)
            }
        ],
        "info_origen": {
            "nom_remitente": "HECTOR FABIO ARISTIZABAL",
            "dir_remitente": "cra 10 # 57 - 61",
            "tel_remitente": "3045570384",
            "ced_remitente": ""
        },
        "info_destino": {
            "nom_destinatario": "LILIANA MOR",
            "dir_destinatario": "cr 48 150 a 40 Int 14 ap 104",
            "tel_destinatario": "3102834397",
            "ced_destinatario": ""
        },
        "info_contenido": {
            "dice_contener": "cosmeticos",
            "texto_guia": "",
            "accion_notaguia": "Productos cosméticos",
            "num_documentos": "",
            "centrocosto": ""
        },
        "numero_guia": ""
    }
    if(codCuenta === "479"){
      // si hay recaudo se debe primero cotizar para saber cual seria la ganancia
      // por ejemplo 6800 vale el envio, mas 2400 es 9400
      //una vez se obtienen el total se debe enviar en la segunda cotizacion en valorRecaudo 9400 para
      //saber cuanto va a cobrar envia por recaudar esos 9400 (para que aparezca en la guia de cobro que se le lleva al cliente)
      //en la segunda cotizacion aparece el valorRecaudo, por ejemplo 2500, entonces se suma 9400 mas 2500 para un total de 11900
      //esos 11900 seria lo que se le mostraria al usuario en la tabla de cotizacion y el valorRecuado para la guia final
      //es decir la que se va a imprimir para que el destinatario pague los 11900 y le queden los 2600 de ganancia a horenvios.com
      
      
      enviaCotizacionBody.info_contenido.valorproducto = parseInt(recaudo);
       
    }
    //comunicarse con el proxy en el servidor por medio de http ya que la api de envia funciona unicamente con https
    /*const comunicacionProxyInterno = aveonline(proxyIp,enviaCotizacionBody) ;
    comunicacionProxyInterno.then(ans=>{
      debugger;
    })
    .catch(err=>{
      debugger;
    });*/

    //si el codigo es 479, se debe recotizar!
    const enviaHttpsUrlCotizacion = process.env.REACT_APP_ENVIA_HTTPS_BASE_URL + '/ServicioLiquidacionREST/Service1.svc/Liquidacion/';
      const enviaPromise = aveonline(enviaHttpsUrlCotizacion, enviaCotizacionBody)
      enviaPromise.then(resp=>{
        //envia puede responder de las siguientes formas
        //1  "respuesta": "", cuando la respuesta es exitosa
        //2    "respuesta": "Ciudad Destino sin Cubrimiento"
        //3  "respuesta": "La Cuenta no tiene Forma de Pago registrado", (cuando uso 7 forma de pago contraentrega)
        //4 "respuesta": "Cubrimiento no autorizado para recaudo de valor de producto."
        //5 "respuesta": "Ciudad sin cubrimiento o valor producto supera el valor maximo para proceso recaudos.",
        if(resp.data.respuesta === ""){
          //envia respondio con una cotizacion, agregar la cotizacion a transportadorasCoberturas
          const copyTransportadorasCoberturas = transportadorasCoberturas.map(cotizacion=>{
            if(cotizacion.nombreTransportadora.startsWith("E")){
              //establecer si se debe recotizar o no
              //establezco una copia del total para compararlo despues en la recotizacion si es necessario
              cotizacion.totalCopy = cotizacion.total;
              //se establece la comision de envia
              const fletePorKilo = resp.data.valor_flete + parseInt(props.comisiones.envia.fletePorKilo, 10);
              let valorOtrosRecaudosEnvia = 0;
              //revisar si hay recaudo y se debe sumar el valor recaudo
              if(recaudo !== ""){
                valorOtrosRecaudosEnvia = resp.data.valor_otros + (parseFloat(recaudo, 10) / 100 * parseFloat(props.comisiones.envia.porcentajeRecaudo, 10)  );
              }
              cotizacion.ciudadesDane = ciudadesDane;
              cotizacion.cotizacionConApiEnvia = true;
              cotizacion.fletexkilo = fletePorKilo;
              cotizacion.costoManejo = resp.data.valor_costom;
              cotizacion.valorOtrosRecaudos = valorOtrosRecaudosEnvia;
              cotizacion.diasentrega = resp.data.dias_entrega;
              cotizacion.trayecto = resp.data.cubrimiento;
              cotizacion.fletexunidad = resp.data.valor_flete;
              cotizacion.fletetotal = resp.data.valor_flete;
              cotizacion.valorTotal = resp.data.valor_flete + resp.data.valor_costom;
              cotizacion.total = fletePorKilo + resp.data.valor_costom + resp.data.valor_otros;
              cotizacion.totalCopy = resp.data.valor_flete + resp.data.valor_costom + resp.data.valor_otros; //revisar si se agrega valor_otros y donde?
              //si el usuario escoje tanto valorRecuado, como destinatario paga flete como destinatario paga  el recaudo
              if(recaudo !== "" && state.checkedA && state.checkedB){
                //si el usuario escoge todas las opciones es necesario agregar el 1% del recaudo al total
                const totalEnGuia = fletePorKilo + resp.data.valor_costom + resp.data.valor_otros + (parseFloat(recaudo,10) / 100 * props.comisiones.envia.porcentajeRecaudo);  
                cotizacion.total =  totalEnGuia;   
                  //si el usuario elije todas las opciones sumar el total de la recotizacion al valor del recaudo
              //enviaCotizacionBody.info_contenido.valorproducto = enviaCotizacionBody.info_contenido.valorproducto + totalEnGuia;       
              }
                }
            return cotizacion;
          });
          //aca se debe analizar si se debe recotizar o no
          //se debe recotizar si el usuario pone valor a recaudar o destinatario paga el flete.
          if(state.checkedA || recaudo !== ""){
            recotizacionEnvia(state, recaudo, copyTransportadorasCoberturas, enviaCotizacionBody)
            .then(cotizacionFinal=>{
              const valoresNuevaCotizacion = cotizacionFinal.data;
              if(valoresNuevaCotizacion.respuesta === ""){
                //la recotizacion fue exitosa
                //reescribir el total y el valorRecaudo que se va a mostrar al cliente
              copyTransportadorasCoberturas.forEach((part, index, theArray) =>{
                if(theArray[index].nombreTransportadora.startsWith("E")){
                  //se acctualiza el valorOtros Recaudos con el total que cobrara envia por recaudar (que aparezca en la guia)
                  theArray[index].valorOtrosRecaudos = valoresNuevaCotizacion.valor_otros;
                  //se actualiza el total a mostrar al cliente con el total mas el valor_otros de la api de envia
                  theArray[index].total = valoresNuevaCotizacion.valor_otros + theArray[index].total;
                  theArray[index].ciudadesDane = ciudadesDane;
                  theArray[index].cotizacionConApiEnvia = true;
                  return theArray[index];
                }
                //hay que retornar los arreglos que no se modificaron?
              });
              //solo se deben agregar las cotizaciones cuando se este seguro que cotizo o recotizo con envia
              setTransportadorasCoberturasCopy(copyTransportadorasCoberturas);
              }
              else {
                //la recotizacion no fue exitosa
                //solo se deben agregar las cotizaciones cuando se este seguro que cotizo o recotizo con envia
                setTransportadorasCoberturasCopy(copyTransportadorasCoberturas);
                return valoresNuevaCotizacion.respuesta;
              }
              
              return copyTransportadorasCoberturas;
            })
            .catch(err=>{
              console.log('hubo un error en la recotizacion', err);
              //solo se deben agregar las cotizaciones cuando se este seguro que cotizo o recotizo con envia
              setTransportadorasCoberturasCopy(copyTransportadorasCoberturas);
            })
          } else {
            //no hay  necesidad de recotizar con envia
            //solo se deben agregar las cotizaciones cuando se este seguro que cotizo o recotizo con envia
            setTransportadorasCoberturasCopy(copyTransportadorasCoberturas);
          }
          //solo se deben agregar las cotizaciones cuando se este seguro que cotizo o recotizo con envia
          //setTransportadorasCoberturasCopy(copyTransportadorasCoberturas);
        } else {
          //no se pudo cotizar
          setTransportadorasCoberturasCopy(transportadorasCoberturas)
        }
      })
      .catch(err=>{
        console.log('hubo error en envia');
        //solo se deben agregar las cotizaciones cuando se este seguro que cotizo o recotizo con envia
        setTransportadorasCoberturasCopy(transportadorasCoberturas);
      })

      } else {
        //no hay ciudades para cotizar con envia
        setTransportadorasCoberturasCopy(transportadorasCoberturas);
            /* setErrorMessage(<div></div>);
            setCotizar(true);
            setTimeout(function(){
              const getDivView = document.getElementById('cotizacionIdView');
              setSpinner(false);
              getDivView.scrollIntoView({behavior: "smooth"});
             }, 250);*/
      }




         
  
  }, [transportadorasCoberturas])

  useEffect(()=>{
    //solo cuando transportadorasCoberturasCopy se asigna (despues de cotizar o recotizar con envia)
    //se desliza la vista hacia la tabla de cotizacion
    if(transportadorasCoberturasCopy.length > 0){
      setTimeout(()=>{
        setErrorMessage(<div></div>);
      setCotizar(true);
      setTimeout(function(){
        const getDivView = document.getElementById('cotizacionIdView');
        setSpinner(false);
        getDivView.scrollIntoView({behavior: "smooth"});
       }, 250);
      }, 1500)
      
    }
   
  }, [transportadorasCoberturasCopy])

  
    const handleCityChange = (event, label) => {
      //aveces la ciudad llega como [""] asi se elija una, reparar esto
      if(event !== undefined  ){
        const arrayId_city = event.split('-');
        const cityFirst = arrayId_city[0].trim();
        const city = arrayId_city[1].substring(1);
        if(label === "Ciudad Origen"){
          setCity(cityFirst);
          setMunicipioOrigen(city);
        } else if( label === "Ciudad Destino") {
          setTargetCity(cityFirst);
          setMunicipioTarget(city);
        }
      }
     
    };
  
    const handleUnitsChange = (event) => {
      setUnits(event.target.value);
    };
    const handleKilosChange = (event) => {
      setKilos(event.target.value);
    };
    const handleAltoChange = (event) => {
      setAlto(event.target.value);
    };
    const handleAnchoChange = (event) => {
      setAncho(event.target.value);
    };
    const handleLargoChange = (event) => {
      setLargo(event.target.value);
    };
    const handleSeguroChange = (event) => {
      setSeguro(event.target.value);
      
    };
    const handleContenidoChange = (event) => {
      setContenido(event.target.value);
    };
    const handleRecaudoChange = (event) => {
        setRecaudo(event.target.value);
    };
   //cotizar envio
    const handleClick = () => {
      setSpinner(true);
      setErrorMessage(<div></div>);
      setCotizar(false);
      //mapear las ciudades elegidas por el usuario al codigo dane
      getMunicipiosCiudadesText(municipioOrigen, municipioTarget)
      .then(resp=>{
        if(resp === -1){
          //no se encontraron los municipios
          setEnviaResponseStatus(false);
        } else {
          //la respuesta contiene los codigos dane del origen y el destino
          setEnviaResponseStatus(true);
         //cuando se obtiene la respuseta de las ciudades es necesario cotizar el envio, preferiblemente
          //en un useEffect
          setCiudadesDane(resp)
        }

      })
      .catch(err=>{
        setEnviaResponseStatus(false);
        console.log(err)
      });
     

     
     
      //aca se debe comunicar con la api de aveonline y arrojar los resultados de fletes
      const destinatarioPagaTransporteFlete = state.checkedA ? 1 : 0;
      const destinatarioPagaRecaudo = state.checkedB ? 1 : 0;

      let transportadorasCobertura = {};
      const dataAuthBody = {
        tipo : 'auth',
        usuario : 'innovacion',
        clave : '019*'
      }
      //autenticacion
      const aveonlinePromiseAuth = aveonline('https://aveonline.co/api/comunes/v1.0/autenticarusuario.php', dataAuthBody);
      aveonlinePromiseAuth.then(resp=>{
        const data = {
          tipo : 'cotizar',
          token : resp.data.token,
          idempresa : resp.data.cuentas[0].usuarios[0].id,
          origen : city,
          destino : targetCity, 
          unidades : parseInt(units),
          kilos : parseInt(kilos),
          valordeclarado : parseInt(seguro),
          contraentrega : destinatarioPagaTransporteFlete, //destinatario paga el costo del transporte 1 true, 0 false
          idasumecosto : destinatarioPagaRecaudo, //destinatario paga el recaudo 1 true, 0 false (si solo selecciona este ese dinero se le cobra al que recibe el paquete, es decir no se factura en el total)
          valorrecaudo :  Number.isNaN(parseInt(recaudo)) ? 0 : parseInt(recaudo)
        };
        /**Si el usuario elije contraentrega (flete) y idasumecosto (el destiantario paga recaudo)
         * entonces al cliente no se le cobra nada y el total se le cobra a la persona que recibe el paquete
         */
        //ATENCION, EN TCC CUANDO CUANDO SE COTIZA O SE HACE UNA GUIA SOLO CON DESTINATARIO PAGA EL FLETE
        //ESTA AGREGANDO 3100 PESOS MAS AL VALOR TOTAL y 3500 con envia*****
        //TAMBIEN ES NECESARIO MIRAR COMO SE MUESTRA EL TOTAL PORQUE NO ESTA COINCIDIENDO CON EL VALOR DE GUIA
        //NO SE ESTA MOSTRANDO TODO EL VALOR, GUIAS QUE SON DE 8 MIL ESTA MOSTRANDO SOLO 3 MIL ATENCION!!!
        const aveonlinePromise = aveonline('https://aveonline.co/api/nal/v1.0/generarGuiaTransporteNacional.php', data);
        aveonlinePromise.then(resp=>{
          resp.data.cotizaciones.forEach(el=>{
            //asigno si la guia contiene recaudo, destinatarioPagaRecaudo y flete.
            /**
             * promise error TypeError: Cannot read property 'tcc' of undefined
             * al parecer el problema llega por las comisionse que no estan llegando correctamente
             */
    el.flete = destinatarioPagaTransporteFlete;
    el.pagaRecaudo = destinatarioPagaRecaudo;
    el.totalRecaudar =  recaudo !== "" ? true : false;
            const sinCoberturaResponse = '000';
            if(el.total != sinCoberturaResponse){
              //agrego la respuesta a la copia para usarla en la recotizacion
              //la transportadora si tiene cobertura
               //se suman los precios de horenvios
            //2600 al flete
            //establezco una copia del valor total para compararlo despues en las recotizaciones
            el.totalCopy = el.total;
            //establecer que transportadora es para agregar las comisiones
            if(el.nombreTransportadora.startsWith("E")){
              el.fletexkilo = el.fletexkilo + parseInt(props.comisiones.envia.fletePorKilo, 10);
              //si el usuario solo elije pagaflete entonces se deben sumar 3500 al total y mostrarlo en otros recaudos
              if(destinatarioPagaTransporteFlete == 1 && recaudo == ""){
                el.valorOtrosRecaudos = el.valorOtrosRecaudos + 3500;
              }
            }
            if(el.nombreTransportadora.startsWith("T")){
              el.fletexkilo = el.fletexkilo + parseInt(props.comisiones.tcc.fletePorKilo, 10);
               //si el usuario solo elije pagaflete entonces se deben sumar 3500 al total y mostrarlo en otros recaudos
               if(destinatarioPagaTransporteFlete == 1 && recaudo == ""){
                el.valorOtrosRecaudos = el.valorOtrosRecaudos + 3100;
              }
            }
            if(el.nombreTransportadora.startsWith("M")){
              el.fletexkilo = el.fletexkilo + parseInt(props.comisiones.mensajerosUrbanos.fletePorKilo, 10);
            }
              //si el usuario agrega recaudo
              if(recaudo !== ""){
                let porcentajeRecaudoHorenvios;
                //se cobra el 1% del valor del recaudo
                if(el.nombreTransportadora.startsWith("E")){
                   porcentajeRecaudoHorenvios = (recaudo / 100) * parseFloat(props.comisiones.envia.porcentajeRecaudo, 10);
                }
                if(el.nombreTransportadora.startsWith("T")){
                  porcentajeRecaudoHorenvios = (recaudo / 100) * parseFloat(props.comisiones.tcc.porcentajeRecaudo, 10);
               }
               if(el.nombreTransportadora.startsWith("M")){
                porcentajeRecaudoHorenvios = (recaudo / 100) * parseFloat(props.comisiones.mensajerosUrbanos.porcentajeRecaudo, 10);
             }
              
               
                el.valorOtrosRecaudos = parseInt(el.valorOtrosRecaudos, 10) + porcentajeRecaudoHorenvios;
                el.porcentajeRecaudoTotal = porcentajeRecaudoHorenvios;
              }   
              //se calcula el total
              el.total = el.fletexkilo + el.valorOtrosRecaudos + el.costoManejo;
              transportadorasCobertura[el.nombreTransportadora] = el;
            } 
          })
          const transportadorasCoberturaKeys = Object.keys(transportadorasCobertura);
          if(transportadorasCoberturaKeys.length > 0){
            //ACTUALIZAR COTIZACION
            setTransportadorasCoberturas(Object.values(transportadorasCobertura));
            //despues de cotizar con aveonline es necesario cotizar con envia
            
            /*setErrorMessage(<div></div>);
            setCotizar(true);
            setTimeout(function(){
              const getDivView = document.getElementById('cotizacionIdView');
              setSpinner(false);
              getDivView.scrollIntoView({behavior: "smooth"});
             }, 250);*/
          } else{
            //no hay cobertura
            console.log('no hay cobertura', transportadorasCobertura);
            setTimeout(function(){
              const getDivView = document.getElementById('errorMessage');
              if(getDivView !== null && getDivView !== undefined){
                setSpinner(false);
                getDivView.scrollIntoView({behavior: "smooth"});
              }
              
             }, 250);
            setErrorMessage(<Alert style={{width : 'fit-content'}}severity="error">Ciudades sin cobertura</Alert>
            )
          }
        })
        .catch(err=>{
          console.log('promise error', err)
        })
      })
      .catch(err=>{
        console.log('hubo un error con la api de autenticacion', err);
        setSpinner(false);
        if(err.message === "Cannot read property 'token' of undefined"){
          setErrorMessage(<Alert style={{width : 'fit-content'}}severity="error">No hay conexión a internet, por favor revise que está conectado a la red.</Alert>);      
        } else {
          setErrorMessage(<Alert style={{width : 'fit-content'}}severity="error">{err.message}</Alert>);      
        }

      })
     
     
    };
    const selectInputChange = (ev) => setAgent(ev);
 
    const handleChangeCheck = (event) => {
      setState({ ...state, [event.target.name]: event.target.checked });
    };
    let formControlLabelDiv;
    //la opcion recaudo solo esta disponible si se agrega un valor al input recaudo mayor a 0
    if(recaudo > 0){
     formControlLabelDiv =    <FormControlLabel
        control={<Checkbox checked={state.checkedB} onChange={handleChangeCheck} name="checkedB" />}
        label="El Destinatario Paga el Recaudo"
      />;
    
    } else {
      formControlLabelDiv = <FormControlLabel
      control={<Checkbox checked={state.checkedB} onChange={handleChangeCheck} name="checkedB" />}
      label="El Destinatario Paga el Recaudo"
      disabled
    />;
    }
   return(
        <div>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
       1. Datos Generales
      </Typography>
      <Grid container justify='center' alignItems='center'  className={classes.gridWidth}>
    <SelectInput selectInputChange={selectInputChange}/>
    <CiudadesAutoComplete label="Ciudad Origen"  updateChildrenCityChange={handleCityChange} />
    <CiudadesAutoComplete label="Ciudad Destino" updateChildrenCityChange={handleCityChange}  />
    {/*<Ciudades label="Ciudad Origen" variant="filled" updateChildrenCityChange={handleCityChange} />
    <Ciudades  label="Ciudad Destino" variant="filled" updateChildrenCityChange={handleCityChange}  />*/}
</Grid>
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Datos de la Mercancía
      </Typography>
      <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
    <TextField required value={units} onChange={handleUnitsChange} label="Unidades" variant="filled" type='number'className={classes.control} />
    <TextField required id="standard-start-adornment" value={kilos} onChange={handleKilosChange} label="Kilos" variant="filled" type='number'className={classes.control}
     InputProps={{
      startAdornment: <InputAdornment position="start">Kg</InputAdornment>,
    }} />
    <TextField required value={alto} onChange={handleAltoChange} label="Alto en cm" variant="filled" className={classes.control} />
    <TextField required value={ancho} onChange={handleAnchoChange}  label="Ancho en cm" variant="filled" className={classes.control} />
    <TextField required value={largo} onChange={handleLargoChange}  label="Largo en cm" variant="filled" className={classes.control} />
    </Grid>
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Valores
      </Typography>
      <Grid container  justify='center' alignItems='center' className={classes.gridWidth }>
    <TextField required id="standard-adornment-amount" value={seguro} onChange={handleSeguroChange}   label="Valor del Seguro (mínimo 50000)" variant="filled" type='number'className={classes.control} 
      InputProps={{
        startAdornment: <InputAdornment position="start">$</InputAdornment>,
      }}/>
    <TextField required value={contenido} onChange={handleContenidoChange}  label="Contenido" variant="filled" className={classes.control} />
   
 <TextField  id="standard-adornment-amount"  value={recaudo} onChange={handleRecaudoChange} label="Valor de Recaudo" variant="filled" type='number'className={classes.control}
     InputProps={{
      startAdornment: <InputAdornment position="start">$</InputAdornment>,
    }} />
    {/*cotizar ? <TextField disabled={true} id="standard-adornment-amount"   value={results.total.toLocaleString('es-CO')} label="Total" onChange={handleTotalChange}variant="filled" className={classes.control}
     InputProps={{
      startAdornment: <InputAdornment position="start">$</InputAdornment>,
    }} />
    : null */}
   
    <FormGroup className={classes.formGroup} row>
      <FormControlLabel
        control={<Checkbox checked={state.checkedA} onChange={handleChangeCheck} name="checkedA" />}
        label="El Destinatario Paga el Flete"
      />
       {formControlLabelDiv}
      </FormGroup >
    </Grid>
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    <Button disabled={mainButtonBool} variant="contained" color="primary" onClick={handleClick}>
  Cotizar Envío
</Button>
    </Grid>
    {cotizar ?
     <TablaCotizacion 
    transportadorasCobertura={transportadorasCoberturasCopy}
     stateProps={
      {agent, city, targetCity, units, kilos, ancho, alto, largo, seguro, contenido, recaudo, state, cotizar,  
        results, state, recaudoRecalculadoBool, props
      }}
     /> :
    null
    }
    {spinner ? <Spinner  />: null}
    <Grid style={{marginTop : 20}} id="errorMessage"container justify="center">
    {errorMessage}
    </Grid>
   
    </div>
    ) 
    
}



function mapStateToProps(state) { 
  return {
     comisiones : state.setDataInfo.dataInfo.comisiones
  };
};
export default connect(mapStateToProps)(CotizarEnvio);
