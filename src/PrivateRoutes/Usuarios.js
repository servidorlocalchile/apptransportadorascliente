import React, { useState, useEffect } from 'react';
import store from '../Redux/store';
import { dataInfo } from '../Redux/actions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Checkbox from '@material-ui/core/Checkbox';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ListadoGuiasUsuarios  from './ListadoGuiasUsuarios';
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';


let ip = '';
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  // dev code
  ip = 'https://localhost/habilitarDeshabilitarUsuario';
} else {
  // production code
  ip = `${process.env.REACT_APP_URL}/habilitarDeshabilitarUsuario`;
  //;

}
const useStyles = makeStyles((theme) => ({
  control: {
    padding: theme.spacing(1),
  },
  alertStyles: {
    width: 'fit-content',
    margin: 'auto'
  },
  formControl: {
    minWidth: 130,
    height : 'fit-content'
  },
  paperStyles: {
    minWidth: 220,
    minHeight: 150,
    margin : 10
  },
  parentGridStyles: {
    margin: 10
  },
  titleStyles: {
    fontWeight: 600
  },
  innerTitleStyles : {
    fontWeight : 600,
    marginRight : 10
  },
  contentGridStyles : {
  width : 'auto'
  },
  innerGridStyles : {
    margin : 5
  }
}));
/**
 * Renderiza la información de los usuarios
 * @param {*} props 
 */
const Usuarios = (props) => {

  const classes = useStyles();
  const matches = useMediaQuery('(max-width:600px)');
  const [errorMessage, setErrorMessage] = useState('');
  const [error, setError] = useState(false);
  const [userSelected, setUserSelected] = useState({
    Email : '',
    HORCODE : '',
    Nombre : '',
    Apellido : '',
    Celular : '',
    Agentes : [],
    GuiasInfo : [],
    CuentaHabilitada : false,
    dataInfoEntries : []
  });
  const [agentSelected, setAgentSelected] = useState({
    HORCODE: "",
barrio: "",
celular: "",
ciudadRemitente: "",
codeNumber: "",
correo: "",
direccion: "",
fijo: "",
nombreCompleto: ""
  });
 const [guiasLengthState, setGuiasLengthState] = useState(0);
    useEffect(()=>{
      //si datainfocambia porque se elimino una guia, actualizar userSelected
      if(userSelected.Email !== undefined && userSelected.Email  !== ''){
        setUserSelected(props.dataInfo[userSelected.Email])
      } 
   
      if(userSelected.Email === undefined ){
        //si userSelected es undefined, entonces es porque es el primer renderizado o porque se cancelaron varias guias 
        //cuando todos los usuarios se seleccionaron
        //en ese caso el total de guias generadas se debe actualizar con el nuevo valor, es decir el total de las guias generadas
        //despoues de la eliminacion de guias, ese total viene en props.dataInfo
        let guiasInfoTodosLosUsuariosCopy = [];
        let todosLosUsuarios = props.dataInfo;
        delete todosLosUsuarios['comisiones'];
        const todosLosUsuariosKeys = Object.keys(todosLosUsuarios);
        todosLosUsuariosKeys.forEach(email=>{
           todosLosUsuarios[email].GuiasInfo.forEach(el=>guiasInfoTodosLosUsuariosCopy.push(el));
        })
        setUserSelected(
          {
            Email : '',
            HORCODE : '',
            Nombre : '',
            Apellido : '',
            Celular : '',
            Agentes : [],
            GuiasInfo : guiasInfoTodosLosUsuariosCopy,
            CuentaHabilitada : false,
            dataInfoEntries : []
          }  
        );
        setGuiasLengthState(guiasInfoTodosLosUsuariosCopy.length);
      }
      if(userSelected.Email === ""){
        let guiasInfoTodosLosUsuariosCopy = [];
        let todosLosUsuarios = props.dataInfo;
        delete todosLosUsuarios['comisiones'];
        const todosLosUsuariosKeys = Object.keys(todosLosUsuarios);
        todosLosUsuariosKeys.forEach(email=>{
           todosLosUsuarios[email].GuiasInfo.forEach(el=>guiasInfoTodosLosUsuariosCopy.push(el));
        })
        setGuiasLengthState(guiasInfoTodosLosUsuariosCopy.length);
      }
    }, [props.dataInfo])
    useEffect(()=>{
      //en el primer renderizado si se seleccionan todos los usuariosn o llegan todos como userSelected, revisar por que
        setGuiasLengthState(userSelected.GuiasInfo.length);
      
      
    }, [userSelected])
  const updateUserSelectedInfo = (selected) => {
    if(userSelected !== undefined && selected !== 'Todos los usuarios'){
     setUserSelected(selected);
    } else if(selected === 'Todos los usuarios'){
      ;
      //agregar todas las guias
      let entries = Object.entries(props.dataInfo);
    
      let dataInfoEntries = entries[0].includes('comisiones') ? entries.slice(1) : entries;
      const allGuiasInfo = [];
      ;
      
       dataInfoEntries.forEach(el => {
         el[1].GuiasInfo.forEach(innerEl=>{ //TypeError: Cannot read property 'forEach' of undefined
            innerEl.HORCODE = el[1].HORCODE;
            allGuiasInfo.push(innerEl);
         })

       
      });
      setUserSelected({GuiasInfo : allGuiasInfo, Agentes : [], dataInfoEntries })
      setGuiasLengthState(allGuiasInfo.length);
    }
    
  }
  const updateAgentChange = (agentSelected)=>{
    if(agentSelected !== undefined){
      setAgentSelected(agentSelected[0]);
    }
    
  }
  
  const handleCheckChange = (event) => {
    setUserSelected({...userSelected, CuentaHabilitada : event.target.checked});
    //Enviar el dato al servidor para habilitar o deshabilitar la cuenta del usuario
    
    const options = {
      headers: {credentials: 'include'}
    };
  axios.put(ip, {
      Email : userSelected.Email,
      CuentaHabilitada : event.target.checked
    }, options)
    .then(function (response) {
const habilitado = !userSelected.CuentaHabilitada ? 'habilitado.' : 'deshabilitado.';
const message = `El usuario ha sido ${habilitado}`;
setErrorMessage(<Alert className={classes.alertStyles} severity="success">{message}</Alert> );
setError(false);
store.dispatch(dataInfo('set_data_info', response.data));

    })
    .catch(function (error) {
      console.log(error);
      const message = error.response.data;
      setErrorMessage( <Alert className={classes.alertStyles} severity="error">{message}.</Alert> );
      setError(true);
    });
  };
  return (
      <Grid container direction='column' justify='center' alignItems='center' >
        <Typography variant="h6" gutterBottom>
          Usuarios
      </Typography>
        <Grid container justify='center' direction={matches ? "column" : "row" } wrap="nowrap"className={classes.contentGridStyles} >
         <Grid container alignItems="center" direction="column" className={classes.innerGridStyles}>
         <SelectInputUsuarios dataInfo={props.dataInfo} selectInputChange={updateUserSelectedInfo} />
         <Paper className={classes.paperStyles} >
            <Grid container justify="space-between" direction="row" wrap="nowrap">
              <Grid className={classes.parentGridStyles}>
                <Typography className={classes.titleStyles} variant="subtitle1" gutterBottom>
                  Correo
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {userSelected.Email}
      </Typography>
              </Grid>

              <Grid className={classes.parentGridStyles}>
                <Typography className={classes.titleStyles} variant="subtitle1" gutterBottom>
                  Código
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {userSelected.HORCODE}
      </Typography>
              </Grid>
            </Grid>
            <Divider />
            <Grid >
            <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Nombre:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {userSelected.Nombre}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Apellidos:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {userSelected.Apellido}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Celular:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {userSelected.Celular}
      </Typography>
              </Grid>
              <Divider />
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Agentes:
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
                  {userSelected.Agentes.length}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Guias Generadas:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {guiasLengthState}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start" alignItems="center">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Cuenta Habilitada:
      </Typography>
      <Checkbox
        checked={userSelected.CuentaHabilitada}
        onChange={handleCheckChange}
        inputProps={{ 'aria-label': 'primary checkbox' }}
     />
              </Grid>
            </Grid>
          </Paper>
          {errorMessage}
         </Grid>
          <Grid container alignItems="center" direction="column" className={classes.innerGridStyles} >
          <SelectInputAgentes userSelected={userSelected} selectInputChange={updateAgentChange}/> 
          <Paper className={classes.paperStyles} >
          <Grid container justify="space-between" direction="row" wrap="nowrap">
              <Grid className={classes.parentGridStyles}>
                <Typography className={classes.titleStyles} variant="subtitle1" gutterBottom>
                  Correo
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.correo}
      </Typography>
              </Grid>

              <Grid className={classes.parentGridStyles}>
                <Typography className={classes.titleStyles} variant="subtitle1" gutterBottom>
                  Código
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.HORCODE}
      </Typography>
              </Grid>
            </Grid>
            <Divider />
            <Grid >
            <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Nombre Completo:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.nombreCompleto}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Celular:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.celular}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Dirección:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.direccion}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Fijo:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.fijo}
      </Typography>
              </Grid>
              <Grid className={classes.parentGridStyles} container direction="row" justify="flex-start">
                <Typography className={classes.innerTitleStyles}  variant="subtitle1" gutterBottom>
                  Barrio:
      </Typography>
                <Typography variant="subtitle1" gutterBottom>
                  {agentSelected.barrio}
      </Typography>
              </Grid>
            </Grid>
          </Paper>
            </Grid>
        </Grid>
        <Divider/>
        <ListadoGuiasUsuarios Email={userSelected.Email} userSelected={userSelected} userInfo={{GuiasInfo : userSelected.GuiasInfo, GuiasInfoRelacion : userSelected.GuiasInfoRelacion}} HorCode={userSelected.HORCODE}/>
      </Grid>
  )

}
/**
 * Renderiza la selección de los usuarios
 * @param {} props 
 */
const SelectInputUsuarios = props => {
  const classes = useStyles();
  const [selected, setSelected] = useState('');
  const [userEmails, setUserEmails] = useState([]);
  useEffect(() => {
    if(props.dataInfo !== undefined){
      let newPropsDatInfo = props.dataInfo;
      //se borra la propiedad comisiones
      delete newPropsDatInfo.comisiones;
      const dataEntries = Object.entries(newPropsDatInfo);
      const userKeys = dataEntries.map(el => {
          return el[0] + ' / ' + el[1].HORCODE;
      })
      userKeys.unshift('Todos los usuarios');
      setUserEmails(userKeys);
    }
   
  }, []);
  useEffect(() => {
    if(selected !== 'Todos los usuarios' ){
      const userEmailAlone = selected.split(' ')[0];
      const userSelectedInfo = props.dataInfo[userEmailAlone];
      if(selected.length > 0){
        props.selectInputChange(userSelectedInfo);
      }
    } else {
      props.selectInputChange(selected);
    }
   

  }, [selected])
  const handleChange = (event) => {
    setSelected(event.target.value);
  };
  const menuItems = userEmails.map(el => <MenuItem value={el}>{el}</MenuItem>)

  return (
    <FormControl variant="filled" className={classes.formControl} >
      <InputLabel id="selectInputUsuarios">Seleccionar Usuario</InputLabel>
      <Select
        labelId="selectInputUsuarios"
        id="selectInputUsuarios"
        value={selected}
        onChange={handleChange}
        className={classes.headerStyles}
      >
        {menuItems}
      </Select>

    </FormControl>
  )
}

/**
 * Renderiza la selección de Agentes
 * @param {} props 
 */
const SelectInputAgentes = props => {
  const classes = useStyles();
  const [selected, setSelected] = useState('');
  const [userEmails, setUserEmails] = useState([]);
  useEffect(() => {
      const userKeys = props.userSelected.Agentes.map(el => {
      return el.correo + ' / ' + el.HORCODE;
    });
    setUserEmails(userKeys);
  }, [props.userSelected]);
  useEffect(() => {
    if(selected.length > 0){
      const userEmailAlone = selected.split(' ')[0];
      const userSelectedInfo = props.userSelected.Agentes.filter(el=>el.correo === userEmailAlone);
      props.selectInputChange(userSelectedInfo);
    }   
  }, [selected])
  const handleChange = (event) => {
    setSelected(event.target.value);
  };
  const menuItems = userEmails.map(el => <MenuItem key={uuidv4()} value={el}>{el}</MenuItem>)

  return (
    <FormControl variant="filled" className={classes.formControl} >
      <InputLabel id="SelectInputAgentes">Seleccionar Agente</InputLabel>
      <Select
        labelId="SelectInputAgentes"
        id="SelectInputAgentes"
        value={selected}
        onChange={handleChange}
        className={classes.headerStyles}
      >
        {menuItems}
      </Select>

    </FormControl>
  )
}
function mapStateToProps(state) {
  return {
    dataInfo: state.setDataInfo.dataInfo
  };
};
export default connect(mapStateToProps)(Usuarios);
