import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import store from '../Redux/store';
import {userInfo, dataInfo} from '../Redux/actions';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import {connect} from 'react-redux';
import {validateEmail} from '../Utils/useFullFunctions';


let ip = '';
 if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  // dev code
  ip = 'https://localhost/agregarAgente';
} else {
  // production code
  ip = `${process.env.REACT_APP_URL}/agregarAgente`;
  //;

}
const useStyles = makeStyles((theme) => ({
    control: {
        padding: theme.spacing(1),
      },
      gridWidth : {
          width : 'auto'
      },     
  alertStyles : {
    width: 'fit-content',
    margin: 'auto'
  }
  }));
const CrearAgente = (props)=>{

    const classes = useStyles();
    const [agent, setAgent] = useState({
    nombreCompleto : '',
    direccion : '',
    correo : '',
    ciudadRemitente : '',
    fijo : '',
    celular : '',
    barrio : ''
    });
    const [errorMessage, setErrorMessage] = useState('');
    const [error, setError] = useState(false);
    const [mainButtonBool, setMainButtonBool] = useState(true);
    const [emailAlert, setEmailAlert] = useState(false);

    
  useEffect(()=>{
    if( agent.nombreCompleto.length > 0 && agent.direccion.length > 0 && agent.correo.length > 0 &&
      agent.ciudadRemitente.length > 0 && agent.celular.length > 0 && agent.barrio.length > 0 
      && validateEmail(agent.correo)){
        setMainButtonBool(false);
    } else {
      setMainButtonBool(true);
    }
  },[agent])
  useEffect(()=>{
    if(agent.correo.length > 0 && agent.correo !== ""){
    
      if(validateEmail(agent.correo)){
        setEmailAlert(false);
        setMainButtonBool(false);
      } else {
        setEmailAlert(true);
        setMainButtonBool(true);
  
      }
    }
  }, [agent.correo])
   
    const handleClick = ()=>{

        const options = {
            headers: {credentials: 'include'}
          };
        axios.put(ip, {
            Email : props.userInfo.Email,
            Agentes : agent
          }, options)
          .then(function (response) {
    //actualizar el mensaje de que se creo un agente
    store.dispatch(userInfo('set_user_info', response.data.userInfoData));
    store.dispatch(dataInfo('set_data_info', response.data.dataInfo.data));
    setErrorMessage(<Alert className={classes.alertStyles} severity="success">Se generó un agente exitosamente.</Alert>);
    setError(false);
          })
          .catch(function (error) {
            console.log(error)
            const message = error.response.data;
            setErrorMessage(<Alert className={classes.alertStyles} severity="success">{message}.</Alert>);
            setError(true);
          });
    }
    let emailTextField;
    if(emailAlert){
      emailTextField = 
      <TextField required error label="Error"     
      helperText="El correo debe estar en un formato correcto."
      autoComplete='email' id="email" value={agent.correo} 
      onChange={ev=>setAgent({...agent, correo : ev.target.value})} 
        variant="filled" className={classes.control} />
    }
    if(!emailAlert){
      emailTextField = 
      <TextField required autoComplete='email' id="email" 
       value={agent.correo} onChange={ev=>setAgent({...agent, correo : ev.target.value})} 
       label="Correo" variant="filled" className={classes.control} />;
    }
    return(
        <div>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
       Crear Agente
      </Typography>
     <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
    <TextField required value={agent.nombreCompleto} onChange={ev=>setAgent({...agent, nombreCompleto : ev.target.value})}  label="Nombre Completo" variant="filled" className={classes.control} />
    <TextField required value={agent.direccion} onChange={ev=>setAgent({...agent, direccion : ev.target.value})}  label="Dirección" variant="filled" className={classes.control} />
      {emailTextField}
    </Grid>
    </Grid>
    <Grid container direction='row' justify='center' alignItems='center' className={classes.gridWidth} > 
    <TextField required value={agent.ciudadRemitente}  onChange={ev=>setAgent({...agent, ciudadRemitente : ev.target.value})}  label="Ciudad" variant="filled" className={classes.control} />
    <TextField value={agent.fijo}  onChange={ev=>setAgent({...agent, fijo : ev.target.value})}  label="Fijo" variant="filled" type="number" className={classes.control} />
    <TextField required value={agent.celular}  onChange={ev=>setAgent({...agent, celular : ev.target.value})}  label="Celular" type="number" variant="filled" className={classes.control} />
    <TextField required value={agent.barrio}  onChange={ev=>setAgent({...agent, barrio : ev.target.value})}  label="Barrio" variant="filled" className={classes.control} />
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    <Button disabled={mainButtonBool} variant="contained" color="primary" onClick={handleClick}>
  Crear Agente
</Button>
{errorMessage}
    </Grid>
  
    </div>
    ) 
    
}

function mapStateToProps(state) { 
    return {
       userInfo : state.setUserInfo.userInfo
    };
  };
  export default connect(mapStateToProps)(CrearAgente);
  