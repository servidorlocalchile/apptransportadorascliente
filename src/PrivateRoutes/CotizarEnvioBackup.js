import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import SelectInput from '../Atoms/SelectInput';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { FormGroup } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TablaCotizacion from '../Atoms/TablaCotizacion';
import InputAdornment from '@material-ui/core/InputAdornment';
import { makeStyles } from '@material-ui/core/styles';
import Ciudades from '../Data/Ciudades';
import aveonline from '../Apis/aveonline';
import Alert from '@material-ui/lab/Alert';
import Spinner from '../Atoms/Spinner';
import CiudadesAutoComplete from '../Atoms/CiudadesAutoComplete';
import getMunicipiosCiudadesText from '../Apis/getMunicipiosCiudadesText';
import {connect} from 'react-redux';
import { el } from 'date-fns/locale';

const useStyles = makeStyles((theme) => ({
    control: {
        padding: theme.spacing(1),
      },
      gridWidth : {
          width : 'auto'
      },
      formGroup : {
        marginLeft : 5
      }
  }));
const CotizarEnvio = (props)=>{

    const classes = useStyles();
    const [agent, setAgent] = useState({nombreCompleto : ''})
    const [city, setCity] = useState("");
    const [targetCity, setTargetCity] = useState('');
    const [units, setUnits] = useState('');
    const [kilos, setKilos] = useState('');
    const [ancho, setAncho] = useState('');
    const [alto, setAlto] = useState('');
    const [largo, setLargo] = useState('');
    const [seguro, setSeguro] = useState('');
    const [contenido, setContenido] = useState('');
    const [recaudo, setRecaudo] = useState('');
    const [state, setState] = useState({
        checkedA: false,
        checkedB: false
      });
    const [cotizar, setCotizar] = useState(false);
    const [results, setResults] = useState({
      fleteEnvia : 0,
      manejo : 0,
      fletePorRecuado : 0,
      total : 0

    })
    const [mainButtonBool, setMainButtonBool] = useState(false);
    const [transportadorasCoberturas, setTransportadorasCoberturas] = useState([]);
    const [transportadorasCoberturasCopy, setTransportadorasCoberturasCopy] = useState([]);
    const [errorMessage, setErrorMessage] = useState(<div></div>);
    const [spinner, setSpinner] = useState(false);
    const [recaudoRecalculadoBool, setRecaudoRecalculadoBool] = useState(false);
    const [municipioOrigen, setMunicipioOrigen] = useState('');
    const [municipioTarget, setMunicipioTarget] = useState('');
    const [enviaResponseStatus, setEnviaResponseStatus] = useState(false);
    const [ciudadesDane, setCiudadesDane] = useState({});
    const [agregarCotizacionEnvia, setAgregarCotizacionEnvia] = useState(false);
    useEffect(()=>{
      
      const fleteEnvia =  (seguro / 100 ) * 5 ; //5% simulado, esta es la cifra que llega de envia
      const manejo = state.checkedA ? seguro / 100 : 0; //1% del valor asegurado
      const fletePorRecaudo = state.checkedB ? (seguro / 100) / 2 : 0; //0,5% del valor asegurado
      let total = (parseInt(recaudo, 10) + fleteEnvia + manejo + fletePorRecaudo);//.toLocaleString('es-CO');
     // console.log(fleteEnvia, manejo, fletePorRecaudo, recaudo)
     // console.log(total)
      if(Number.isNaN(total)){
        //console.log(total)
       // total = 0;
      } 
      setResults({
        fleteEnvia, manejo, fletePorRecaudo, total
      })
    }, [seguro,  recaudo, state])
  useEffect(()=>{
    //aveces city aparece como "" cuando ya se ha elegido
    if( city.length > 0 && targetCity.length > 0 && units.length > 0 &&
      kilos.length > 0 && seguro.length > 0  && contenido.length > 0){
        setMainButtonBool(false);
    } else {
      setMainButtonBool(true);
    }
  },[agent, city, targetCity,units, kilos, seguro, recaudo , contenido]);
  useEffect(()=>{
    if(agent !== undefined && agent.nombreCompleto.length > 0){
      //se ha seleccionado un agente en la lista de agentes
      setCity(agent.ciudadRemitente);
    } 
  }, [agent])
  useEffect(()=>{
      const getDivView = document.getElementById('spinnerId');
      if(getDivView !== null){
        getDivView.scrollIntoView({behavior: "smooth"});
      }
     
  }, [spinner])

  useEffect(()=>{
    //se activa este useEffect al renderizar por primera vez, filtrar el renderizado con el length de ciudades Dane
    if(Object.keys(ciudadesDane).length > 0){
    

      
    }
    

  }, [ciudadesDane])

 
  useEffect(()=>{
    /*
    if(transportadorasCoberturasCopy.length > 0){
      setErrorMessage(<div></div>);
      setCotizar(true);
      setTimeout(function(){
        const getDivView = document.getElementById('cotizacionIdView');
        setSpinner(false);
        getDivView.scrollIntoView({behavior: "smooth"});
       }, 250);
    }
             */
  
  }, [transportadorasCoberturasCopy])
    const handleCityChange = (event, label) => {
      //aveces la ciudad llega como [""] asi se elija una, reparar esto
      if(event !== undefined  ){
        const arrayId_city = event.split('-');
        const cityFirst = arrayId_city[0].trim();
        const city = arrayId_city[1].substring(1);
        if(label === "Ciudad Origen"){
          setCity(cityFirst);
          setMunicipioOrigen(city);
        } else if( label === "Ciudad Destino") {
          setTargetCity(cityFirst);
          setMunicipioTarget(city);
        }
      }
     
    };
  
    const handleUnitsChange = (event) => {
      setUnits(event.target.value);
    };
    const handleKilosChange = (event) => {
      setKilos(event.target.value);
    };
    const handleAltoChange = (event) => {
      setAlto(event.target.value);
    };
    const handleAnchoChange = (event) => {
      setAncho(event.target.value);
    };
    const handleLargoChange = (event) => {
      setLargo(event.target.value);
    };
    const handleSeguroChange = (event) => {
      setSeguro(event.target.value);
      
    };
    const handleContenidoChange = (event) => {
      setContenido(event.target.value);
    };
    const handleRecaudoChange = (event) => {
        setRecaudo(event.target.value);
    };
   //cotizar envio
    const handleClick = () => {
      setSpinner(true);
      setErrorMessage(<div></div>);
      setCotizar(false);
      //mapear las ciudades elegidas por el usuario al codigo dane
      getMunicipiosCiudadesText(municipioOrigen, municipioTarget)
      .then(resp=>{
        if(resp === -1){
          //no se encontraron los municipios
          setEnviaResponseStatus(false);
        } else {
          //la respuesta contiene los codigos dane del origen y el destino
          setEnviaResponseStatus(true);
         //cuando se obtiene la respuseta de las ciudades es necesario cotizar el envio, preferiblemente
          //en un useEffect
          setCiudadesDane(resp)
        }

      })
      .catch(err=>{
        setEnviaResponseStatus(false);
        console.log(err)
      });
     

     
     
      //aca se debe comunicar con la api de aveonline y arrojar los resultados de fletes
      const destinatarioPagaTransporteFlete = state.checkedA ? 1 : 0;
      const destinatarioPagaRecaudo = state.checkedB ? 1 : 0;

      let transportadorasCobertura = {};
      const dataAuthBody = {
        tipo : 'auth',
        usuario : 'innovacion',
        clave : '019*'
      }
      //autenticacion
      const aveonlinePromiseAuth = aveonline('https://aveonline.co/api/comunes/v1.0/autenticarusuario.php', dataAuthBody);
      aveonlinePromiseAuth.then(resp=>{
        const data = {
          tipo : 'cotizar',
          token : resp.data.token,
          idempresa : resp.data.cuentas[0].usuarios[0].id,
          origen : city,
          destino : targetCity, 
          unidades : parseInt(units),
          kilos : parseInt(kilos),
          valordeclarado : parseInt(seguro),
          contraentrega : destinatarioPagaTransporteFlete, //destinatario paga el costo del transporte 1 true, 0 false
          idasumecosto : destinatarioPagaRecaudo, //destinatario paga el recaudo 1 true, 0 false (si solo selecciona este ese dinero se le cobra al que recibe el paquete, es decir no se factura en el total)
          valorrecaudo :  Number.isNaN(parseInt(recaudo)) ? 0 : parseInt(recaudo)
        };
        /**Si el usuario elije contraentrega (flete) y idasumecosto (el destiantario paga recaudo)
         * entonces al cliente no se le cobra nada y el total se le cobra a la persona que recibe el paquete
         */
        //ATENCION, EN TCC CUANDO CUANDO SE COTIZA O SE HACE UNA GUIA SOLO CON DESTINATARIO PAGA EL FLETE
        //ESTA AGREGANDO 3100 PESOS MAS AL VALOR TOTAL*****
        //TAMBIEN ES NECESARIO MIRAR COMO SE MUESTRA EL TOTAL PORQUE NO ESTA COINCIDIENDO CON EL VALOR DE GUIA
        //NO SE ESTA MOSTRANDO TODO EL VALOR, GUIAS QUE SON DE 8 MIL ESTA MOSTRANDO SOLO 3 MIL ATENCION!!!
        const aveonlinePromise = aveonline('https://aveonline.co/api/nal/v1.0/generarGuiaTransporteNacional.php', data);
        aveonlinePromise.then(resp=>{
          resp.data.cotizaciones.forEach(el=>{
            //asigno si la guia contiene recaudo, destinatarioPagaRecaudo y flete.
            /**
             * promise error TypeError: Cannot read property 'tcc' of undefined
             * al parecer el problema llega por las comisionse que no estan llegando correctamente
             */
    el.flete = destinatarioPagaTransporteFlete;
    el.pagaRecaudo = destinatarioPagaRecaudo;
    el.totalRecaudar =  recaudo !== "" ? true : false;
            const sinCoberturaResponse = '000';
            if(el.total != sinCoberturaResponse){
              //agrego la respuesta a la copia para usarla en la recotizacion
              //la transportadora si tiene cobertura
               //se suman los precios de horenvios
            //2600 al flete
            //establezco una copia del valor total para compararlo despues en las recotizaciones
            el.totalCopy = el.total;
            //establecer que transportadora es para agregar las comisiones
            if(el.nombreTransportadora.startsWith("E")){
              el.fletexkilo = el.fletexkilo + parseInt(props.comisiones.envia.fletePorKilo, 10);
            }
            if(el.nombreTransportadora.startsWith("T")){
              el.fletexkilo = el.fletexkilo + parseInt(props.comisiones.tcc.fletePorKilo, 10);
              /**
               * promise error TypeError: Cannot read property 'tcc' of undefined
               */
            }
            if(el.nombreTransportadora.startsWith("M")){
              el.fletexkilo = el.fletexkilo + parseInt(props.comisiones.mensajerosUrbanos.fletePorKilo, 10);
            }
              //si el usuario agrega recaudo
              if(recaudo !== ""){
                let porcentajeRecaudoHorenvios;
                //se cobra el 1% del valor del recaudo
                if(el.nombreTransportadora.startsWith("E")){
                   porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.envia.porcentajeRecaudo, 10);
                }
                if(el.nombreTransportadora.startsWith("T")){
                  porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.tcc.porcentajeRecaudo, 10);
               }
               if(el.nombreTransportadora.startsWith("M")){
                porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.mensajerosUrbanos.porcentajeRecaudo, 10);
             }
              
               
                el.valorOtrosRecaudos = parseInt(el.valorOtrosRecaudos, 10) + porcentajeRecaudoHorenvios;
                el.porcentajeRecaudoTotal = porcentajeRecaudoHorenvios;
              }   
              //se calcula el total
              el.total = el.fletexkilo + el.valorOtrosRecaudos + el.costoManejo;
              transportadorasCobertura[el.nombreTransportadora] = el;
            } 
          })
          const transportadorasCoberturaKeys = Object.keys(transportadorasCobertura);
          if(transportadorasCoberturaKeys.length > 0){
            //aca antes de mostrar el total al usuario es necesario volver a cotizar si el usuario eligio 
            //flete, el destinatario paga recaudo o las dos
            //establecer si el usuario agrego una o las dos opciones
            /**Si el usuario elije contraentrega (flete) y idasumecosto (el destiantario paga recaudo)
             * entonces al cliente no se le cobra nada y el total se le cobra a la persona que recibe el paquete
             */
           
            //se establece la diferencia entre el total original y el total con las comisiones
            if(destinatarioPagaTransporteFlete === 1 || destinatarioPagaRecaudo === 1){
              //si elije  flete, aplica para cuando las dos opciones tambien son true
               
              setRecaudoRecalculadoBool(true);
              const transportadorasCoberturaValues = Object.values(transportadorasCobertura); 
              transportadorasCoberturaValues.forEach(el=>{
                const diferencia = el.total - el.totalCopy;
                data.valorrecaudo = parseInt(recaudo, 10) + diferencia;
                //establecer si la diferencia debe ser del total con el fletepor recaudo y flete envio o solo flete envio

                if(destinatarioPagaTransporteFlete === 1 || destinatarioPagaRecaudo === 1){
                  //se resta el 1% del flete recaudo para que coincida con la factura final
                    data.valorrecaudo = data.valorrecaudo - el.porcentajeRecaudoTotal;
                }
                //con la diferencia de cada transportadora se debe realizar otra cotizacion para obtener el total que se le va a mostrar al cliente
                //se realiza la recotizacion
                const aveonlinePromise = aveonline('https://aveonline.co/api/nal/v1.0/generarGuiaTransporteNacional.php', data);
                aveonlinePromise.then(answer=>{
                    answer.data.cotizaciones.forEach(ele=>{
                      ele.flete = destinatarioPagaTransporteFlete;
                      ele.pagaRecaudo = destinatarioPagaRecaudo;
                      ele.totalRecaudar =  recaudo !== "" ? true : false;
                              const sinCoberturaResponse = '000';
                              if(ele.total != sinCoberturaResponse){
                                //se itera sobre la respuesta final de cada transportadora, esta es la que va al final

                                  //establecer que transportadora es para agregar las comisiones
            if(ele.nombreTransportadora.startsWith("E")){
              ele.fletexkilo = ele.fletexkilo + parseInt(props.comisiones.envia.fletePorKilo, 10);
            }
            if(ele.nombreTransportadora.startsWith("T")){
              ele.fletexkilo = ele.fletexkilo + parseInt(props.comisiones.tcc.fletePorKilo, 10);
            }
            if(ele.nombreTransportadora.startsWith("M")){
              ele.fletexkilo = ele.fletexkilo + parseInt(props.comisiones.mensajerosUrbanos.fletePorKilo, 10);
            }
                                //y se recalcula las comisiones
                                let porcentajeRecaudoHorenvios;
                                //se cobra el 1% del valor del recaudo
                                if(ele.nombreTransportadora.startsWith("E")){
                                   porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.envia.porcentajeRecaudo, 10);
                                }
                                if(ele.nombreTransportadora.startsWith("T")){
                                  porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.tcc.porcentajeRecaudo, 10);
                               }
                               if(ele.nombreTransportadora.startsWith("M")){
                                porcentajeRecaudoHorenvios = (recaudo / 100) * parseInt(props.comisiones.mensajerosUrbanos.porcentajeRecaudo, 10);
                             }
                             ele.valorOtrosRecaudos = parseInt(ele.valorOtrosRecaudos, 10) + porcentajeRecaudoHorenvios;
                             //se calcula el total
              ele.total = ele.fletexkilo + ele.valorOtrosRecaudos + ele.costoManejo;
              //agregar un nuevo valor que contenga el valorrecaudo recalculado
              //y establecer en datosRemitenteDestinatario si existe, si existe entonces no usar el recaudo original sino el recalculado para
              //generar la guia correctamente

             // ele.recaudoRecalculado = data.valorrecaudo;
              //se sobreescribe transportadorasCobertura para que contengan los nuevos valores de la cotizacion
              transportadorasCobertura[ele.nombreTransportadora] = ele;
              setTransportadorasCoberturas(Object.values(transportadorasCobertura));
              //cotizar con envia
              
              setErrorMessage(<div></div>);
              setCotizar(true);
              setTimeout(function(){
                const getDivView = document.getElementById('cotizacionIdView');
                setSpinner(false);
                getDivView.scrollIntoView({behavior: "smooth"});
               }, 250);
               
                              }
                    })
                })
              })
              setTransportadorasCoberturas(Object.values(transportadorasCobertura));
              //cotizar con envia
              setErrorMessage(<div></div>);
              setCotizar(true);
              setTimeout(function(){
                const getDivView = document.getElementById('cotizacionIdView');
                setSpinner(false);
                getDivView.scrollIntoView({behavior: "smooth"});
               }, 250);

            } /*else if( destinatarioPagaTransporteFlete === 0 && destinatarioPagaRecaudo === 1  ){
              //si solo elijge pagarecaudo (el precio cambia)
            }*/
            else
            {
              console.log('esto no debe ejecutarse')
              setRecaudoRecalculadoBool(false);
              console.log(transportadorasCobertura);
              setTransportadorasCoberturas(Object.values(transportadorasCobertura));
              //cotizar con envia
              
              setErrorMessage(<div></div>);
              setCotizar(true);
              setTimeout(function(){
                const getDivView = document.getElementById('cotizacionIdView');
                setSpinner(false);
                getDivView.scrollIntoView({behavior: "smooth"});
               }, 250);
            }
          
          
          } else{
            //no hay cobertura
            console.log('no hay cobertura', transportadorasCobertura);
            setTimeout(function(){
              const getDivView = document.getElementById('errorMessage');
              if(getDivView !== null && getDivView !== undefined){
                setSpinner(false);
                getDivView.scrollIntoView({behavior: "smooth"});
              }
              
             }, 250);
            setErrorMessage(<Alert style={{width : 'fit-content'}}severity="error">Ciudades sin cobertura</Alert>
            )
          }
        })
        .catch(err=>{
          console.log('promise error', err)
        })
      })
      .catch(err=>{
        console.log('hubo un error con la api de autenticacion', err);
        setSpinner(false);
        if(err.message === "Cannot read property 'token' of undefined"){
          setErrorMessage(<Alert style={{width : 'fit-content'}}severity="error">No hay conexión a internet, por favor revise que está conectado a la red.</Alert>);      
        } else {
          setErrorMessage(<Alert style={{width : 'fit-content'}}severity="error">{err.message}</Alert>);      
        }

      })
     
     
    };
    const selectInputChange = (ev) => setAgent(ev);
    const handleTotalChange = () => {
     // setTotal(true);
      //setRecaudo(total)
    };
    const handleChangeCheck = (event) => {
      setState({ ...state, [event.target.name]: event.target.checked });
    };
    let formControlLabelDiv;
    //la opcion recaudo solo esta disponible si se agrega un valor al input recaudo mayor a 0
    if(recaudo > 0){
     formControlLabelDiv =    <FormControlLabel
        control={<Checkbox checked={state.checkedB} onChange={handleChangeCheck} name="checkedB" />}
        label="El Destinatario Paga el Recaudo"
      />;
    
    } else {
      formControlLabelDiv = <FormControlLabel
      control={<Checkbox checked={state.checkedB} onChange={handleChangeCheck} name="checkedB" />}
      label="El Destinatario Paga el Recaudo"
      disabled
    />;
    }
   return(
        <div>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
       1. Datos Generales
      </Typography>
      <Grid container justify='center' alignItems='center'  className={classes.gridWidth}>
    <SelectInput selectInputChange={selectInputChange}/>
    <CiudadesAutoComplete label="Ciudad Origen"  updateChildrenCityChange={handleCityChange} />
    <CiudadesAutoComplete label="Ciudad Destino" updateChildrenCityChange={handleCityChange}  />
    {/*<Ciudades label="Ciudad Origen" variant="filled" updateChildrenCityChange={handleCityChange} />
    <Ciudades  label="Ciudad Destino" variant="filled" updateChildrenCityChange={handleCityChange}  />*/}
</Grid>
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Datos de la Mercancía
      </Typography>
      <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
    <TextField required value={units} onChange={handleUnitsChange} label="Unidades" variant="filled" type='number'className={classes.control} />
    <TextField required id="standard-start-adornment" value={kilos} onChange={handleKilosChange} label="Kilos" variant="filled" type='number'className={classes.control}
     InputProps={{
      startAdornment: <InputAdornment position="start">Kg</InputAdornment>,
    }} />
    <TextField required value={alto} onChange={handleAltoChange} label="Alto en cm" variant="filled" className={classes.control} />
    <TextField required value={ancho} onChange={handleAnchoChange}  label="Ancho en cm" variant="filled" className={classes.control} />
    <TextField required value={largo} onChange={handleLargoChange}  label="Largo en cm" variant="filled" className={classes.control} />
    </Grid>
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Valores
      </Typography>
      <Grid container  justify='center' alignItems='center' className={classes.gridWidth }>
    <TextField required id="standard-adornment-amount" value={seguro} onChange={handleSeguroChange}   label="Valor del Seguro" variant="filled" type='number'className={classes.control} 
      InputProps={{
        startAdornment: <InputAdornment position="start">$</InputAdornment>,
      }}/>
    <TextField required value={contenido} onChange={handleContenidoChange}  label="Contenido" variant="filled" className={classes.control} />
   
 <TextField  id="standard-adornment-amount"  value={recaudo} onChange={handleRecaudoChange} label="Valor de Recaudo" variant="filled" type='number'className={classes.control}
     InputProps={{
      startAdornment: <InputAdornment position="start">$</InputAdornment>,
    }} />
    {/*cotizar ? <TextField disabled={true} id="standard-adornment-amount"   value={results.total.toLocaleString('es-CO')} label="Total" onChange={handleTotalChange}variant="filled" className={classes.control}
     InputProps={{
      startAdornment: <InputAdornment position="start">$</InputAdornment>,
    }} />
    : null */}
   
    <FormGroup className={classes.formGroup} row>
      <FormControlLabel
        control={<Checkbox checked={state.checkedA} onChange={handleChangeCheck} name="checkedA" />}
        label="El Destinatario Paga el Flete"
      />
       {formControlLabelDiv}
      </FormGroup >
    </Grid>
    </Grid>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    <Button disabled={mainButtonBool} variant="contained" color="primary" onClick={handleClick}>
  Cotizar Envío
</Button>
    </Grid>
    {cotizar ?
     <TablaCotizacion 
    transportadorasCobertura={transportadorasCoberturas}
     stateProps={
      {agent, city, targetCity, units, kilos, ancho, alto, largo, seguro, contenido, recaudo, state, cotizar,  
        results, state, recaudoRecalculadoBool, props
      }}
     /> :
    null
    }
    {spinner ? <Spinner  />: null}
    <Grid style={{marginTop : 20}} id="errorMessage"container justify="center">
    {errorMessage}
    </Grid>
   
    </div>
    ) 
    
}



function mapStateToProps(state) { 
  return {
     comisiones : state.setDataInfo.dataInfo.comisiones
  };
};
export default connect(mapStateToProps)(CotizarEnvio);
