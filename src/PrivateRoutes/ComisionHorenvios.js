import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import store from '../Redux/store';
import {userInfo, dataInfo} from '../Redux/actions';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import {connect} from 'react-redux';
import {validateEmail} from '../Utils/useFullFunctions';
import Spinner from '../Atoms/Spinner';

let ip = '';
 if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  // dev code
  ip = 'https://localhost/editarComision';
} else {
  // production code
  ip = `${process.env.REACT_APP_URL}/editarComision`;
  //;

}
const useStyles = makeStyles((theme) => ({
    control: {
        padding: theme.spacing(1),
      },
      gridWidth : {
          width : 'auto'
      },     
  alertStyles : {
    width: 'fit-content',
    margin: 'auto'
  }
  }));
const ComisionHorenvios = (props)=>{

    const classes = useStyles();
    const [comisiones, setComisiones] = useState({
      envia : {
        fletePorKilo : '',
        porcentajeRecaudo : ''
      },
      tcc : {
        fletePorKilo : '',
        porcentajeRecaudo : ''
      },
      mensajerosUrbanos : {
        fletePorKilo : '',
        porcentajeRecaudo : ''
      }
    });
    const [errorMessage, setErrorMessage] = useState('');
    const [errorMessageTcc, setErrorMessageTcc] = useState('');
    const [errorMessageMensajeros, setErrorMessageMensajeros] = useState('');
    const [mainButtonBool, setMainButtonBool] = useState(true);
    const [mainButtonBoolTcc, setMainButtonBoolTcc] = useState(true);
    const [mainButtonBoolMensajeros, setMainButtonBoolMensajeros] = useState(true);
    const [spinner, setSpinner] = useState(false);
    const [spinnerTcc, setSpinnerTcc] = useState(false);
    const [spinnerMensajeros, setSpinnerMensajeros] = useState(false);

    
  useEffect(()=>{
    if( comisiones.envia.fletePorKilo.length > 0 && comisiones.envia.porcentajeRecaudo.length > 0 ){
        setMainButtonBool(false);
    } else {
      setMainButtonBool(true);
    }
  },[comisiones.envia]);
  useEffect(()=>{
    if( comisiones.tcc.fletePorKilo.length > 0 && comisiones.tcc.porcentajeRecaudo.length > 0 ){
        setMainButtonBoolTcc(false);
    } else {
      setMainButtonBoolTcc(true);
    }
  },[comisiones.tcc]);
  useEffect(()=>{
    if( comisiones.mensajerosUrbanos.fletePorKilo.length > 0 && comisiones.mensajerosUrbanos.porcentajeRecaudo.length > 0 ){
        setMainButtonBoolMensajeros(false);
    } else {
      setMainButtonBoolMensajeros(true);
    }
  },[comisiones.mensajerosUrbanos])
  useEffect(()=>{
    const getDivView = document.getElementById('spinnerId');
    if(getDivView !== null){
      getDivView.scrollIntoView({behavior: "smooth"});
    }
   
}, [spinner])
   
      const handleClick = (transportadora)=>{
      //revisar que campos se editaron para no enviar strings vacios a las comisiones en basae de datos
      let comisionAenviar;
      if(transportadora === "Envia"){
        comisionAenviar =  {envia :  comisiones.envia}
        setSpinner(true);
      }
      if(transportadora === "TCC"){
        comisionAenviar = { tcc : comisiones.tcc}
        setSpinnerTcc(true);
      }
      if(transportadora === "Mensajeros"){
        comisionAenviar =  { mensajerosUrbanos : comisiones.mensajerosUrbanos };
        setSpinnerMensajeros(true);
      }
        const options = {
            headers: {credentials: 'include'}
          };
        axios.put(ip, {
            Email : props.userInfo.Email,
            Comisiones : comisionAenviar
          }, options)
          .then(function (response) {
    //actualizar el mensaje de que se creo un agente
    store.dispatch(userInfo('set_user_info', response.data.userInfoData));
    store.dispatch(dataInfo('set_data_info', response.data.dataInfo.data));
     setTimeout(()=>{
      if(transportadora === "Envia"){
        setErrorMessage(<Alert className={classes.alertStyles} severity="success">Las comisiones de Envia han sido atualizadas.</Alert>);
        setSpinner(false);
      }
      if(transportadora === "TCC"){
        setErrorMessageTcc(<Alert className={classes.alertStyles} severity="success">Las comisiones de TCC han sido atualizadas.</Alert>)
        setSpinnerTcc(false);
      }
      if(transportadora === "Mensajeros"){
        setErrorMessageMensajeros(<Alert className={classes.alertStyles} severity="success">Las comisiones de Mensajeros Urbanos han sido atualizadas.</Alert>)
        setSpinnerMensajeros(false);
      }
     }, 2000)       
   
    //setError(false);
          })
          .catch(function (error) {
            let message;
            if(error.message === "Network Error"){
              message = "No hay conexión a internet, por favor intente más tarde."
            } else {
              message = error.response.data;
            } 
            setTimeout(()=>{
              if(transportadora === "Envia"){
                setErrorMessage(<Alert className={classes.alertStyles} severity="error">{message}.</Alert>);
                setSpinner(false);
  
              }
              if(transportadora === "TCC"){            
                setErrorMessageTcc(<Alert className={classes.alertStyles} severity="error">{message}.</Alert>);
                setSpinnerTcc(false);
              }
              if(transportadora === "Mensajeros"){
                setErrorMessageMensajeros(<Alert className={classes.alertStyles} severity="error">{message}.</Alert>);
                setSpinnerMensajeros(false);
              }
            }, 2000)
           
           ;
        // setError(true);
          });
    }
 
  
    return(
        <div>
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Editar Comisión Horenvios
      </Typography>
     <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
    <TextField required value={comisiones.envia.fletePorKilo} onChange={ev=>setComisiones({...comisiones, envia : {
        fletePorKilo : ev.target.value,
        porcentajeRecaudo : comisiones.envia.porcentajeRecaudo
    }})}  label="Comisión Flete" variant="filled" className={classes.control} />
    <TextField required value={comisiones.envia.porcentajeRecaudo} onChange={ev=>setComisiones({...comisiones, envia : {
      fletePorKilo : comisiones.envia.fletePorKilo,
      porcentajeRecaudo : ev.target.value,
      
    }})}  label="Porcentaje Recaudo" variant="filled" className={classes.control} />
    </Grid>
    </Grid>
    
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    {spinner ? <Spinner  />:  <Button disabled={mainButtonBool} variant="contained" color="primary" onClick={()=>handleClick('Envia')}>
  Cambiar Comisión Envia
</Button>}
   
{errorMessage}
    </Grid>
    
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Editar Comisión TCC
      </Typography>
     <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
    <TextField required value={comisiones.tcc.fletePorKilo} onChange={ev=>setComisiones({...comisiones, tcc : {
        fletePorKilo : ev.target.value,
        porcentajeRecaudo : comisiones.tcc.porcentajeRecaudo
    }})}  label="Comisión Flete" variant="filled" className={classes.control} />
    <TextField required value={comisiones.tcc.porcentajeRecaudo} onChange={ev=>setComisiones({...comisiones, tcc : {
      fletePorKilo : comisiones.tcc.fletePorKilo,
      porcentajeRecaudo : ev.target.value,
      
    }})}  label="Porcentaje Recaudo" variant="filled" className={classes.control} />
    </Grid>
    </Grid>
    
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    {spinnerTcc ? <Spinner  />:  <Button disabled={mainButtonBoolTcc} variant="contained" color="primary" onClick={()=>handleClick("TCC")}>
  Cambiar Comisión TCC
</Button>}
   
{errorMessageTcc}
    </Grid>

    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} > 
    <Typography variant="h6" gutterBottom>
        Editar Comisión Mensajeros Urbanos
      </Typography>
     <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
    <TextField required value={comisiones.mensajerosUrbanos.fletePorKilo} onChange={ev=>setComisiones({...comisiones, mensajerosUrbanos : {
        fletePorKilo : ev.target.value,
        porcentajeRecaudo : comisiones.mensajerosUrbanos.porcentajeRecaudo
    }})}  label="Comisión Flete" variant="filled" className={classes.control} />
    <TextField required value={comisiones.mensajerosUrbanos.porcentajeRecaudo} onChange={ev=>setComisiones({...comisiones, mensajerosUrbanos : {
      fletePorKilo : comisiones.mensajerosUrbanos.fletePorKilo,
      porcentajeRecaudo : ev.target.value,
      
    }})}  label="Porcentaje Recaudo" variant="filled" className={classes.control} />
    </Grid>
    </Grid>
    
    <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    {spinnerMensajeros ? <Spinner  />: <Button disabled={mainButtonBoolMensajeros} variant="contained" color="primary" onClick={()=>handleClick("Mensajeros")}>
  Cambiar Comisión Mensajeros Urbanos
</Button>}
    
{errorMessageMensajeros}
    </Grid>
    </div>
    ) 
    
}

function mapStateToProps(state) { 
    return {
       userInfo : state.setUserInfo.userInfo
    };
  };
  export default connect(mapStateToProps)(ComisionHorenvios);
  