import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import AssignmentIcon from '@material-ui/icons/Assignment'; //carta, acuerdo, camara
import FingerprintIcon from '@material-ui/icons/Fingerprint'; //cedula
import FolderSharedIcon from '@material-ui/icons/FolderShared';//rutnit
import Tooltip from '@material-ui/core/Tooltip';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import {connect} from 'react-redux';

let ip = '';
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    // dev code
    ip = 'https://localhost/enviarDocumentos';
} else {
    // production code
    ip = `${process.env.REACT_APP_URL}/enviarDocumentos`;
    //;

}
const useStyles = makeStyles((theme) => ({
    control: {
        padding: theme.spacing(1),
    },
    gridWidth: {
        width: 'auto',
    },
    alertStyles: {
        width: 'fit-content',
        margin: 'auto'
    },
    buttonStyles : {
        margin : 5
    }
}));
const SubirDocumentos = (props) => {

    const classes = useStyles();
    const [errorMessage, setErrorMessage] = useState('');
    const [error, setError] = useState(false);
    const [mainButtonBool, setMainButtonBool] = useState(true);
    const [docUploaded, setDocUploaded] = useState({
        subirCedula : false,
        subirRutNit : false,
        cartaAutorizacion : false,
        acuerdoComercial : false,
        camaraComercio : false
    });
    const [success, setSuccess] = useState(false);
    useEffect(() => {
        if (docUploaded.subirCedula && docUploaded.subirRutNit &&
            docUploaded.cartaAutorizacion && docUploaded.acuerdoComercial) {
            setMainButtonBool(false);
        } else {
            setMainButtonBool(true);
        }
    }, [docUploaded])
    const handleChange = (ev)=>{
        const id = ev.target.id;
        switch(id) {
            case 'subirCedula':
                setDocUploaded({...docUploaded, subirCedula : true});
              break;
            case 'subirRutNit':
              setDocUploaded({...docUploaded, subirRutNit : true});
              break;
            case 'cartaAutorizacion':
                setDocUploaded({...docUploaded, cartaAutorizacion : true});
                break;
            case 'acuerdoComercial':
                setDocUploaded({...docUploaded, acuerdoComercial : true});
                break;
            case 'camaraComercio': 
            setDocUploaded({...docUploaded, camaraComercio : true});
                break;
            default:
          }
    }
    const handleClick = () => {
        const userEmail = {
            Email: props.userInfo.Email
        } 
        const json = JSON.stringify(userEmail);
        const blob = new Blob([json], {
            type: 'application/json'
          });
        const formData = new FormData();
        const archivoCedula = document.getElementById('subirCedula');
        const archivoRutNit = document.getElementById('subirRutNit');
        const archivoCarta = document.getElementById('cartaAutorizacion');
        const archivoAcuerdo = document.getElementById('acuerdoComercial');
        const archivoCamara = document.getElementById('camaraComercio');
        formData.append("usuario", blob);
        formData.append("documentos", archivoCedula.files[0]);
        formData.append("documentos", archivoRutNit.files[0]);
        formData.append("documentos", archivoCarta.files[0]);
        formData.append("documentos", archivoAcuerdo.files[0]);
        formData.append("documentos", archivoCamara.files[0]);

        const options = {
            headers: { credentials: 'include', 
            'Content-Type': 'multipart/form-data'
        }
        };
        axios.post(ip, formData, options)
            .then(function (response) {
                setErrorMessage('');
                setError(false);
                setSuccess(true);
            })
            .catch(function (error) {
                setErrorMessage(error.message);
                setError(true);
                setSuccess(false);
            });
    }
    const subirCedula = <label className={classes.buttonStyles} htmlFor="subirCedula">
    <input
        style={{ display: 'none' }}
        id="subirCedula"
        name="subirCedula"
        type="file"
        onChange={handleChange}
    />
    <Button startIcon={<FingerprintIcon />} 

    endIcon={docUploaded.subirCedula ? <CheckCircleIcon/> : null} color="secondary" variant="contained" component="span">
        Cédula de Ciudadania
</Button>
</label>;
    const subirRutNit = <label className={classes.buttonStyles} htmlFor="subirRutNit">
        <input
            style={{ display: 'none' }}
            id="subirRutNit"
            name="subirRutNit"
            type="file"
            onChange={handleChange}
        />
        <Button startIcon={<FolderSharedIcon />} endIcon={docUploaded.subirRutNit ? <CheckCircleIcon/> : null} color="secondary" variant="contained" component="span">
            Rut o Nit
  </Button>
    </label>;
    const cartaAutorizacion = <label className={classes.buttonStyles}  htmlFor="cartaAutorizacion">
        <input
            style={{ display: 'none' }}
            id="cartaAutorizacion"
            name="cartaAutorizacion"
            type="file"
            onChange={handleChange}
        />
        <Button startIcon={<AssignmentIcon />} endIcon={docUploaded.cartaAutorizacion ? <CheckCircleIcon/> : null} color="secondary" variant="contained" component="span">
            Carta Autorización de Recaudo
   </Button>
    </label>;
    const acuerdoComercial = <label className={classes.buttonStyles} htmlFor="acuerdoComercial">
        <input
            style={{ display: 'none' }}
            id="acuerdoComercial"
            name="acuerdoComercial"
            type="file"
            onChange={handleChange}
        />
              <Tooltip title="Subir solo si es persona jurídica">
        <Button startIcon={<AssignmentIcon />} endIcon={docUploaded.acuerdoComercial ? <CheckCircleIcon/> : null} color="secondary" variant="contained" component="span">
            Acuerdo Comercial
  </Button>
  </Tooltip>

    </label>;
    const camaraComercio = <label className={classes.buttonStyles} htmlFor="camaraComercio">
        <input
            style={{ display: 'none' }}
            id="camaraComercio"
            name="camaraComercio"
            type="file"
            onChange={handleChange}
        />

        <Button startIcon={<AssignmentIcon />} endIcon={docUploaded.camaraComercio ? <CheckCircleIcon/> : null}color="secondary" variant="contained" component="span">
            Cámara y Comercio
  </Button>
    </label>;

const documentosSubidosExito = <Alert className={classes.alertStyles} severity="success">Los documentos se subieron correctamente. Una vez se aprueben su cuenta quedará habilitada.</Alert>;
if(!success){
        return (
            <div>
                <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
                    <Typography variant="h6" gutterBottom>
                        Subir Documentos
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
                       Para habilitar su cuenta es necesario que suba los siguientes documentos, tenga en cuenta
                       que el documento de cámara y comercio solo es necesario si usted es persona jurídica.
          </Typography>
                    <Grid container  justify='center' alignItems='center' className={classes.gridWidth}>
                        {subirCedula} {subirRutNit} {cartaAutorizacion}
                    </Grid>
                </Grid>
                <Grid container direction='row' justify='center' alignItems='center' className={classes.gridWidth} >
                {acuerdoComercial} {camaraComercio}
                  </Grid>
                <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
    
                    <Button 
                        variant="contained" color="primary" onClick={handleClick}
                        disabled={mainButtonBool}
                        className={classes.buttonStyles}
                        startIcon={<CloudUploadIcon />}
                    >
                        Subir Documentos
              </Button>
                    {error ? <Alert className={classes.alertStyles} severity="error">{errorMessage}.</Alert>
                        : null
                    }
                </Grid>
    
            </div>
        )
    } else {   
      return  documentosSubidosExito;
    }
    

}

function mapStateToProps(state) { 
    return {
       userInfo : state.setUserInfo.userInfo
    };
  };
  export default connect(mapStateToProps)(SubirDocumentos);
  