import React, { useEffect, useState } from "react";
import axios from "axios";
import store from "../Redux/store";
import { userInfo, dataInfo } from "../Redux/actions";
import LogoHorenvios from "../Resources/Images/LogoHorenvios.png";
import { makeStyles, lighten } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import clsx from "clsx";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import BusquedaRelacionEnvios from "../Atoms/BusquedaRelacionEnvios";
import Alert from "@material-ui/lab/Alert";
import { jsPDF } from "jspdf";
import "jspdf-autotable";
import {
  registrarHoraActual,
  generarNumeroGuia,
} from "../Utils/useFullFunctions";
import { connect } from "react-redux";
import aveonline from "../Apis/aveonline";
import CircularProgress from "@material-ui/core/CircularProgress";
import enviaConsultaGuia from "../Apis/enviaConsultaGuia";

let ip = "";
if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
  // dev code
  ip = "https://localhost/actualizarRelacionGuias";
} else {
  // production code
  ip = `${process.env.REACT_APP_URL}/actualizarRelacionGuias`;
  //;
}

function createData(
  guia,
  orden,
  transportadora,
  agente,
  origenDestino,
  fecha,
  peso,
  tipo,
  cobertura,
  diasEntrega,
  valorDeclarado,
  fleteTransportadora,
  fletePorRecaudo,
  flete,
  valorRecaudo,
  estado,
  total
) {
  return {
    guia,
    orden,
    transportadora,
    agente,
    origenDestino,
    fecha,
    peso,
    tipo,
    cobertura,
    diasEntrega,
    valorDeclarado,
    fleteTransportadora,
    fletePorRecaudo,
    flete,
    valorRecaudo,
    estado,
    total,
  };
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: "guia", numeric: false, disablePadding: true, label: "Guía" },
  {
    id: "orden",
    numeric: false,
    disablePadding: false,
    label: "Órden de Compra",
  },
  {
    id: "transportadora",
    numeric: false,
    disablePadding: false,
    label: "Transportadora",
  },
  { id: "agente", numeric: false, disablePadding: false, label: "Agente" },
  {
    id: "origenDestino",
    numeric: false,
    disablePadding: false,
    label: "Origen / Destino",
  },
  { id: "fecha", numeric: false, disablePadding: false, label: "Fecha" },
  { id: "peso", numeric: true, disablePadding: false, label: "Peso" },
  { id: "tipo", numeric: false, disablePadding: false, label: "Tipo de Envío" },
  {
    id: "cobertura",
    numeric: false,
    disablePadding: false,
    label: "Cobertura",
  },
  {
    id: "diasEntrega",
    numeric: false,
    disablePadding: false,
    label: "Días Entrega",
  },
  {
    id: "valorDeclarado",
    numeric: true,
    disablePadding: false,
    label: "Valor Declarado",
  },
  {
    id: "fleteTransportadora",
    numeric: false,
    disablePadding: false,
    label: "Flete Transportadora",
  },
  {
    id: "fletePorRecaudo",
    numeric: false,
    disablePadding: false,
    label: "Flete Por Recaudo",
  },
  { id: "flete", numeric: false, disablePadding: false, label: "Manejo" },
  {
    id: "valorRecaudo",
    numeric: false,
    disablePadding: false,
    label: "Valor Recaudo",
  },
  { id: "estado", numeric: false, disablePadding: false, label: "Estado" },
  { id: "total", numeric: true, disablePadding: false, label: "Total" },
];

function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ "aria-label": "select all desserts" }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            style={{ fontWeight: 700 }}
            key={headCell.id}
            align={headCell.numeric ? "center" : "center"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: "1 1 100%",
  },
}));
//boton que se muestra cuando se selecciona una guia o varias guias
const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { numSelected } = props;
  const buttonClick = () => {
    const columns = [
      { title: "#Guía", dataKey: "guia" },
      { title: "Nombre del Remitente", dataKey: "nombreRemitente" },
      { title: "Ciudad Remitente", dataKey: "ciudadRemitente" },
      { title: "Nombre del Destinatario", dataKey: "nombreDestinatario" },
      { title: "Ciudad Destinatario", dataKey: "ciudadDestinatario" },
      { title: "Fecha", dataKey: "fecha" },
      { title: "Dirección", dataKey: "direccion" },
      { title: "Teléfono", dataKey: "telefono" },
      { title: "Peso", dataKey: "peso" },
      { title: "Valor Declarado", dataKey: "valorDeclarado" },
      { title: "Observaciones", dataKey: "observaciones" },
      { title: "Total", dataKey: "total" },
    ];
    const guiasArray = props.allGuiasArray.filter((el) => {
      if (props.guiasIds.includes(el.numeroGuia)) {
        //el arreglo de seleccion si incluye el numero de guia
        return el;
      }
    });
    const guiasQueNoSeGeneraron = props.allGuiasArray.filter((el) => {
      if (!props.guiasIds.includes(el.numeroGuia)) {
        //el arreglo de seleccion no incluye el numero de guia
        return el;
      }
    });
    const rows = guiasArray.map((el) => {
      let horCode =
        el.agentInfo.nombreCompleto.length > 0
          ? " Código SubAgente: " + el.agentInfo.HORCODE
          : "";
      let total = el.datosCotizacion.total;
      if (el.datosCotizacion.total.length === 3) {
        total =
          el.datosCotizacion.total[0] +
          " Cobro a Dest: " +
          el.datosCotizacion.total[2];
        if (el.informacionAdicional.detalles.state.checkedB) {
          //destinatario paga recaudo
          total =
            el.datosCotizacion.total[0] +
            " Cobro a Dest: " +
            el.datosCotizacion.total[2] +
            " Dest paga Recaudo: " +
            el.datosCotizacion.recaudo;
        }
        if (
          !el.informacionAdicional.detalles.state.checkedB &&
          el.datosCotizacion.recaudo !== ""
        ) {
          //usuario paga recaudo
          total =
            el.datosCotizacion.total[0] +
            " (Recaudo) " +
            el.datosCotizacion.recaudo +
            " Cobro a Dest: " +
            el.datosCotizacion.total[2];
        }
      }
      return {
        guia: el.numeroGuia,
        nombreRemitente: el.datosRemitente.nombreCompleto + horCode,
        ciudadRemitente: el.datosRemitente.ciudadRemitente,
        nombreDestinatario: el.datosDestinatario.nombreCompleto,
        ciudadDestinatario: el.datosDestinatario.ciudadDestinatario,
        fecha: el.horaActual,
        direccion: el.datosDestinatario.direccion,
        telefono: el.datosDestinatario.celular,
        peso: el.datosCotizacion.kilos,
        valorDeclarado: el.datosCotizacion.valoracion,
        observaciones: el.informacionAdicional.detalles.contenido,
        total: total,
        //
      };
    });
    //crear el pdf
    const doc = new jsPDF({
      orientation: "landscape",
    });
    const numeroRelacion = generarNumeroGuia(1000000000);
    doc.autoTable(columns, rows, {
      styles: { fillColor: "darkgray", fontSize: 8 },
      headStyles: { fillColor: "#303f9f" },
      margin: { top: 60 },
      beforePageContent: function (data) {
        const horizontalPositionPlus = 50;
        const fecha = registrarHoraActual().horaActualLegible;
        doc.text(fecha, 20, 20);
        doc.setFont("helvetica", "bold");
        doc.text("Horenvios", 85 + horizontalPositionPlus, 20);
        doc.setFont("times", "normal");
        doc.setFontSize(10);
        doc.text(
          "Relación de Envío #" + numeroRelacion,
          70 + horizontalPositionPlus,
          30
        );
        //cambiar logoEnvia por la transportadora que se elija
        doc.addImage(
          LogoHorenvios,
          "PNG",
          15 + horizontalPositionPlus,
          30,
          15,
          15
        );
        doc.setFont("helvetica", "bold");
        doc.text("Fecha:", 50 + horizontalPositionPlus, 42);
        doc.setFont("times", "normal");
        doc.text(fecha, 62 + horizontalPositionPlus, 42);
        doc.setFont("helvetica", "bold");
        doc.text("Código HOR:", 110 + horizontalPositionPlus, 42);
        doc.setFont("times", "normal");
        doc.text(props.horCode, 132 + horizontalPositionPlus, 42);
        doc.setFont("helvetica", "bold");
      },
    });
    doc.save(numeroRelacion + ".pdf");
    //despues de guardar el pdf es necesario borrar las guias seleccionadas en guiasInfoRelacion
    //para que no aparezcan mas en esta vista de relacion de guias
    //enviar la info a el servidor
    const options = {
      headers: { credentials: "include" },
    };
    axios
      .put(
        ip,
        {
          Email: props.userInfo.Email,
          GuiasInfoRelacion: guiasQueNoSeGeneraron,
        },
        options
      )
      .then(function (response) {
        //actualizar la info sin las guias que se generaron
        store.dispatch(userInfo("set_user_info", response.data.userInfo));
        store.dispatch(dataInfo("set_data_info", response.data.dataInfo));

        //recargar la vista
        props.updateRelacionView();
      })
      .catch(function (error) {
        console.log("hubo un error", error.response);
      });
  };
  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography
          className={classes.title}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} Seleccionado{numSelected > 1 ? "s" : null}
        </Typography>
      ) : (
        <Typography
          align="center"
          className={classes.title}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Relación de Guías
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Generar Relación de Guías">
          <Button onClick={buttonClick} variant="outlined" color="secondary">
            Generar Relación de Guías
          </Button>
        </Tooltip>
      ) : null}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  alertStyles: {
    width: "fit-content",
    margin: "auto",
  },
}));

function RelacionGuias(props) {
  const classes = useStyles();
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("fecha");
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [dense, setDense] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [showGuias, setShowGuias] = useState(false);
  const [guiasArray, setGuiasArray] = useState([]);
  const [showAlertDate, setShowAlertDate] = useState(false);
  const [messageTranportadoraEmpty, setMessageTransportadoraEmpty] =
    useState("");
  const [transportadoraName, setTransportadoraName] = useState("");
  const [spinner, setSpinner] = useState(false);
  const [rows, setRows] = useState([]);
  const [alert, setAlert] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [iframeDiv, setIframeDiv] = useState(false);
  const [mostrarRows, setMostrarRows] = useState({
    estado: false,
    finalRows: [],
  });

  useEffect(() => {
    setSpinner(false);
  }, [rows]);
  const guardarListadoGuiasEnServidor = (array, noCancelar) => {
    if (!noCancelar) {
      setSpinner(true);
    }

    const options = {
      headers: { credentials: "include" },
    };
    axios
      .put(
        ip,
        {
          Email: props.userInfo.Email,
          GuiasInfo: array,
          GuiasInfoRelacion: array,
          Sobreescribir: true,
        },
        options
      )
      .then(function (response) {
        //en vez de usar crear guias modificar userInfo con la nueva info que llega del servidor

        if (!noCancelar) {
          setTimeout(() => {
            setSpinner(false);
            setAlert("success");
            setErrorMessage("La guia se ha cancelado.");
            const getDivView = document.getElementById("errorMessage");
            setIframeDiv(<div></div>);
            if (getDivView !== null && getDivView !== undefined) {
              getDivView.scrollIntoView({ behavior: "smooth" });
            }
            setTimeout(() => {
              store.dispatch(userInfo("set_user_info", response.data.userInfo));
              store.dispatch(dataInfo("set_data_info", response.data.dataInfo));
              //history.push('/Listado de Guías')
            }, 2000);
          }, 2000);
        }
      })
      .catch(function (error) {
        setSpinner(false);
        console.log(
          "hubo un error al comunicarse con el servidor local",
          error.response
        );
        setAlert("error");
        setErrorMessage(
          "Hubo un error al cancelar las guias, revisa tu conexión a internet o intenta más tarde."
        );
        setTimeout(() => {
          const getDivView = document.getElementById("errorMessage");
          if (getDivView !== null && getDivView !== undefined) {
            getDivView.scrollIntoView({ behavior: "smooth" });
          }
        }, 250);
      });
  };
  /**Muestra condicionalmente las guias dependiendo de la seleccion en BusquedaRelacionEnvios.js */
  const updateData = (transportadora, initialDate, finalDate) => {
    if (transportadora === "") {
      setMessageTransportadoraEmpty(
        "Por favor elija una transportadora en 1. Seleccione Transportadora"
      );
      return;
    }
    //revisar que el usuario no haya generado una relacion de guias, si es asi
    //mostrar solo las guas que no se han generado en relacion

    let guiasInfoArray = props.userInfo.GuiasInfoRelacion;
    //es el arreglo que contiene las guias que se deben actualizar
    let newInfoArray = [];
    guiasInfoArray.forEach((el) => {
      const transportadoraFromArray = el.datosCotizacion.transportadora;
      //se agregan las guias que se encuentran en la fecha seleccionada por el usuario
      if (
        transportadoraFromArray === transportadora ||
        transportadora === "Seleccionar Todas las Transportadoras"
      ) {
        //la transportadora conicide, revisar también si la fecha es correcta
        const initialDateObject = new Date(initialDate);
        const finalDateObject = new Date(finalDate);
        const dateFromArrayElement = new Date(el.horaActualFormatoOriginal);
        //revisar si la hora esta en el rango
        if (
          dateFromArrayElement.getTime() > initialDateObject.getTime() &&
          dateFromArrayElement.getTime() < finalDateObject
        ) {
          newInfoArray.push(el);
          /* setShowAlertDate(false);
                setShowGuias(true);
                setTransportadoraName(transportadora);
                setMessageTransportadoraEmpty('')*/
        } else {
          //mostrar un aviso que indique que no hay guias en las fechas especificadas
          // setShowAlertDate(true);
          //setShowGuias(false);
          //setTransportadoraName('');
          // setMessageTransportadoraEmpty('');
        }
      }
    });
    if (newInfoArray.length > 0) {
      //se agregaron guias en las fechas especificadas
      setShowAlertDate(false);
      setShowGuias(true);
      setSpinner(true);
      setTransportadoraName(transportadora);
      setMessageTransportadoraEmpty("");
      let rowsEffect = [];
      //antes de actualizar guiasArray es necesario obtener el estado actualizado de las apis
      const guiasGeneradas = newInfoArray.filter((guias) =>
        guias.estado.startsWith("G")
      );
      guiasGeneradas.forEach((el) => {
        const agentNameAndCode =
          el.agentInfo.nombreCompleto.length > 0
            ? el.agentInfo.nombreCompleto + " / Código: " + el.agentInfo.HORCODE
            : props.userInfo.HORCODE;
        rowsEffect.push(
          createData(
            el.numeroGuia,
            el.datosRemitente.ordenDeCompra,
            el.datosCotizacion.transportadora,
            agentNameAndCode,
            el.datosCotizacion.origenDestino,
            el.horaActual,
            el.datosCotizacion.kilos,
            el.datosCotizacion.tipoEnvio,
            el.datosCotizacion.trayecto,
            el.datosCotizacion.diasEntrega,
            el.datosCotizacion.valoracion,
            el.datosCotizacion.fletePorKilo,
            el.datosCotizacion.fletePorRecaudo,
            el.datosCotizacion.manejo,
            el.datosCotizacion.recaudo,
            el.estado,
            el.datosCotizacion.total
          )
        );
      });
      setRows(rowsEffect);
      //esto para que es?
      //esto deberia actualizarse solo despues de que se consulta el estado de todas las guias

      setGuiasArray([...newInfoArray]);
    } else {
      setTransportadoraName("");
      setShowAlertDate(true);
      setShowGuias(false);
      setMessageTransportadoraEmpty("");
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.guia);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, guia, row) => {
    const selectedIndex = selected.indexOf(guia);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, guia);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (guia) => selected.indexOf(guia) !== -1;

  const actualizarLaVista = () => {
    window.location.href = window.location.href;
  };
  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const cellAllign = "center";

  let bodyHtml;
  if (showGuias && !spinner) {
    bodyHtml = (
      <Paper className={classes.paper}>
        <EnhancedTableToolbar
          updateRelacionView={actualizarLaVista}
          horCode={props.userInfo.HORCODE}
          transportadora={transportadoraName}
          numSelected={selected.length}
          allGuiasArray={guiasArray}
          guiasIds={selected}
          userInfo={props.userInfo}
        />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? "small" : "medium"}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.guia);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  let totalRow = row.total;
                  //establecer si totalRow es un arreglo o un numero
                  if (isNaN(totalRow)) {
                    //formatear el total dependiendo de la cantidad de elementos en el row
                    if (row.total.length === 2) {
                      totalRow = (
                        <Grid container direction="column">
                          <Grid item>{row.total[0]}</Grid>
                          <Grid item style={{ color: "red" }}>
                            {row.total[1]}
                          </Grid>
                        </Grid>
                      );
                    }
                    if (row.total.length > 2) {
                      totalRow = (
                        <Grid container direction="column">
                          <Grid item>{row.total[0]}</Grid>
                          <Grid item>{row.total[1]}</Grid>
                          <Grid item style={{ color: "red" }}>
                            {row.total[2]}
                          </Grid>
                        </Grid>
                      );
                    }
                  } else {
                    totalRow = <Grid item>{totalRow}</Grid>;
                  }
                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.guia)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.guia}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.guia}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.orden}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.transportadora}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.agente}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.origenDestino}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.fecha}</TableCell>
                      <TableCell align={cellAllign}>{row.peso}</TableCell>
                      <TableCell align={cellAllign}>{row.tipo}</TableCell>
                      <TableCell align={cellAllign}>{row.cobertura}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.diasEntrega}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.valorDeclarado}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.fleteTransportadora}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.fletePorRecaudo}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.flete}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.valorRecaudo}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.estado}</TableCell>
                      <TableCell align={cellAllign}>{totalRow}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100, 200]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    );
  } else if (!showGuias) {
    bodyHtml = (
      <Alert className={classes.alertStyles} severity="error">
        No existen guias en el rango de fechas seleccionado.
      </Alert>
    );
  }

  if (showGuias) {
    return (
      <div className={classes.root}>
        <BusquedaRelacionEnvios
          showSteps={true}
          guiasInfo={props.userInfo.GuiasInfoRelacion}
          updateData={updateData}
        />
        {spinner ? (
          <Grid
            container
            justify="center"
            direction="column"
            alignContent="center"
            alignItems="center"
          >
            <CircularProgress style={{ marginTop: 10 }} />
            <Typography style={{ marginTop: 10 }}>
              Actualizando el estado de las guías
            </Typography>
          </Grid>
        ) : null}
        {bodyHtml}
      </div>
    );
  } else if (
    messageTranportadoraEmpty ===
    "Por favor elija una transportadora en 1. Seleccione Transportadora"
  ) {
    return (
      <div>
        <BusquedaRelacionEnvios
          showSteps={true}
          guiasInfo={props.userInfo.GuiasInfoRelacion}
          updateData={updateData}
        />
        <Alert className={classes.alertStyles} severity="error">
          {messageTranportadoraEmpty}.
        </Alert>
      </div>
    );
  } else {
    return (
      <div>
        <BusquedaRelacionEnvios
          showSteps={true}
          guiasInfo={props.userInfo.GuiasInfoRelacion}
          updateData={updateData}
        />
        {showAlertDate ? (
          <Alert className={classes.alertStyles} severity="error">
            No existen guías de {transportadoraName} en el rango de fechas
            especificado.
          </Alert>
        ) : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userInfo: state.setUserInfo.userInfo,
  };
}
export default connect(mapStateToProps)(RelacionGuias);
