import React, { useState, useEffect } from "react";
import BusquedaRelacionEnvios from "../Atoms/BusquedaRelacionEnvios";
import { makeStyles, lighten } from "@material-ui/core/styles";
import { jsPDF } from "jspdf";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import clsx from "clsx";
import axios from "axios";
import store from "../Redux/store";
import { userInfo, dataInfo } from "../Redux/actions";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import SaveAltIcon from "@material-ui/icons/SaveAlt";
import PrintIcon from "@material-ui/icons/Print";
import DescriptionIcon from "@material-ui/icons/Description";
import Alert from "@material-ui/lab/Alert";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useHistory } from "react-router-dom";
import generarRotulo from "../Atoms/generarRotulo";
import generarRotulos from "../Atoms/generarRotulos";
import aveonline from "../Apis/aveonline";
import enviaConsultaGuia from "../Apis/enviaConsultaGuia";
import actualizarEstadoGuiasEnvia from "../Utils/actualizarEstadoGuiasEnvia";
import enviaCancelarGuia from "../Apis/enviaCancelarGuia";
var numberOfAjaxCAllPending = 0;


let ip = "";

function createData(
  guia,
  orden,
  transportadora,
  agente,
  origenDestino,
  fecha,
  peso,
  tipo,
  cobertura,
  diasEntrega,
  valorDeclarado,
  fleteTransportadora,
  fletePorRecaudo,
  flete,
  valorRecaudo,
  estado,
  total,
  actions
) {
  return {
    guia,
    orden,
    transportadora,
    agente,
    origenDestino,
    fecha,
    peso,
    tipo,
    cobertura,
    diasEntrega,
    valorDeclarado,
    fleteTransportadora,
    fletePorRecaudo,
    flete,
    valorRecaudo,
    estado,
    total,
    actions,
  };
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: "guia", numeric: false, disablePadding: true, label: "Guía" },
  {
    id: "orden",
    numeric: false,
    disablePadding: false,
    label: "Órden de Compra",
  },
  {
    id: "transportadora",
    numeric: false,
    disablePadding: false,
    label: "Transportadora",
  },
  { id: "agente", numeric: false, disablePadding: false, label: "Agente" },
  {
    id: "origenDestino",
    numeric: false,
    disablePadding: false,
    label: "Origen / Destino",
  },
  { id: "fecha", numeric: false, disablePadding: false, label: "Fecha" },
  { id: "peso", numeric: true, disablePadding: false, label: "Peso" },
  { id: "tipo", numeric: false, disablePadding: false, label: "Tipo de Envío" },
  {
    id: "cobertura",
    numeric: false,
    disablePadding: false,
    label: "Cobertura",
  },
  {
    id: "diasEntrega",
    numeric: false,
    disablePadding: false,
    label: "Días Entrega",
  },
  {
    id: "valorDeclarado",
    numeric: true,
    disablePadding: false,
    label: "Valor Declarado",
  },
  {
    id: "fleteTransportadora",
    numeric: false,
    disablePadding: false,
    label: "Flete Transportadora",
  },
  {
    id: "fletePorRecaudo",
    numeric: false,
    disablePadding: false,
    label: "Flete Por Recaudo",
  },
  { id: "flete", numeric: false, disablePadding: false, label: "Manejo" },
  {
    id: "valorRecaudo",
    numeric: false,
    disablePadding: false,
    label: "Valor Recaudo",
  },
  { id: "estado", numeric: false, disablePadding: false, label: "Estado" },
  { id: "total", numeric: true, disablePadding: false, label: "Total" },
  { id: "actions", numeric: true, disablePadding: false, label: "Acciones" },
];
function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ "aria-label": "select all desserts" }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            style={{ fontWeight: 700 }}
            key={headCell.id}
            align={headCell.numeric ? "center" : "center"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: "1 1 100%",
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { numSelected } = props;
  const guardarListadoGuiasEnServidor = (
    array,
    arrayRelacion,
    sobreescribir,
    cancelarVariasGuias
  ) => {
    const options = {
      headers: { credentials: "include" },
    };
    
    let email;
    //revisar si se cancelan las guias de un usuario o de varios
    if(cancelarVariasGuias){
      if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        // dev code
        ip = "https://localhost/guardarTodasLasGuias";
      } else {
        // production code
        ip = `${process.env.REACT_APP_URL}/guardarTodasLasGuias`;
        //;
      }
      email = store.getState().setUserInfo.userInfo.Email;
    } else {
      //guardar las guias en la url por defecto para un usuario
      if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        // dev code
        ip = "https://localhost/guardarGuias";
      } else {
        // production code
        ip = `${process.env.REACT_APP_URL}/guardarGuias`;
        //;
      }
      
      email = props.Email;
    }
    
    axios
      .put(
        ip,
        {
          Email: email,
          GuiasInfo: array,
          GuiasInfoRelacion: arrayRelacion,
          DataInfoEntries : array,
          Sobreescribir: sobreescribir,

         
        },
        options
      )
      .then(function (response) {
        //en vez de usar crear guias modificar userInfo con la nueva info que llega del servidor
        //informar al parent que la respuesta fue satisfactoria
        props.cancelarGuiasResponse("success", response, cancelarVariasGuias);
      })
      .catch(function (error) {
        //informar al parent que hubo un error
        props.cancelarGuiasResponse("error");
      });
  };
  const cancelarGuia = (bool) => {
    //si bool es true, signifnica que se deben cancelar las guias de varios usuarios
    let guiaCancelarRelacion, guiaCancelar;
    if(bool){
      //en props.userInfo.GuiasInfo llegan todas las guias de los usuarios
      //en props.userInfo.GuiasInfoRelacion no hay guias por lo que se deben obtener del estado
      let guiasInfoRelacionTodosLosUsuarios = store.getState().setDataInfo.dataInfo;
      //eliminar el arreglo comisiones si lo hubiere
      let guiasInfoRelacionTodosLosUsuariosKeys = Object.keys(guiasInfoRelacionTodosLosUsuarios);
      
      if(guiasInfoRelacionTodosLosUsuariosKeys.includes('comisiones')){
        delete guiasInfoRelacionTodosLosUsuarios['comisiones'];
        console.log(guiasInfoRelacionTodosLosUsuarios);
        guiasInfoRelacionTodosLosUsuariosKeys.shift();
        
      }

      let guiasInfoRelacionTodosLosUsuariosSinComisionKeys = Object.keys(guiasInfoRelacionTodosLosUsuarios);
      guiasInfoRelacionTodosLosUsuariosSinComisionKeys.forEach(user=>{
        //se generan las guias normales a cancelawr
        const GuiasInfoConGuiasEliminadas =  guiasInfoRelacionTodosLosUsuarios[user].GuiasInfo.filter(guia=>{
          if (props.selected.includes(guia.numeroGuia)) {
            //tambien es necesario revisar si las guias a eliminar son de envia o no
            if (guia.datosCotizacion.cotizarConApiEnvia) {
              //guia.estado = "ANULADA EN BOGOTA";
              //al cancelar la guia en la api de envia, los datos se sobreescriben
              props.cancelarGuiaEnEnvia(guia.numeroGuia, guia.horaActual, true);
              //se cancela la guia en el componente Listado Guias
            } 
          } else {
            //se agrega las guias que no se van a cancelar al arreglo que se va a mandar al endpoint
            //cada usuario debe tener las guias agregadas en GuiasInfo y GuiasInfoRelacion
             return guia;
          }
        })
        //se generan las guias en relacion a cancelar
      const GuiasInfoRelacionConGuiasEliminadas =  guiasInfoRelacionTodosLosUsuarios[user].GuiasInfoRelacion.filter(guia=>{
          if (props.selected.includes(guia.numeroGuia)) {
            //tambien es necesario revisar si las guias a eliminar son de envia o no
            if (guia.datosCotizacion.cotizarConApiEnvia) {
              //guia.estado = "ANULADA EN BOGOTA";
              //al cancelar la guia en la api de envia, los datos se sobreescriben
              props.cancelarGuiaEnEnvia(guia.numeroGuia, guia.horaActual, true);
              //se cancela la guia en el componente Listado Guias
            } 
          } else {
            //se agrega las guias que no se van a cancelar al arreglo que se va a mandar al endpoint
            //cada usuario debe tener las guias agregadas en GuiasInfo y GuiasInfoRelacion
             return guia;
          }
        })
        //cada vez que se itera en un usuario actualizo el arreglo con las guias que fueron eliminadas
      //el objeto que contiene los usuario se manda tal cual al endpoint donde se atualiza con las guias 
      guiasInfoRelacionTodosLosUsuarios[user].GuiasInfo = GuiasInfoConGuiasEliminadas;
      guiasInfoRelacionTodosLosUsuarios[user].GuiasInfoRelacion = GuiasInfoRelacionConGuiasEliminadas;
      //como el endpoint para actualizar varias guias de usuarios recibe dataEntries es necesario modificar el nombre
    //y agregar los usuarios con las guias actualizadas en un solo arreglo, es decir no se toma en cuenta el segundo 
    //parametro cuando se cancelan las guias

      });
      //generar formato con arreglo para que el servidor pueda recibir los datos como cuando se actualizan las guias
     let guiasInfoConRelacionFormatoCorrecto = guiasInfoRelacionTodosLosUsuariosKeys.map(email=>{
        return [email, guiasInfoRelacionTodosLosUsuarios[email]];
      })
      
      guardarListadoGuiasEnServidor(guiasInfoConRelacionFormatoCorrecto, guiaCancelarRelacion, true, true);
    } else {
           //es necesario obtener las guias y actualizar solo la guia que se va a cancelar para que no se duplique
    //con valores de cancelada y generada
    props.activateSpinner();
    ;
    const guiasInfo = props.userInfo.GuiasInfo;
    const guiasInfoRelacion = props.userInfo.GuiasInfoRelacion;
    
     guiaCancelar = guiasInfo.filter((guia) => {
      if (props.selected.includes(guia.numeroGuia)) {
        //tambien es necesario revisar si las guias a eliminar son de envia o no
        if (guia.datosCotizacion.cotizarConApiEnvia) {
          //guia.estado = "ANULADA EN BOGOTA";
          //al cancelar la guia en la api de envia, los datos se sobreescriben
          props.cancelarGuiaEnEnvia(guia.numeroGuia, guia.horaActual, true);
          //se cancela la guia en el componente Listado Guias
        } else {
          guia.estado = "CANCELADA";
        }
      } else {
        return guia;
      }
    });
     guiaCancelarRelacion = guiasInfoRelacion.filter((guia) => {
      if (!props.selected.includes(guia.numeroGuia)) {
        return guia;
      }
    });
    ;
    //guardarListadoGuiasEnServidor(guiaCancelar, guiaCancelarRelacion, true);
    //se actualizan las guias en el servidor de un solo usuario
    guardarListadoGuiasEnServidor(guiaCancelar, guiaCancelarRelacion, true, false);
    }
    
 
  };

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography
          className={classes.title}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} Seleccionado{numSelected > 1 ? "s" : null}
        </Typography>
      ) : (
        <Typography
          align="center"
          className={classes.title}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Listado de Guías
        </Typography>
      )}

      {numSelected > 0 ? (
        <Grid container wrap="nowrap" justify="flex-end">
          <Tooltip title="Guardar Rótulos">
            <IconButton
              onClick={() =>
                generarRotulos(props.selected, props.userInfo.GuiasInfo)
              }
            >
              <DescriptionIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Eliminar Guías">
            <IconButton onClick={()=>cancelarGuia(true)}>
              <HighlightOffIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      ) : null}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  alertStyles: {
    width: "fit-content",
    margin: "auto",
  },
}));

function ListadoGuiasUsuarios(props) {
  const classes = useStyles();
  const history = useHistory();
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("fecha");
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [dense, setDense] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [guiasArray, setGuiasArray] = useState([]);
  const [showGuias, setShowGuias] = useState(true);
  const [rows, setRows] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [alert, setAlert] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [iframe, setIframe] = useState(false);
  const [iframeDiv, setIframeDiv] = useState(false);
  const [mostrarRows, setMostrarRows] = useState({
    estado: false,
    finalRows: [],
  });
  const [cotizarEstado, setCotizarEstado] = useState(false);
  const [actualizarTodasLasGuias, setActualizarTodasLasGuias] =useState(false);
  const [renderizarCuandoSeCancelanVariasGuias, setRenderizarCuandoSeCancelanVariasGuias] = useState(false);

  // Add a request interceptor
axios.interceptors.request.use(function (config) {
  numberOfAjaxCAllPending++;
  // show loader
  return config;
}, function (error) {
  return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  numberOfAjaxCAllPending--;
  console.log("------------  Ajax pending", numberOfAjaxCAllPending);

  if (numberOfAjaxCAllPending == 0) {
      //hide loader
      //console.log('hideLoader');
      setSpinner(false);
  } else {
    console.log('show loader');
    setSpinner(true);
  }
  return response;
}, function (error) {
  numberOfAjaxCAllPending--;
  if (numberOfAjaxCAllPending == 0) {
      //hide loader
      console.log('hideLoader');
      setSpinner(false);
  }
  return Promise.reject(error);
});

  const guardarListadoGuiasEnServidor = (array, arrayRelacion, noCancelar) => {
    ;
    if (!noCancelar) {
      setSpinner(true);
    }
    let email;
     //cuando se han seleccionado todos los usuarios en props.userSelected existe un arrelgo que se llama
    //dataInfoEntries, revisar que no sea undefined, si es asi entonces se deben actualizar las guias
    //de varios usuarios al mismo tiempo
    
    if(props.userSelected.dataInfoEntries !== undefined){
      //varios usuarios
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
      // dev code
      ip = "https://localhost/guardarTodasLasGuias";
    } else {
      // production code
      ip = `${process.env.REACT_APP_URL}/guardarTodasLasGuias`;
      //;
    }
    //si son varios usuarios el correo por defecto debe ser el usuario actual de la app
    email = store.getState().setUserInfo.userInfo.Email;
    } else {
      if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        // dev code
        ip = "https://localhost/guardarGuias";
      } else {
        // production code
        ip = `${process.env.REACT_APP_URL}/guardarGuias`;
        //;
      }
      
      email = props.userSelected.Email;
    }

    ;
    const options = {
      headers: { credentials: "include" },
    };
    
    axios
      .put(
        ip,
        {
          Email: email,
          DataInfoEntries : props.userSelected.dataInfoEntries,
          GuiasInfo: array,
          GuiasInfoRelacion: arrayRelacion,
          Sobreescribir: true,
        },
        options
      )
      .then(function (response) {
        //en vez de usar crear guias modificar userInfo con la nueva info que llega del servidor
        //cuando la api responde cotizar estado ya ha cambiado a false, asi que es necesario revisar si ese cambio 
        //en el estado no esta permitiendo que se actualize la vista con las guias cuando se seleccionan todos los usuarios
        //tambien no cancelar esta llegando como true por lo que no se esta enviando la info al usuario
        ;
        console.log(noCancelar);
       if (!noCancelar) {
          //se deben actualizar los datos con la respuesta que llega del servidor
          
          store.dispatch(userInfo('set_user_info', response.data.userInfo));
          store.dispatch(dataInfo("set_data_info", response.data.dataInfo));
          setTimeout(() => {
            setSpinner(false);
            
            setAlert("success");
            setErrorMessage("La guia se ha eliminado.");
            const getDivView = document.getElementById("errorMessage");
            if (getDivView !== null && getDivView !== undefined) {
              getDivView.scrollIntoView({ behavior: "smooth" });
            }
          }, 2000);
        }
        //si responde la api que de guardarTodas las guias entonces actualizar el store
        if(response.config.url.includes('guardarTodasLasGuias')){
            //1 //se esta repitiendo esta llamada constantemente cuando se actualiza el total de guias en Usuarios cuando se cancelan varias guias 
          store.dispatch(userInfo('set_user_info', response.data.userInfo));
          store.dispatch(dataInfo("set_data_info", response.data.dataInfo));
          setTimeout(() => {
            setSpinner(false);
            setAlert("success");
            setErrorMessage("Las guias se han actualizado.");
            setActualizarTodasLasGuias(true)
            const getDivView = document.getElementById("errorMessage");
            if (getDivView !== null && getDivView !== undefined) {
              getDivView.scrollIntoView({ behavior: "smooth" });
            }
          }, 2000);
        }
      })
      .catch(function (error) {
        ;
        setSpinner(false);
        console.log(
          "hubo un error al comunicarse con el servidor local",
          error.response
        );
        setAlert("error");
        
        setErrorMessage(
          "Hubo un error al comunicarse con el servidor local revisa tu conexión a internet o intenta más tarde."
        );
        setTimeout(() => {
          const getDivView = document.getElementById("errorMessage");
          if (getDivView !== null && getDivView !== undefined) {
            getDivView.scrollIntoView({ behavior: "smooth" });
          }
        }, 250);
      });
  };
  const cancelarGuiaEnEnvia = (numeroGuia, horaGeneracionGuia, noSobreescribir) => {
    //si la guia es de envia y se genero con la api de envia es necesario eliminarla localmente y o cancelarla en la api
    const body = {
      numero_guia: numeroGuia,
      usuario_online: "55F2HORT",
      fec_captura: horaGeneracionGuia,
    };

    const cancelarGuiaEnviaPromise = enviaCancelarGuia(
      process.env.REACT_APP_ENVIA_HTTPS_BASE_URL +
        "/ServicioLiquidacionREST/Service1.svc/Anulacion/",
      body
    );
    cancelarGuiaEnviaPromise
      .then((ans) => {
        if (ans.data.respuesta === "Exitoso") {
          ;
          if(noSobreescribir){
            return;
          }
          ;
          const guiasInfo = props.userInfo.GuiasInfo;
          const guiasInfoRelacion = props.userInfo.GuiasInfoRelacion;
          const guiaCancelar = guiasInfo.filter((guia) => {
            if (guia.numeroGuia !== numeroGuia) {
              return guia;
            }
            
          });
          const guiaCancelarRelacion = guiasInfoRelacion.filter((guia) => {
            if (guia.numeroGuia !== numeroGuia) {
              // guia.estado = "GUIA CANCELADA POR EL USUARIO";
              return guia;
            } 
            
          });
          guardarListadoGuiasEnServidor(
            guiaCancelar,
            guiaCancelarRelacion,
            false
          );
        } else {
          //si sobreescribir es true entonces en la base de datos ya se actualizo porque la cancelacion
          //se hizo desde enhanced toolbar, es decir se cancelaron varias guias
          ;
          if(noSobreescribir){
            return;
          }
          ;
          console.log(ans.data);
          //no se pudo consultar la guia porque ya ha sido cancelada
          console.log(
            "no se pudo consultar la guia porque ya ha sido cancelada?"
          );
          //me esta cambpiando el usuario cuando cancelo las guias por que?
          //mostrar un mensaje que indique que la guia ya se habia cancelado
          ;
          const guiasInfo = props.userInfo.GuiasInfo;
          const guiasInfoRelacion = props.userInfo.GuiasInfoRelacion;
          const guiaCancelar = guiasInfo.filter((guia) => {
            if (guia.numeroGuia !== numeroGuia) {
              return guia;
            }
          });
          const guiaCancelarRelacion = guiasInfoRelacion.filter((guia) => {
            if (guia.numeroGuia !== numeroGuia) {
              return guia;
            }
          });
          ;
          //aca despues de cancelar varias guias se sobreescriben las guias y vuelven a aparecer
          //las guias que se querian eliminar como guias a mostrar. 
          guardarListadoGuiasEnServidor(
            guiaCancelar,
            guiaCancelarRelacion,
            false
          );
          setAlert("error");
          setErrorMessage("La guia ya se había cancelado.");
        }
      })
      .catch((err) => {
        console.log("hubo un error al cancelar la guia", err);
        //mostrar un mensaje que indique que la guia ya se habia cancelado
        setAlert("error");
        setErrorMessage("La guia ya se había cancelado.");
      });
  };
  const cancelarGuia = (ev, el) => {
    //eliminar las guias de la base de
    //es necesario obtener las guias y actualizar solo la guia que se va a cancelar para que no se duplique
    //con valores de cancelada y generada
    setIframeDiv(<div></div>);
    //es necesario obtener las guias y actualizar solo la guia que se va a cancelar para que no se duplique
    //con valores de cancelada y generada
    //si la guia se genero con la api de envia no usar esta logica sino mas bien
    //cancelar la guia en el servidor de envia
    if (el.datosCotizacion.cotizarConApiEnvia) {
      cancelarGuiaEnEnvia(el.numeroGuia, el.horaActual);
      return;
    }
    const guiasInfo = props.userInfo.GuiasInfo;
    const guiasInfoRelacion = props.userInfo.GuiasInfoRelacion;

    const guiaCancelar = guiasInfo.filter((guia) => {
      if (guia.numeroGuia !== el.numeroGuia) {
        return guia;
      }
    });
    
    const guiaCancelarRelacion = guiasInfoRelacion.filter((guia) => {
      if (guia.numeroGuia !== el.numeroGuia) {
        return guia;
      }
    });
    guardarListadoGuiasEnServidor(guiaCancelar, guiaCancelarRelacion, false);
    setIframe(<div></div>);
  };
  const guardarGuiaBoton = (el) => {
    let srcString = el.resultado.rutaguia;
    if (!srcString.includes("https")) {
      srcString = srcString.replace("http", "https");
    }
    const myWindow = window.open(
      "",
      "new window",
      "width=1000px,height=1000px"
    );
    // const iframe = `${<iframe src={srcString} type='application/pdf' id='iframeid' height='1000px' width='100%' title='Guia'></iframe>}`;
    const iframe = document.createElement("iframe");
    iframe.setAttribute("src", srcString);
    iframe.setAttribute("type", "application/pdf");
    iframe.setAttribute("id", "iframeid");
    iframe.setAttribute("height", "100%");
    iframe.setAttribute("width", "1000px");
    iframe.setAttribute("title", "Guía");
    myWindow.document.body.appendChild(iframe);

    /* setIframeDiv(
      <iframe
        src={srcString}
        type="application/pdf" 
        id="iframeid" 
        height="1000px"
        width="100%  "
        title="Guia"
        target="_blank" 
      ></iframe>
    );
    setIframe(true);
    callIframe();*/
    //GENERAR PDF

    //GENERAR PDF
  };
  const callIframe = () => {
    setTimeout(() => {
      var pdf = new jsPDF("l", "pt", "a4");
      const doc = document.getElementById("iframeid");
      if (doc !== null) {
        doc.scrollIntoView({ behavior: "smooth" });
        doc.focus();
        //doc.contentWindow.print();
      }
    }, [1000]);
  };
//se dispara este efecto despues de que se seleccionan todos los usuarios
  useEffect(() => {
    //despues de canclear una guia se llega aca
    //cuando las guias se cancelan es mejor no realizar una solicitud a la api
    let rowsEffect = [];
    //se genera el arreglo de los rows que contienen los elementos del dom de las guias a renderizar
    props.userInfo.GuiasInfo.forEach((el) => {
      const agentNameAndCode =
        el.agentInfo.nombreCompleto.length > 0
          ? el.agentInfo.nombreCompleto + " / Código: " + el.agentInfo.HORCODE
          : props.userInfo.HORCODE;
      let guardarGuiaBoton = (
        <Tooltip title="Guardar Guía">
          <IconButton onClick={() => guardarGuiaBoton(el)}>
            <SaveAltIcon />
          </IconButton>
        </Tooltip>
      );
      if (el.datosCotizacion.transportadora.startsWith("M")) {
        guardarGuiaBoton = null;
      }
      rowsEffect.push(
        createData(
          el.numeroGuia,
          el.datosRemitente.ordenDeCompra,
          el.datosCotizacion.transportadora,
          agentNameAndCode,
          el.datosCotizacion.origenDestino,
          el.horaActual,
          el.datosCotizacion.kilos,
          el.datosCotizacion.tipoEnvio,
          el.datosCotizacion.trayecto,
          el.datosCotizacion.diasEntrega,
          el.datosCotizacion.valoracion,
          el.datosCotizacion.fletePorKilo,
          el.datosCotizacion.fletePorRecaudo,
          el.datosCotizacion.manejo,
          el.datosCotizacion.recaudo,
          el.estado,
          el.datosCotizacion.total,
          <Grid container wrap="nowrap">
            <Tooltip title="Guardar Rótulo">
              <IconButton onClick={() => generarRotulo(el)}>
                <DescriptionIcon />
              </IconButton>
            </Tooltip>
            {guardarGuiaBoton}
            <Tooltip title="Eliminar Guía ">
              <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                <HighlightOffIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        )
      );
    });
    setRows(rowsEffect);
    //es necesario realizar la solicitud a la api porque el usuario llega de la eleccion del usuario de la app
    
    setGuiasArray([]);
    setSpinner(false);
  }, [props.userInfo.GuiasInfo]);
//se dispara este useEffect despues de que se ejecuta el useEffect the props.userInfo.GuiasInfo
  useEffect(() => {
    
    //despues de cancelar una guia e ingreesar en props.userInfo.GuiasInfo se ingresa aca
    //tambien se ingresa aca cuando se cancelan varias guias al mismo tiempo de un solo usuario
    //sin embargo la vista se actualiza con todas las guias incluyendo la que se elimino
    //se crean las guias cuando el usuario da click en buscar guias y siempre es la ultima iteracion
    //este es el lugar ideal para realizar la solicitud a la api de horenvios para verificar el estado de cada guia

    let rowsEffect = [];
    //si guiasArray es 0 y GuiasInfo es cero, es o porque este usuario no ha generado guias
    //o porque tenia guias y las elimino todas, en ese caso se debe renderizar una vista sin guias. 
    if(props.userInfo.GuiasInfo.length === 0 && guiasArray.length === 0){
    //  setAlert('error');
     // setErrorMessage('El usuario no tiene guias generadas.')
      setRows([]);
      setMostrarRows({ estado: true, finalRows: [] });
    }

    //si en el estado guiasArray hay guias, significa que se agregaron desde updateData, sino es la primera vista y se debe
    //agregar las guia de las props
    let guiasInfoArray =
      guiasArray.length === 0 ? props.userInfo.GuiasInfo : guiasArray;
    //si se trata de la primera carga en usuarios no generar el token
    if (guiasInfoArray.length === 0) {
      return;
    }
    let guiasInfoArrayCopy = [];
    //if(guiasArray.length > 0){
    setSpinner(true);
   
    //antes de agregar cada guia al arreglo que se va a renderizar
    //es necesario realizar la solicitud a aveonline para mostrar el estado de la guia
    //cuando se seleccionan todas las guias de todos los usuarios, en este punto llegan guias
    /**Tanto de envia como de tcc, por lo que solo se debe hacer la actualizacion de las guias de aveonline con la 
     * api de aveonline y la actualizacoin de estado de envia con la api de envia
     */
    let counter = 0;
    const dataAuthBody = {
      tipo: "auth",
      usuario: "innovacion",
      clave: "019*",
    };
    //autenticacion
    const aveonlinePromiseAuth = aveonline(
      "https://aveonline.co/api/comunes/v1.0/autenticarusuario.php",
      dataAuthBody
    );
    aveonlinePromiseAuth
      .then((resp) => {
        const token = resp.data.token;
        //si se obtiene el token es necesario asegurarse de que si hay un error que se mostro anteriormente
        //por ejemplo si el cliente estaba desconectado de internet y se habia mostrado el mensaje de error
        //de que el usuario no estaba conectado, volver a resetear el mensaje de error para que no se muestre
        //aveces se llega aca despues de cancelar una guia, por lo que el mensaje de error
        /**
         * la guia ya se habia cancelado se sobreescribe, es necesario dejar ese mensaje de error
         * en ese caso en particular
         */
        if(errorMessage !== "La guia ya se había cancelado."){
          setErrorMessage(
            ""
          );
          setAlert("no hay error");
        }

        
        // una vez se obtiene el token de autenticacion se
        //realiza una consulta del estado de cada una de las guias
        let length = guiasInfoArray.length;
        //la copia de las guias que se va a actualizar con los nuevos estados de guias y que se va a guardar en el servidor
        guiasInfoArray.forEach((el) => {
          //antes de agregar la guia al arreglo es necesario consultar el estado
          //pero solo consultar las guias que no tienen el estado  ENTREGADA
          if (
            el.estado === "ENTREGADA" ||
            el.estado === "CANCELADA" ||
            el.datosCotizacion.cotizarConApiEnvia
          ) {
            //si llega a este punto y se seleccionan todos los usuarios, consultar el estado de la guia
            //con la api de envia, revisar que no se realize doble consulta al estado cuando se selecciona
            //un solo usuario
            //no consultar y agregar al arreglo, eliminar la guia del contador
            guiasInfoArrayCopy.push(el);
            length = length - 1;

            // counter = counter - 1;
            const agentNameAndCode =
              el.agentInfo.nombreCompleto.length > 0
                ? el.agentInfo.nombreCompleto +
                  " / Código: " +
                  el.agentInfo.HORCODE
                : props.userInfo.HORCODE;
            let guardarGuiaBoton = (
              <Tooltip title="Guardar Guía">
                <IconButton onClick={() => guardarGuiaBoton(el)}>
                  <SaveAltIcon />
                </IconButton>
              </Tooltip>
            );
            if (el.datosCotizacion.transportadora.startsWith("M")) {
              guardarGuiaBoton = null;
            }
            rowsEffect.push(
              createData(
                el.numeroGuia,
                el.datosRemitente.ordenDeCompra,
                el.datosCotizacion.transportadora,
                agentNameAndCode,
                el.datosCotizacion.origenDestino,
                el.horaActual,
                el.datosCotizacion.kilos,
                el.datosCotizacion.tipoEnvio,
                el.datosCotizacion.trayecto,
                el.datosCotizacion.diasEntrega,
                el.datosCotizacion.valoracion,
                el.datosCotizacion.fletePorKilo,
                el.datosCotizacion.fletePorRecaudo,
                el.datosCotizacion.manejo,
                el.datosCotizacion.recaudo,
                el.estado,
                el.datosCotizacion.total,
                <Grid container wrap="nowrap">
                  <Tooltip title="Guardar Rótulo">
                    <IconButton onClick={() => generarRotulo(el)}>
                      <DescriptionIcon />
                    </IconButton>
                  </Tooltip>
                  {guardarGuiaBoton}
                  <Tooltip title="Eliminar Guía ">
                    <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                      <HighlightOffIcon />
                    </IconButton>
                  </Tooltip>
                </Grid>
              )
            );
            //revisar que al restar no sea igual a counter, si es asi todas las guias estan canceladas o generadas

            if (length === counter) {
              setRows(rowsEffect);
              console.log(cotizarEstado);
              setCotizarEstado(true);
              // setSpinner(false);
            }
          } else {
            //realizar la consulta
            const estadoBody = {
              tipo: "obtenerEstadoAuth",
              token: token,
              id: 6817,
              guia: el.numeroGuia,
            };

            //se itera sobre cada guia y se consulta el estado
            const aveonlineGuiaEstado = aveonline(
              "https://aveonline.co/api/nal/v1.0/guia.php",
              estadoBody
            );
            aveonlineGuiaEstado
              .then((resp) => {
                const estado = resp.data.guias[0].estado;
                el.estado = estado; //actualizo el estado en la guia para enviarlo a el servidor
                guiasInfoArrayCopy.push(el);
                const agentNameAndCode =
                  el.agentInfo.nombreCompleto.length > 0
                    ? el.agentInfo.nombreCompleto +
                      " / Código: " +
                      el.agentInfo.HORCODE
                    : props.userInfo.HORCODE;
                let guardarGuiaBoton = (
                  <Tooltip title="Guardar Guía">
                    <IconButton onClick={() => guardarGuiaBoton(el)}>
                      <SaveAltIcon />
                    </IconButton>
                  </Tooltip>
                );
                if (el.datosCotizacion.transportadora.startsWith("M")) {
                  guardarGuiaBoton = null;
                }
                rowsEffect.push(
                  createData(
                    el.numeroGuia,
                    el.datosRemitente.ordenDeCompra,
                    el.datosCotizacion.transportadora,
                    agentNameAndCode,
                    el.datosCotizacion.origenDestino,
                    el.horaActual,
                    el.datosCotizacion.kilos,
                    el.datosCotizacion.tipoEnvio,
                    el.datosCotizacion.trayecto,
                    el.datosCotizacion.diasEntrega,
                    el.datosCotizacion.valoracion,
                    el.datosCotizacion.fletePorKilo,
                    el.datosCotizacion.fletePorRecaudo,
                    el.datosCotizacion.manejo,
                    el.datosCotizacion.recaudo,
                    el.estado,
                    el.datosCotizacion.total,
                    <Grid container wrap="nowrap">
                      <Tooltip title="Guardar Rótulo">
                        <IconButton onClick={() => generarRotulo(el)}>
                          <DescriptionIcon />
                        </IconButton>
                      </Tooltip>
                      {guardarGuiaBoton}
                      <Tooltip title="Eliminar Guía ">
                        <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                          <HighlightOffIcon />
                        </IconButton>
                      </Tooltip>
                    </Grid>
                  )
                );
                counter = counter + 1;

                if (counter === length) {
                  //es la ultima guia del arreglo pero no es posible saber si si se consultaron todas las guias
                  setSpinner(false);
                  //actualizo el servidor con las guias nuevas
                  //enviar la info a el servidor y devolver el nuevo objeto de la base de datos
                  //filtrar las guias que se actualizaron con un nuevo estado en el arreglo guiasInfoRelacion y actualizar ese arreglo
                  let guiasInfoRelacionCopy = [];
                  ;
                  guiasInfoArrayCopy.forEach((el) => {
                    //cuando se seleccionan todos los usuarios guiasInfoRelacion es undefined
                    if(props.userInfo.GuiasInfoRelacion !== undefined){
                      props.userInfo.GuiasInfoRelacion.forEach((elRelacion) => {
                        if (el.numeroGuia === elRelacion.numeroGuia) {
                          //la guia en el arreglo general existe en el arreglo de relacion, actualizar el estado
                          elRelacion.estado = el.estado;
                          guiasInfoRelacionCopy.push(elRelacion);
                        }
                      });
                    }
                  
                  });
                   //este llamado se esta haciendo constantemente por actualizar el total de guias en Usuarios?
                  guardarListadoGuiasEnServidor(
                    guiasInfoArrayCopy,
                    guiasInfoRelacionCopy,
                    true
                  );
                  setRows(rowsEffect);
                  console.log(cotizarEstado);
                  setCotizarEstado(true);
                }
              })
              .catch((err) => {
                console.log("hubo un error", err);
              });
          }
        });
        //cuando se termina de consultar el estado de las guias es necesario actualizar la vista con las guias

      })
      .catch((err) => {
        console.log("hubo un error", err);
        if (err.message === "Cannot read property 'token' of undefined") {
          //probablemente no hay conexion
          setSpinner(false);
          setErrorMessage(
            "No hay conexión a internet. Por favor revise la conexión e intente de nuevo."
          );
          setAlert("error");
        }
      });
  }, [guiasArray]);
  //despues de enviar la solicitudd a la API para guardar las guias se activa este useEffect
  useEffect(() => {
    
    //setMostrarRows(true)
    if (!cotizarEstado) {
      return;
    }
    //se crean las guias cuando el usuario da click en buscar guias y siempre es la ultima iteracion
    //este es el lugar ideal para realizar la solicitud a la api de envia  para verificar el estado de cada guia

    let rowsEffect = [];
    //si en el estado guiasArray hay guias, significa que se agregaron desde updateData, sino es la primera vista y se debe
    //agregar las gusa de las props
    let guiasInfoArray =
      guiasArray.length === 0 ? props.userInfo.GuiasInfo : guiasArray;
    let guiasInfoArrayCopy = [];
    //antes de agregar cada guia al arreglo que se va a renderizar
    //es necesario realizar la solicitud a aveonline para mostrar el estado de la guia
    let counter = 0;
    //se realiza una consulta del estado de cada una de las guias
    let length = guiasInfoArray.length;
    //la copia de las guias que se va a actualizar con los nuevos estados de guias y que se va a guardar en el servidor
    
    guiasInfoArray.forEach((el) => {
      //antes de agregawr la guia al arreglo es necesario consultar el estado
      //pero solo consultar las guias que no tienen el estado  ENTREGADA
      //tampoco se debe obtener el estado de las guias generados con envia, para eso se debe hacer otra logica
      /**
             * Generada
  Recogida
  En reparto
  Novedad
  Devolución
  Entregada
  Anulada (SOLO NO ACTUALIZAR LAS ANULADAS)
             */
      if (
        el.estado.toUpperCase().startsWith("ANULADA") ||
        el.estado.toUpperCase().startsWith("ENTREGADA") ||
        el.estado.toUpperCase().startsWith("DEVOLUCIÓN") ||
        !el.datosCotizacion.cotizarConApiEnvia
      ) {
        //no consultar y agregar al arreglo, eliminar la guia del contador
        guiasInfoArrayCopy.push(el);
        length = length - 1;

        // counter = counter - 1;
        const agentNameAndCode =
          el.agentInfo.nombreCompleto.length > 0
            ? el.agentInfo.nombreCompleto + " / Código: " + el.agentInfo.HORCODE
            : props.userInfo.HORCODE;
        let botonGuardarGuia = (
          <Tooltip title="Guardar Guía">
            <IconButton onClick={() => guardarGuiaBoton(el)}>
              <SaveAltIcon />
            </IconButton>
          </Tooltip>
        );
        if (el.datosCotizacion.transportadora.startsWith("M")) {
          botonGuardarGuia = null;
          console.log("esto no?");
        }
        
        rowsEffect.push(
          createData(
            el.numeroGuia,
            el.datosRemitente.ordenDeCompra,
            el.datosCotizacion.transportadora,
            agentNameAndCode,
            el.datosCotizacion.origenDestino,
            el.horaActual,
            el.datosCotizacion.kilos,
            el.datosCotizacion.tipoEnvio,
            el.datosCotizacion.trayecto,
            el.datosCotizacion.diasEntrega,
            el.datosCotizacion.valoracion,
            el.datosCotizacion.fletePorKilo,
            el.datosCotizacion.fletePorRecaudo,
            el.datosCotizacion.manejo,
            el.datosCotizacion.recaudo,
            el.estado,
            el.datosCotizacion.total,
            <Grid container wrap="nowrap">
              <Tooltip title="Guardar Rótulo">
                <IconButton onClick={() => generarRotulo(el)}>
                  <DescriptionIcon />
                </IconButton>
              </Tooltip>
              {botonGuardarGuia}
              <Tooltip title="Eliminar Guía ">
                <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                  <HighlightOffIcon />
                </IconButton>
              </Tooltip>
            </Grid>
          )
        );
        //revisar que al restar no sea igual a counter, si es asi todas las guias estan canceladas o generadas

        if (length === counter) {
          //setRows(rowsEffect);
          ;
          setMostrarRows({ estado: true, finalRows: rowsEffect });
          //setSpinner(false);
        }
      } else {
        //consultar el estado de cada guia en la API de envia

        //se itera sobre cada guia y se consulta el estado
        const url =
          process.env.REACT_APP_ENVIA_HTTPS_BASE_URL +
          "/ServicioRESTConsultaEstados/Service1Consulta.svc/ConsultaGuia/" +
          el.numeroGuia;
        const estadoGuiaEnvia = enviaConsultaGuia(url);
        estadoGuiaEnvia
          .then((resp) => {
            //las guias que han sido anuladas responden con un status "Error";
            //si la respuesta es que no se puede consultar la guia, quiere decir que esta anulada
            if (resp.data === undefined) {
              //no se puede leer el estado de la guia
              setAlert("error");
              setErrorMessage("No se pueden consultar algunas guías.");

              //return;
            }
            if (resp.data.status !== "Error") {
              //si no hay error se actualiza el estado de la guia
              const estado = resp.data.estado;
              el.estado = estado; //actualizo el estado en la guia para enviarlo a el servidor
            }
            guiasInfoArrayCopy.push(el);
            const agentNameAndCode =
              el.agentInfo.nombreCompleto.length > 0
                ? el.agentInfo.nombreCompleto +
                  " / Código: " +
                  el.agentInfo.HORCODE
                : props.userInfo.HORCODE;
            let botonGuardarGuia = (
              <Tooltip title="Guardar Guía">
                <IconButton onClick={() => guardarGuiaBoton(el)}>
                  <SaveAltIcon />
                </IconButton>
              </Tooltip>
            );
            if (el.datosCotizacion.transportadora.startsWith("M")) {
              botonGuardarGuia = null;
            }
            rowsEffect.push(
              createData(
                el.numeroGuia,
                el.datosRemitente.ordenDeCompra,
                el.datosCotizacion.transportadora,
                agentNameAndCode,
                el.datosCotizacion.origenDestino,
                el.horaActual,
                el.datosCotizacion.kilos,
                el.datosCotizacion.tipoEnvio,
                el.datosCotizacion.trayecto,
                el.datosCotizacion.diasEntrega,
                el.datosCotizacion.valoracion,
                el.datosCotizacion.fletePorKilo,
                el.datosCotizacion.fletePorRecaudo,
                el.datosCotizacion.manejo,
                el.datosCotizacion.recaudo,
                el.estado,
                el.datosCotizacion.total,
                <Grid container wrap="nowrap">
                  <Tooltip title="Guardar Rótulo">
                    <IconButton onClick={() => generarRotulo(el)}>
                      <DescriptionIcon />
                    </IconButton>
                  </Tooltip>
                  {botonGuardarGuia}
                  <Tooltip title="Eliminar Guía ">
                    <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                      <HighlightOffIcon />
                    </IconButton>
                  </Tooltip>
                </Grid>
              )
            );
            counter = counter + 1;

            if (counter === length) {
              //es la ultima guia del arreglo pero no es posible saber si si se consultaron todas las guias
              // setSpinner(false);
              //actualizo el servidor con las guias nuevas
              //enviar la info a el servidor y devolver el nuevo objeto de la base de datos
              //filtrar las guias que se actualizaron con un nuevo estado en el arreglo guiasInfoRelacion y actualizar ese arreglo
              let guiasInfoRelacionCopy = [];
              guiasInfoArrayCopy.forEach((el) => {
                props.userInfo.GuiasInfoRelacion.forEach((elRelacion) => {
                  if (el.numeroGuia === elRelacion.numeroGuia) {
                    //la guia en el arreglo general existe en el arreglo de relacion, actualizar el estado
                    elRelacion.estado = el.estado;
                    guiasInfoRelacionCopy.push(elRelacion);
                  }
                });
              });
              guardarListadoGuiasEnServidor(
                guiasInfoArrayCopy,
                guiasInfoRelacionCopy,
                true
              );
              //aca en vez de volver a llamar a rows, es necesario llamar otro estado que actualize las rows
              //por lo tanto rows no seria el elemento final a renderizar debe haber una rows final que se dispare
              //solo despues de que se actualizen las guias de aveonline y envia

              //setRows(rowsEffect);
              ;
              setMostrarRows({ estado: true, finalRows: rowsEffect });
              console.log(cotizarEstado);
              setCotizarEstado(false);
            }
          })
          .catch((err) => {
            console.log("hubo un error", err);
          });
      }
    });
    setCotizarEstado(false);  
    return () => {
      //cotizar estado esta generando false antes de que se actualizen las guias de todos los usuarios
      ;
      setCotizarEstado(false);
    };
  }, [cotizarEstado]);

  useEffect(()=>{
    
    if(!actualizarTodasLasGuias){
      return
    }
    let rowsEffect = [];
    let usersEmailsArray = Object.keys(store.getState().setDataInfo.dataInfo).slice(1);

    usersEmailsArray.forEach(user=>{
      ;
      store.getState().setDataInfo.dataInfo[user].GuiasInfo.forEach(el=>{
        const agentNameAndCode =
        el.agentInfo.nombreCompleto.length > 0
          ? el.agentInfo.nombreCompleto +
            " / Código: " +
            el.agentInfo.HORCODE
          : props.userInfo.HORCODE;
      let botonGuardarGuia = (
        <Tooltip title="Guardar Guía">
          <IconButton onClick={() => guardarGuiaBoton(el)}>
            <SaveAltIcon />
          </IconButton>
        </Tooltip>
      );
      if (el.datosCotizacion.transportadora.startsWith("M")) {
        botonGuardarGuia = null;
      }
      rowsEffect.push(
        createData(
          el.numeroGuia,
          el.datosRemitente.ordenDeCompra,
          el.datosCotizacion.transportadora,
          agentNameAndCode,
          el.datosCotizacion.origenDestino,
          el.horaActual,
          el.datosCotizacion.kilos,
          el.datosCotizacion.tipoEnvio,
          el.datosCotizacion.trayecto,
          el.datosCotizacion.diasEntrega,
          el.datosCotizacion.valoracion,
          el.datosCotizacion.fletePorKilo,
          el.datosCotizacion.fletePorRecaudo,
          el.datosCotizacion.manejo,
          el.datosCotizacion.recaudo,
          el.estado,
          el.datosCotizacion.total,
          <Grid container wrap="nowrap">
            <Tooltip title="Guardar Rótulo">
              <IconButton onClick={() => generarRotulo(el)}>
                <DescriptionIcon />
              </IconButton>
            </Tooltip>
            {botonGuardarGuia}
            <Tooltip title="Eliminar Guía ">
              <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                <HighlightOffIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        )
      );
      })
    })
   ;
    setMostrarRows({ estado: true, finalRows: rowsEffect });
    setActualizarTodasLasGuias(false);
    return ()=>{
      setActualizarTodasLasGuias(false);
    }
  }, [actualizarTodasLasGuias])

  useEffect(() => {
    
    if (mostrarRows.estado) {
      setSpinner(false);
      setSelected([]);
    }
  }, [mostrarRows]);
  
  //cuando se cancelan varias guias la app no se esta actualizando, por lo que es necesario forzar la actualizacion
  useEffect(()=>{
    let rowsEffect = [];
    if(renderizarCuandoSeCancelanVariasGuias){
      //forzar renderizado
      let todasLasGuias = store.getState().setDataInfo.dataInfo;
      delete todasLasGuias['comisiones'];
      const todasLasGuiasKeys = Object.keys(todasLasGuias);
      todasLasGuiasKeys.forEach(email=>{
        todasLasGuias[email].GuiasInfo.forEach(el=>{
          //agregar las guias al arreglo que va a renderizar todas las guias de los usuarios
          const agentNameAndCode =
          el.agentInfo.nombreCompleto.length > 0
            ? el.agentInfo.nombreCompleto +
              " / Código: " +
              el.agentInfo.HORCODE
            : props.userInfo.HORCODE;
        let botonGuardarGuia = (
          <Tooltip title="Guardar Guía">
            <IconButton onClick={() => guardarGuiaBoton(el)}>
              <SaveAltIcon />
            </IconButton>
          </Tooltip>
        );
        if (el.datosCotizacion.transportadora.startsWith("M")) {
          botonGuardarGuia = null;
        }
        rowsEffect.push(
          createData(
            el.numeroGuia,
            el.datosRemitente.ordenDeCompra,
            el.datosCotizacion.transportadora,
            agentNameAndCode,
            el.datosCotizacion.origenDestino,
            el.horaActual,
            el.datosCotizacion.kilos,
            el.datosCotizacion.tipoEnvio,
            el.datosCotizacion.trayecto,
            el.datosCotizacion.diasEntrega,
            el.datosCotizacion.valoracion,
            el.datosCotizacion.fletePorKilo,
            el.datosCotizacion.fletePorRecaudo,
            el.datosCotizacion.manejo,
            el.datosCotizacion.recaudo,
            el.estado,
            el.datosCotizacion.total,
            <Grid container wrap="nowrap">
              <Tooltip title="Guardar Rótulo">
                <IconButton onClick={() => generarRotulo(el)}>
                  <DescriptionIcon />
                </IconButton>
              </Tooltip>
              {botonGuardarGuia}
              <Tooltip title="Eliminar Guía ">
                <IconButton onClick={(ev) => cancelarGuia(ev, el)}>
                  <HighlightOffIcon />
                </IconButton>
              </Tooltip>
            </Grid>
          )
        );
        })
      })
    } else {
      return;
    }
    
    //reiniciar el estado a false
    setMostrarRows({ estado: true, finalRows: rowsEffect });
    setRenderizarCuandoSeCancelanVariasGuias(false);
  }, [renderizarCuandoSeCancelanVariasGuias])
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = mostrarRows.finalRows.map((n) => n.guia);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, guia) => {
    const selectedIndex = selected.indexOf(guia);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, guia);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const isSelected = (guia) => selected.indexOf(guia) !== -1;
  /**Muestra condicionalmente las guias dependiendo de la seleccion en BusquedaRelacionEnvios.js */
  const updateData = (transportadora, initialDate, finalDate) => {
    ;
    let guiasInfoArray = props.userInfo.GuiasInfo;
    let newInfoArray = [];
    guiasInfoArray.forEach((el) => {
      // revisar si la fecha es correcta
      const initialDateObject = new Date(initialDate);
      const finalDateObject = new Date(finalDate);
      const dateFromArrayElement = new Date(el.horaActualFormatoOriginal);
      //revisar si la hora esta en el rango
      if (
        dateFromArrayElement.getTime() > initialDateObject.getTime() &&
        dateFromArrayElement.getTime() < finalDateObject
      ) {
        newInfoArray.push(el);
        //setShowGuias(true);
        //console.log(dateFromArrayElement, initialDateObject, finalDateObject)
      } else {
        //mostrar un aviso que indique que no hay guias en las fechas especificadas
        // console.log(dateFromArrayElement, initialDateObject, finalDateObject)
        // setShowGuias(false);
      }
    });
    if (newInfoArray.length > 0) {
      //si se agregaron guias en las fechas especificadas
      setShowGuias(true);
      
      setGuiasArray([...newInfoArray]);
    } else {
      //ninguna guia se agrego en las fechas especificadas
      setShowGuias(false);
    }
  };
  const cancelarGuiasResponse = (status, response, cancelarVariasGuias) => {
    if (status === "success") {
     

      setTimeout(() => {
        
        store.dispatch(userInfo("set_user_info", response.data.userInfo));
        store.dispatch(dataInfo("set_data_info", response.data.dataInfo));
        setSpinner(false);
        setAlert("success");
        setErrorMessage("Las guias se han eliminado.");
        const getDivView = document.getElementById("errorMessage");
        if (getDivView !== null && getDivView !== undefined) {
          getDivView.scrollIntoView({ behavior: "smooth" });
        }
        if(cancelarVariasGuias){
        //revisar si se llega a esta punto desde cancelar guias de varios usuarios
        //si es asi renderizar de nuevo la app
        setRenderizarCuandoSeCancelanVariasGuias(true);
        }
      }, 2000);
    } else if (status === "error") {
      setSpinner(false);
      setAlert("error");
      setErrorMessage(
        "Hubo un error al eliminar las guias, revisa tu conexión a internet o intenta más tarde."
      );
      setTimeout(() => {
        const getDivView = document.getElementById("errorMessage");
        if (getDivView !== null && getDivView !== undefined) {
          getDivView.scrollIntoView({ behavior: "smooth" });
        }
      }, 250);
    }
  };
  const activateSpinner = () => {
    setSpinner(true);
  };
  const updateUsuarios = () => {
    setIframe(<div></div>);
  };
  const emptyRows =
    rowsPerPage -
    Math.min(rowsPerPage, mostrarRows.finalRows.length - page * rowsPerPage);
  const cellAllign = "center";
  let errorDiv;
  if (alert === "error") {
    errorDiv = (
      <Alert severity="error" id="errorMessage">
        {errorMessage}
      </Alert>
    );
  }
  if (alert === "success") {
    errorDiv = (
      <Alert severity="success" id="errorMessage">
        {errorMessage}
      </Alert>
    );
  }
  if (alert === "no hay error") {
    errorDiv = null;
  }
  let iframeDivHtml;
  if (iframe) {
    //crear iframe
    iframeDivHtml = iframeDiv;
  }
  let bodyHtml;
  if (showGuias && !spinner) {
    bodyHtml = (
      <div>
        <EnhancedTableToolbar
          numSelected={selected.length}
          selected={selected}
          userInfo={props.userInfo}
          Email={props.userSelected.Email}
          cancelarGuiasResponse={cancelarGuiasResponse}
          activateSpinner={activateSpinner}
          cancelarGuiaEnEnvia={cancelarGuiaEnEnvia}
        />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? "small" : "medium"}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={mostrarRows.finalRows.length}
            />
            <TableBody>
              {stableSort(mostrarRows.finalRows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.guia);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  let totalRow = row.total;
                  //establecer si totalRow es un arreglo o un numero
                  if (isNaN(totalRow)) {
                    //formatear el total dependiendo de la cantidad de elementos en el row
                    if (row.total.length === 2) {
                      totalRow = (
                        <Grid container direction="column">
                          <Grid item>{row.total[0]}</Grid>
                          <Grid item style={{ color: "red" }}>
                            {row.total[1]}
                          </Grid>
                        </Grid>
                      );
                    }
                    if (row.total.length > 2) {
                      totalRow = (
                        <Grid container direction="column">
                          <Grid item>{row.total[0]}</Grid>
                          <Grid item>{row.total[1]}</Grid>
                          <Grid item style={{ color: "red" }}>
                            {row.total[2]}
                          </Grid>
                        </Grid>
                      );
                    }
                  } else {
                    totalRow = <Grid item>{totalRow}</Grid>;
                  }

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.guia)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.guia}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.guia}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.orden}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.transportadora}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.agente}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.origenDestino}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.fecha}</TableCell>
                      <TableCell align={cellAllign}>{row.peso}</TableCell>
                      <TableCell align={cellAllign}>{row.tipo}</TableCell>
                      <TableCell align={cellAllign}>{row.cobertura}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.diasEntrega}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.valorDeclarado}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.fleteTransportadora}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.fletePorRecaudo}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.flete}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.valorRecaudo}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.estado}</TableCell>
                      <TableCell align={cellAllign}>{totalRow}</TableCell>
                      <TableCell align={cellAllign}>{row.actions}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100, 200]}
          component="div"
          count={mostrarRows.finalRows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />{" "}
      </div>
    );
  } else if (!showGuias) {
    bodyHtml = (
      <Alert className={classes.alertStyles} severity="error">
        No existen guias en el rango de fechas seleccionado.
      </Alert>
    );
  }
  if (showGuias && !spinner) {
    bodyHtml = (
      <div>
        <EnhancedTableToolbar
          numSelected={selected.length}
          selected={selected}
          userInfo={props.userInfo}
          Email={props.userSelected.Email}
          cancelarGuiasResponse={cancelarGuiasResponse}
          activateSpinner={activateSpinner}
          cancelarGuiaEnEnvia={cancelarGuiaEnEnvia}
        />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? "small" : "medium"}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={mostrarRows.finalRows.length}
            />
            <TableBody>
              {stableSort(mostrarRows.finalRows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.guia);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  let totalRow = row.total;
                  //establecer si totalRow es un arreglo o un numero
                  if (isNaN(totalRow)) {
                    //formatear el total dependiendo de la cantidad de elementos en el row
                    if (row.total.length === 2) {
                      totalRow = (
                        <Grid container direction="column">
                          <Grid item>{row.total[0]}</Grid>
                          <Grid item style={{ color: "red" }}>
                            {row.total[1]}
                          </Grid>
                        </Grid>
                      );
                    }
                    if (row.total.length > 2) {
                      totalRow = (
                        <Grid container direction="column">
                          <Grid item>{row.total[0]}</Grid>
                          <Grid item>{row.total[1]}</Grid>
                          <Grid item style={{ color: "red" }}>
                            {row.total[2]}
                          </Grid>
                        </Grid>
                      );
                    }
                  } else {
                    totalRow = <Grid item>{totalRow}</Grid>;
                  }

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.guia)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.guia}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.guia}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.orden}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.transportadora}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.agente}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.origenDestino}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.fecha}</TableCell>
                      <TableCell align={cellAllign}>{row.peso}</TableCell>
                      <TableCell align={cellAllign}>{row.tipo}</TableCell>
                      <TableCell align={cellAllign}>{row.cobertura}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.diasEntrega}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.valorDeclarado}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.fleteTransportadora}
                      </TableCell>
                      <TableCell align={cellAllign}>
                        {row.fletePorRecaudo}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.flete}</TableCell>
                      <TableCell align={cellAllign}>
                        {row.valorRecaudo}
                      </TableCell>
                      <TableCell align={cellAllign}>{row.estado}</TableCell>
                      <TableCell align={cellAllign}>{totalRow}</TableCell>
                      <TableCell align={cellAllign}>{row.actions}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100, 200]}
          component="div"
          count={mostrarRows.finalRows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />{" "}
      </div>
    );
  } else if (!showGuias) {
    bodyHtml = (
      <Alert className={classes.alertStyles} severity="error">
        No existen guias en el rango de fechas seleccionado.
      </Alert>
    );
  }
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <BusquedaRelacionEnvios
          showSteps={false}
          guiasInfo={props.userInfo.GuiasInfo}
          updateData={updateData}
        />
        {spinner ? (
          <Grid
            container
            justify="center"
            direction="column"
            alignContent="center"
            alignItems="center"
          >
            <CircularProgress style={{ marginTop: 10 }} />
            <Typography style={{ marginTop: 10 }}>
              Actualizando el estado de las guías
            </Typography>
          </Grid>
        ) : null}
        {mostrarRows.estado ? bodyHtml : null}
        {errorDiv}
        {iframeDivHtml}
      </Paper>
    </div>
  );
}

export default ListadoGuiasUsuarios;
