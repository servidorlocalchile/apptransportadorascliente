/* eslint-disable no-use-before-define */
import React, {useState, useEffect} from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ciudades from './ciudades';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  control: {
      padding: theme.spacing(1),
    }
}));
function sleep(delay = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

export default function CiudadesAutoComplete(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState([]);
  const loading = open && options.length === 0;

  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  useEffect(()=>{
    let active = true;
    if (!loading) {
      return undefined;
    }
    return () => {
      active = false;
    };
  }, [loading])
  const handleOptionLabelChange = (ev)=>{
    props.updateChildrenCityChange(ev.cityString, props.label)
    return ev.cityString;
  }
  const handleClose = ()=>{
    debugger;
    props.updateChildrenCityChange("", props.label);
  }
  const handleChange = (ev, values)=>{
    ev.persist();
    props.updateChildrenCityChange(values.cityString, props.label);

  }
  const inputChange = (ev, value)=>{
    if(value.length >= 3){
      const optionCities = ciudades.filter(city=>{
        if(city.cityString.toLowerCase().includes(value.toLowerCase())){
          return city.cityString;
        }
      });
      setOptions(optionCities);
    }
    
  }
 
  return (
    <Autocomplete
    className={classes.control}
      id={props.label}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      options={options}
      onInputChange={(ev, value)=>inputChange(ev, value)}
      getOptionSelected={(option, value) => option.cityString === value.cityString}
      getOptionLabel={(option)=> option.cityString}
      loading={loading}
      loadingText="Buscando ciudades"
      onChange={handleChange}
      style={{ width: 300 }}
      freeSolo={true}
      renderInput={(params) => <TextField {...params} label={props.label} variant="filled" 
      InputProps={{
        ...params.InputProps,
        endAdornment: (
          <React.Fragment>
            {loading ? <CircularProgress color="inherit" size={20} /> : null}
            {params.InputProps.endAdornment}
          </React.Fragment>
        ),
      }} />}
     
    />
  );
}


