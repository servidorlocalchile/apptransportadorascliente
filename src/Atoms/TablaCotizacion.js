import React, {useState} from 'react';
import LogoEnvia from '../Resources/Images/LogoEnvia.png';
import LogoTcc from '../Resources/Images/LogoTcc.jpg';
import LogoMensajerosUrbanos from '../Resources/Images/LogoMensajerosUrbanos.svg';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { DataGrid } from '@material-ui/data-grid';
import { makeStyles } from '@material-ui/core/styles';
import DatosRemitenteDestinatario  from './DatosRemitenteDestinatario';
import './tablaCotizacion.css';
import { is } from 'date-fns/locale';

/**
 * '& .MuiDataGrid-colCellWrapper.scroll' : {
      minWidth: '100%!important'
    }
 */
const useStyles = makeStyles({
  root: {
    fontSize : '12px',
    '& .MuiDataGrid-cellWithRenderer' :{
      wordBreak : 'break-word'

    }
    
  },
  grid : {
    marginTop :'15px'
  }
  
});
const LogoEnviaImg = (props)=><img style={{width : '40px'}} src={LogoEnvia}  alt='Logo Envía' />;
const TCCImg = (props)=><img style={{width : '40px'}} src={LogoTcc}  alt='Logo Tcc' />;
const MensajerosUrbanosImg = (props)=><img style={{width : '40px'}} src={LogoMensajerosUrbanos}  alt='Logo Mensajeros Urbanos' />;


    const widths = {
      minWidth : 40,
      minWidthMedian : 70,
      medianWidth : 90,
      maxWidths : 140,
      maxHiguerWidthsMax : 160,
      maxHiguerWidths : 180,
    }
/**
 * Calcula la cotización con los endpoints y renderiza una lista de las 
 * transportadoras disponibles y sus precios de envío.
 * @param {*} props 
 */
   const TablaCotizacion = (props)=> {
    const classes = useStyles();
    const [showDatos, setShowDatos] = useState(false);
    const [rowSelection, setRowSelection] = useState({})

    const columns = [
        /*{ field: 'id', headerName: 'ID', width: widths.minWidth },*/
        { field: 'transportadora', headerName: 'TRANSP',
        renderCell: (params) => {
          if(params.data.id == '1010'){
            //TCC
            return <TCCImg />;
          }
          if(params.data.id == '29'){
            //envia
            return < LogoEnviaImg />
          }
          //seguramente se trata de mensajeros urbanos
          return < MensajerosUrbanosImg />
        }
      },
        { field: 'origen', headerName: 'ORIGEN', 
          renderCell : (params)=>{
            return params.value;
          },
        width: widths.maxHiguerWidthsMax },
        { field: 'destino', headerName: 'DESTINO', 
          renderCell : (params)=>{
            return params.value;
          },
        width: widths.maxHiguerWidthsMax },
        {
          field: 'unidades',
          headerName: 'UNID',
          type: 'number',
         width: widths.minWidthMedian,
        },
        {
          field: 'kilos',
          headerName: 'KILOS',
          type : 'number',
          sortable: false,
          width: widths.minWidthMedian,/*
          valueGetter: (params) =>
            `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,*/
        },
        {
            field: 'valoracion',
            headerName: 'VALORACIÓN',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'tipoEnvio',
            headerName: 'TIPO DE ENVÍO',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'trayecto',
            headerName: 'TRAYECTO',
            type: 'string',
            width: widths.maxWidths,
          },
          {
            field: 'diasEntrega',
            headerName: 'Días Entrega',
            type: 'string',
            width: widths.maxWidths,
          },
          {
            field: 'fletePorKilo',
            headerName: 'FLETE X KILO',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'manejo',
            headerName: 'MANEJO',
            type: 'number',
           /* width: widths.maxWidths,*/
          },
          {
            field: 'fletePorRecaudo',
            headerName: 'FLETE X RECAUDO',
            type: 'number',
            width: widths.maxHiguerWidths,
          },
          {
            field: 'total',
            headerName: 'TOTAL',
            //type: 'number',
            renderCell : (params)=>{
              return params.value;
            },
           /* width: widths.maxWidths,*/
          }
      ];
      
      //aqui se generan los rows
      const rows = props.transportadorasCobertura.map(el=>{
        let totalText = parseInt(el.total,10).toLocaleString('es-CO');
        const origenText = <Typography style={{whiteSpace : 'normal'}} variant='caption'>{el.origen}</Typography>;
        const destinoText = <Typography style={{whiteSpace : 'normal'}} variant="caption">{props.stateProps.targetCity}</Typography>;
        let totalTextDiv = <Typography style={{whiteSpace : 'normal'}} variant='caption'>{totalText}</Typography>;
        
        //si solo se agrega valor a recaudar
        if(el.valorOtrosRecaudos !== 0 && !props.stateProps.state.checkedA && !props.stateProps.state.checkedB){
          const cobroUsuarioApp = el.fletexkilo + el.costoManejo + el.valorOtrosRecaudos;
          const money = parseInt(props.stateProps.recaudo, 10).toLocaleString("es-CO");
          totalText = <Grid container direction="column">
            <div>{parseInt(cobroUsuarioApp, 10).toLocaleString('es-CO')}</div>
            <div>Cobro a Dest:</div>
            <div>{money}</div>
          </Grid>
           totalTextDiv = <Typography variant='caption'>{totalText}</Typography>;
        }

        //revisar si el usuario selecciona flete, el destinatario paga recaudo o las dos opciones
      //usuario solo selecciona flete sin valor a recaudar
      if( props.stateProps.state.checkedA && !props.stateProps.state.checkedB && props.stateProps.recaudo === "" ){
        //mostrar en total cobro a destinatario porque el flete lo paga el destinatario y no el usuario de la app
        //el usuario de la app no asume ningun costo
        totalText = <Grid container direction="column">
          <div>Cobro a Dest:</div>
          <div>{el.total}</div>
        </Grid>
         totalTextDiv = <Typography style={{whiteSpace : 'normal'}} variant='caption'>{totalText}</Typography>;
      }
      
      //usuario agrega valor a recaudar y el destinatario asume el costo del recaudo y no selecciona flete
      if(el.valorOtrosRecaudos !== 0 && !props.stateProps.state.checkedA && props.stateProps.state.checkedB){
        //mostrar que se le cobra el porcentaje del valor a recaudar al destinatario
        //y el valor del envio al usuario de la app que es fleteporkilo mas manejo
        const cobroUsuarioApp = el.fletexkilo + el.costoManejo;
        const money = (parseInt(props.stateProps.recaudo, 10) + parseInt(el.valorOtrosRecaudos, 10)).toLocaleString("es-CO");

        totalText = <Grid container direction="column">
          <div>{cobroUsuarioApp}</div>
          <div>Cobro a Dest:</div>
          <div>{money}</div>
        </Grid>
         totalTextDiv = <Typography variant='caption'>{totalText}</Typography>;
         //agregar en el row de seleccion el cobro especifico para que aparezca en la factura
        /* el.cobroDiferencial = {
           clientePaga : cobroUsuarioApp,
           destinatarioPaga: el.valorOtrosRecaudos
         }*/
      }
       //destinatario paga el flete y el valor del recaudo lo paga el usuario
       if(el.valorOtrosRecaudos !== 0 && props.stateProps.state.checkedA && !props.stateProps.state.checkedB && props.stateProps.recaudo !== ""){
        //mostrar que se le cobra el porcentaje del valor a recaudar al destinatario
        //y el valor del envio al usuario de la app que es fleteporkilo mas manejo
        const cobroUsuarioApp = parseInt(el.valorOtrosRecaudos, 10).toLocaleString('es-CO');
        const money = (parseInt(el.fletexkilo, 10) + parseInt(props.stateProps.recaudo, 10) + parseInt(el.costoManejo)).toLocaleString("es-CO");
        totalText = <Grid container direction="column">
          <div>{cobroUsuarioApp}</div>
          <div>Cobro a Dest:</div>
          <div>{money}</div>
        </Grid>
         totalTextDiv = <Typography variant='caption'>{totalText}</Typography>;
         //agregar en el row de seleccion el cobro especifico para que aparezca en la factura
        /* el.cobroDiferencial = {
           clientePaga : cobroUsuarioApp,
           destinatarioPaga: el.valorOtrosRecaudos
         }*/
      }
      //el usuario agrega valor a recaudar, selecciona flete y asume el costo del recaudo
      if(el.valorOtrosRecaudos !== 0 && props.stateProps.state.checkedA && props.stateProps.state.checkedB){
        //al usuario de la app no se le cobra nada y todos los costos recaen sobre el destinatario
        const money = (parseInt(el.total, 10) + parseInt(props.stateProps.recaudo, 10)).toLocaleString("es-CO");
        totalText = <Grid container direction="column">
          <div>Cobro a Dest:</div>
          <div>{money}</div>
        </Grid>;
         totalTextDiv = <Typography variant='caption'>{totalText}</Typography>;
      }

     //establecer si la guia se genero con la api de envia
     const transportadoraKeys = Object.keys(el);
     let cotizarConApiEnvia = false;
     let ciudadesDane = {};
     if(transportadoraKeys.includes('cotizacionConApiEnvia')){
       //se genero con la api de envia porque incluye la key
       cotizarConApiEnvia = true;
       ciudadesDane = el.ciudadesDane;
     }

        return {
          id: el.codTransportadora, transportadora: el.nombreTransportadora, 
          origen: origenText, 
          destino : destinoText,
          unidades: el.unidades, kilos : el.kilos, 
        valoracion : parseInt(el.valoracion, 10).toLocaleString("es-CO"), tipoEnvio : el.tipoEnvio,
        trayecto : el.trayecto,
        diasEntrega : el.diasentrega,
         fletePorKilo : parseInt(el.fletexkilo,10).toLocaleString("es-CO"), //a este valor se debe agregar un porcentaje para la ganancia
        manejo : parseInt(el.costoManejo, 10).toLocaleString("es-CO"), 
        fletePorRecaudo : parseInt(el.valorOtrosRecaudos, 10).toLocaleString("es-CO"), //a este valor tambien se le debe agregar un porcentaje para la ganancia
        total : totalTextDiv,
        cotizarConApiEnvia : cotizarConApiEnvia,
        ciudadesDane : ciudadesDane
      }
      })
    return (
      <Grid container>
      <Grid container>
       <Grid container direction='column' justify='center' alignItems='center' className={classes.grid} > 
    <Typography variant="h6" gutterBottom>
       2. Cotización
      </Typography>
      </Grid>
      <div style={{ display: 'flex', height: '100%', width : '100%' }}>
  <div style={{ flexGrow: 1 }} id="parentDiv">
  <DataGrid autoHeight rows={rows} columns={columns} pageSize={5} className={classes.root} 
  onSelectionChange={(newSelection) => {
          setShowDatos(true);
          let selection = rows.filter(el=>el.id === newSelection.rowIds[0]);
          if(selection.length > 0){
                 //destino y origen son un elemento react asi que es necesario extraer el string y modificar el valor original
          selection[0].origen = selection[0].origen.props.children;
          selection[0].destino = selection[0].destino.props.children;
          //antes de estableecer la seleccion de la hilera es necesario establecer si el recaudo es recalculado o no
         if(props.stateProps.recaudoRecalculadoBool){
           // selection[0].valorRecaudo =
           //establecer la inicial de la transportadora elegida por el usuario
          const transportadoraTexto = selection[0].transportadora.toLowerCase();
          //establecer un arreglo con los nombres de cada transportadora
          const arregloTransportadorasComisionesNombres = Object.keys(props.stateProps.props.comisiones);
          arregloTransportadorasComisionesNombres.forEach(el=>{
            if(el.startsWith(transportadoraTexto[0])){
              //se trata de la transportadora, recalcular el recaudo
             const comisionesAcalcular = props.stateProps.props.comisiones[el];
             //importante establecer que pasa cuando el usuario selecciona mas de un kilo***** queda PENDIENTE!!!
             const valoracionInt = parseInt(props.stateProps.recaudo, 10);
             let totalNuevo =  (valoracionInt / 100) * comisionesAcalcular.porcentajeRecaudo +   comisionesAcalcular.fletePorKilo + valoracionInt;
              //establecer si el usuario elijio una de las dos opciones o las dos
              if(props.stateProps.state.checkedA && !props.stateProps.state.checkedB){
                //si elije solo flete
                totalNuevo = comisionesAcalcular.porcentajeRecaudo +   comisionesAcalcular.fletePorKilo + valoracionInt;
              } else if(!props.stateProps.state.checkedA && props.stateProps.state.checkedB){
                //si elije solo recaudo se coloca el total de la valoracion + el 1% 
                totalNuevo = (valoracionInt / 100) * comisionesAcalcular.porcentajeRecaudo + valoracionInt;
                // comisionesAcalcular.porcentajeRecaudo +   comisionesAcalcular.fletePorKilo + valoracionInt;

              }
               selection[0].valorRecaudo = totalNuevo;
               
            }
          })

          }
            setRowSelection(selection[0]);
          setTimeout(function(){
            const getDivView = document.getElementById('datosRemitenteViewId');
            if(getDivView !== null && getDivView !== undefined){
              getDivView.scrollIntoView({behavior: "smooth"});
            }
            
           }, 250);
          }
     
        }}/>
  </div>
</div>
      </Grid>
      <div id='cotizacionIdView'></div>
      {showDatos ?<DatosRemitenteDestinatario  
      aditionalInfo={{
        dimensiones : {
          alto : props.stateProps.alto,
          ancho: props.stateProps.ancho,
          largo : props.stateProps.largo
        },
        detalles : {
          ciudadOrigen : props.stateProps.city,
          ciudadDestino : props.stateProps.targetCity,
          contenido : props.stateProps.contenido,
          kilos : props.stateProps.kilos,
          unidades : props.stateProps.units,
          seguro : props.stateProps.seguro,
          state : props.stateProps.state
        }
      }} 
      agentInfo={props.stateProps.agent} 
      rowSelection={rowSelection} 
      recaudoCalculadoBool={props.stateProps.recaudoRecalculadoBool}
      valorRecaudo={props.stateProps.recaudo}/> : null }
     
      
      </Grid>
    );
  }
  

  
  export default TablaCotizacion;

  