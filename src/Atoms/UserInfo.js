import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
    color : theme.palette.primary.contrastText,
    [theme.breakpoints.down('xs')]: {
      fontSize: 'xx-small'
    }
  },
  secondary : {
    color :
    theme.palette.secondary.light,
    [theme.breakpoints.down('xs')]: {
      fontSize: 'xx-small'
    }
  },
 

}));

export default function UserInfo(props) {
  const classes = useStyles();
  const margin = props.Title === 'Nombre' ? {marginLeft : '5px'} : null;
  return (      
                <Grid alignItems='center' container direction='column' justify='center' >
                <Typography variant='subtitle1' className={classes.root}>{props.Title}</Typography> 
                <Typography style={margin} variant='subtitle1' className={classes.secondary}>{props.Data}</Typography>  
                </Grid> 
  );
}
