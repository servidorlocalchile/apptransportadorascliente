import { jsPDF } from "jspdf";
import 'jspdf-autotable';
import LogoHorenvios from '../Resources/Images/LogoHorenvios.png';
import LogoEnvia from '../Resources/Images/LogoEnvia.png';
import LogoTcc from '../Resources/Images/LogoTcc.jpg';
import ReportProblemIcon from '../Resources/Images/alert.png';
import generarRotulo from '../Atoms/generarRotulo';

const generarRotulos = (selected, guias) => {
       const rotulosGuardar =  selected.map((guia)=>{
        //buscar las guias seleccionadas en las guias de la base de datos
         const rotulosFiltrados = guias.filter((el)=>{
            if(el.numeroGuia === guia){
                return el;
            }
            
        });
        return rotulosFiltrados;
    });    
    //guardar cada una de las guias seleccionadas
    rotulosGuardar.forEach(rotulo=>{
        generarRotulo(rotulo[0]);
    })
}

export default generarRotulos;