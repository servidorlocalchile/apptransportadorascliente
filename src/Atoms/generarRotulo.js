import { jsPDF } from "jspdf";
import 'jspdf-autotable';
import LogoHorenvios from '../Resources/Images/LogoHorenvios.png';
import LogoEnvia from '../Resources/Images/LogoEnvia.png';
import LogoTcc from '../Resources/Images/LogoTcc.jpg';
import LogoMensajeros from '../Resources/Images/logo-mensajeros-urbanos.png';

import ReportProblemIcon from '../Resources/Images/alert.png';


const generarRotulo = props => {
    console.log(props);
    const columns = [
        { title: "Nombre del Remitente", dataKey: "nombreRemitente" },
        { title: "Teléfono", dataKey: "teléfono" },
        { title: "Celular", dataKey: "celular" },
        { title: "Correo", dataKey: "correo" },
        { title: "Ciudad", dataKey: "ciudad" },
        { title: "Barrio", dataKey: "barrio" },
        { title: "Dirección", dataKey: "dirección" },
        { title: "Orden de Compra", dataKey: "orden" },
        { title: "Contenido", dataKey: "contenido" },
        { title: "Unidades", dataKey: "unidades" }
    ];
    const rows = [
        {
            "nombreRemitente": props.datosRemitente.nombreCompleto,
            "teléfono": props.datosRemitente.fijo,
            "celular": props.datosRemitente.celular,
            "correo": props.datosRemitente.correo,
            "ciudad": props.datosRemitente.ciudadRemitente,
            "barrio": props.datosRemitente.barrio,
            "dirección": props.datosRemitente.direccion,
            "orden": props.datosRemitente.ordenDeCompra,
            "contenido": props.informacionAdicional.detalles.contenido,
            "unidades": props.informacionAdicional.detalles.unidades

        }
    ]

    const columnsDestinatario = [
        { title: "Nombre del Destinatario", dataKey: "nombreDestinatario" },
        { title: "Teléfono", dataKey: "teléfono" },
        { title: "Celular", dataKey: "celular" },
        { title: "Correo", dataKey: "correo" },
        { title: "Ciudad", dataKey: "ciudad" },
        { title: "Barrio", dataKey: "barrio" },
        { title: "Dirección", dataKey: "dirección" },
        { title: "Observación", dataKey: "observación" }
    ];

    const rowsDestinatario = [
        {
            "nombreDestinatario": props.datosDestinatario.nombreCompleto,
            "teléfono": props.datosDestinatario.fijo,
            "celular": props.datosDestinatario.celular,
            "correo": props.datosDestinatario.correo,
            "ciudad": props.datosDestinatario.ciudadDestinatario,
            "barrio": props.datosDestinatario.barrio,
            "dirección": props.datosDestinatario.direccion, 
            "observación": props.datosDestinatario.referencia
        }
    ];
    
    let LogoTransportadora;
    let widthImage = 20;
    let heightImage = 20;
    if(props.datosCotizacion.transportadora.startsWith("E")){
        LogoTransportadora = LogoEnvia;
    }
    if(props.datosCotizacion.transportadora.startsWith("T")){
        LogoTransportadora = LogoTcc;
    }
    if(props.datosCotizacion.transportadora.startsWith("M")){
        LogoTransportadora = LogoMensajeros;
        widthImage = 20;
        heightImage = 13;
    }
    
    const doc = new jsPDF()
    doc.autoTable(columns, rows, {
        //startY: doc.lastAutoTable.finalY + 30,
        styles: { fillColor: "darkgray", fontSize: 8 },
        headStyles: { fillColor: "#303f9f" },
        margin: { top: 40 },
        beforePageContent: function (data) {
            const horizontalPositionPlus = 50;
            doc.setFontSize(8)
            const fecha = props.horaActual;
            doc.text(fecha, 20, 5);
            doc.setFontSize(12)
            doc.text(20, 15, 'Guía # ' + props.numeroGuia)
            doc.setFontSize(14)
            doc.setFont("helvetica", "bold");
            doc.text(90,  15, 'Horenvios');
            doc.addImage(LogoTransportadora, 'JPEG', 20, 20,  widthImage, heightImage);
            doc.addImage(LogoHorenvios, 'JPEG', 150, 20, 20, 20);
            doc.setFont("times", "normal")
            doc.text(90, 25, 'Datos Remitente')

        }
    });

    doc.autoTable(columnsDestinatario, rowsDestinatario, {
        startY: doc.lastAutoTable.finalY + 20,
        styles: { fillColor: "darkgray", fontSize: 8 },
        headStyles: { fillColor: "#303f9f" },
        beforePageContent : function (data) {
            doc.setFontSize(14)
            doc.setFont("times", "normal")
            doc.text(90, doc.lastAutoTable.finalY + 10, 'Datos Destinatario')
        }   
    });
    //aviso
    doc.rect(20, doc.lastAutoTable.finalY + 10, 173, 100)
    doc.addImage(ReportProblemIcon, 'JPEG', 100, doc.lastAutoTable.finalY + 25, 10, 10);
    doc.setFont("helvetica", "bold");
    doc.setFontSize(14)
    doc.text(98, doc.lastAutoTable.finalY + 40, "Alerta");
    doc.setFont("times", "normal")
    doc.setFontSize(12)
    doc.text(77, doc.lastAutoTable.finalY + 50, "Apreciado cliente, si usted encuentra:");
    doc.setFont("helvetica", "bold");
    doc.setFontSize(10)
    doc.text(25, doc.lastAutoTable.finalY + 60, "1.");
    doc.text(25, doc.lastAutoTable.finalY + 70, "2.");
    doc.text(25, doc.lastAutoTable.finalY + 80, "3.");

    doc.setFont("times", "normal")
            doc.text(30, doc.lastAutoTable.finalY + 60, " Roto este sello.");
            doc.text(30, doc.lastAutoTable.finalY + 70, " Observa alguna novedad en la unidad del empaque.");
            doc.text(30, doc.lastAutoTable.finalY + 80, " Evidencia maltrato en la unidad del empaque.");
            doc.text(30, doc.lastAutoTable.finalY + 90, " -Asegúrese de registrar la novedad al lado de su firma en la constancia de entrega.")
            doc.text(30, doc.lastAutoTable.finalY + 95, " -Si usted no registra la novedad en la guía: ")
            doc.setFont("helvetica", "bold");
            doc.setFontSize(8)
            doc.text(30, doc.lastAutoTable.finalY + 100, " EL REMITENTE NO SE HARÁ RESPONSABLE POR NINGÚN TIPO DE RECLAMACION POR PÉRDIDA Y/O AVERÍA. ")
            



    doc.save("numeroRelacion" + '.pdf');
}

export default generarRotulo;