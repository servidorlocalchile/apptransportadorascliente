import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import Tooltip from '@material-ui/core/Tooltip';
import SearchIcon from '@material-ui/icons/Search';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { uuid } from 'uuidv4';

import './busquedaRelacionEnvios.css';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '5px',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 160,
    },
    searchButtonStyles : {
        cursor : 'pointer',
        '&:hover' : {
           color : theme.palette.secondary.main 
        }
    },
    selectGrid : {
        width : '40%'
    }
}));
/**Busca las guias generadas entre las fechas elegidas por el usuario */
const BusquedaRelacionEnvios = (props) => {
    const classes = useStyles();
    const matches = useMediaQuery('(max-width:430px)');
    let fecha = new Date();
    fecha.setDate(fecha.getDate());
    fecha.setHours(23,59,59,999);
    let fechaInicio = new Date();
    fechaInicio.setDate(fecha.getDate() -1);
    fechaInicio.setHours(0,0,0,0);
    const [transportadora, setTransportadora] = useState('');
    const [selectedInitialDate, setSelectedInitialDate] = useState(fechaInicio);
    const [selectedFinalDate, setSelectedFinalDate] = useState(fecha);
  
    const handleInitialChange = (date) => {
        setSelectedInitialDate(date);
      };
      const handleFinalChange = (date) => {
        setSelectedFinalDate(date);
      };
    const handleChange = (event) => {
        setTransportadora(event.target.value);
    };
    const buscarGuias = ()=>{
        props.updateData(transportadora, selectedInitialDate, selectedFinalDate )
    }
    let transportadorasArray = [];
    props.guiasInfo.forEach((el) => {
        let transpEl = el.datosCotizacion.transportadora;
        let isThere = transportadorasArray.indexOf(transpEl);
        if (isThere === -1) {
            //no está en el arreglo,  añadir
            transportadorasArray.push(transpEl)
        }
    })
    return (
        <div className={classes.root}>
            <  Grid container wrap={matches ? 'wrap' : 'nowrap' }justify='center' alignContent='center' alignItems='center' >
                {props.showSteps ?
                <Grid className={classes.selectGrid} container direction='column' justify='center'alignContent='center' alignItems='center'>
                <Typography variant="subtitle1" gutterBottom>
                   1. Seleccione la Transportadora
                 </Typography>
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id="demo-simple-select-filled-label">Transportadora</InputLabel>
                    <Select
                        labelId="demo-simple-select-filled-label"
                        id="demo-simple-select-filled"
                        value={transportadora}
                        onChange={handleChange}
                    >
                        {transportadorasArray.length === 0 ? <MenuItem key={uuid()} value='Aún no hay guías generadas'>Aún no hay guías generadas</MenuItem>: null}
                        {transportadorasArray.length > 0 ? <MenuItem key={uuid()} value='Seleccionar Todas las Transportadoras'>Seleccionar Todas las Transportadoras</MenuItem>: null }
                        {transportadorasArray.map((el) => <MenuItem key={uuid()} value={el}>{el}</MenuItem>)}
                    </Select>
                </FormControl>
               
            </Grid> : null
                }
                
                <Grid container direction='column' justify='center'alignContent='center' alignItems='center'>
                        <Typography variant="subtitle1" gutterBottom>
                           {props.showSteps ? 2. : null} Seleccione el Rango de Fechas
      </Typography>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Grid container wrap='nowrap'>
      <Grid container justify="space-around">
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="dd/MM/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Fecha Inicio"
          value={selectedInitialDate}
          onChange={handleInitialChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>
        <Grid container justify="space-around" alignItems="center" wrap="nowrap">
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="dd/MM/yyyy"
          margin="normal"
          id="date-picker-inline2"
          label="Fecha Final"
          value={selectedFinalDate}
          onChange={handleFinalChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        <Tooltip title="Buscar Guías">
  <SearchIcon className={classes.searchButtonStyles}onClick={buscarGuias} />
</Tooltip>
        </Grid>
        </Grid>
        </MuiPickersUtilsProvider>
                            
                    </Grid>

            </Grid>

        </div>
    )
}

export default BusquedaRelacionEnvios;