import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import {Link} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    
      margin: '5px',
    
  },
  links : {    
    color : 'white',
textDecoration : 'none',
minWidth : 80,
},
buttonGroup : {
    flexGrow: 1},
    stylesButtons : {
      borderRightColor : 'inherit!important',
      backgroundColor : '#a0d46899'
    }
    
}));

const DashboardActionButtons = (props)=>{
    const classes = useStyles();
    const matches = useMediaQuery('(max-width:750px)');

    let buttonRoutes = [];
    if(props.userInfo === undefined){}
    else if(props.userInfo.CuentaHabilitada && props.userInfo.CodigoSuperUsuario === undefined){
      buttonRoutes = 
      <ButtonGroup className={classes.buttonGroup} style={{flexDirection : matches ? 'column' : 'row'}} size='small' color="secondary" aria-label="outlined primary button group">
           <Button className={classes.stylesButtons} ><Link to="/Cotizar Envio" className={classes.links}>Cotizar envío</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Listado de Guías" className={classes.links}>Listado de Guías</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Relación de Guías" className={classes.links}>Relación de Guías</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Crear Agente" className={classes.links}>Crear Agente</Link></Button>
          </ButtonGroup>

    } else if(props.userInfo.CuentaHabilitada && props.userInfo.CodigoSuperUsuario !== undefined){
      //se trata de un super usuario 
      buttonRoutes = 
      <Paper elevation={8} style={{backgroundColor : '#a0d46899'}}>
      <ButtonGroup className={classes.buttonGroup} style={{flexDirection : matches ? 'column' : 'row'}} size='small' color="secondary" aria-label="outlined primary button group">
           <Button className={classes.stylesButtons}><Link to="/Cotizar Envio" className={classes.links}>Cotizar envío</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Listado de Guías" className={classes.links}>Listado de Guías</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Relación de Guías" className={classes.links}>Relación de Guías</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Crear Agente" className={classes.links}>Crear Agente</Link></Button>
          <Button className={classes.stylesButtons}><Link to="/Usuarios" className={classes.links}>Usuarios</Link></Button>

          </ButtonGroup>
          </Paper>
    } else if(!props.userInfo.CuentaHabilitada){
      buttonRoutes = 
      <ButtonGroup className={classes.buttonGroup} style={{flexDirection : matches ? 'column' : 'row'}} size='small' color="secondary" aria-label="outlined primary button group">
      <Button className={classes.stylesButtons}><Link to="/Subir Documentos" className={classes.links}>Subir Documentos</Link></Button>;
      </ButtonGroup>;

    }
    return(
        <div className={classes.root}>
          {buttonRoutes}
        </div>
    )
}
function mapStateToProps(state) { 
  return {
     userInfo : state.setUserInfo.userInfo
  };
};
export default connect(mapStateToProps)(DashboardActionButtons)

