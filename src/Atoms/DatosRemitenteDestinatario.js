

import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import store from '../Redux/store';
import { userInfo, dataInfo } from '../Redux/actions';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { registrarHoraActual } from '../Utils/useFullFunctions';
import { connect } from 'react-redux';
import { validateEmail } from '../Utils/useFullFunctions';
import aveonline from '../Apis/aveonline';
import generarGuiaEnvia from '../Apis/generarGuiaEnvia';
import Spinner from '../Atoms/Spinner';
import enviaGeneracionGuia from '../Apis/enviaGeneracionGuia';
import getMunicipiosCiudadesText from '../Apis/getMunicipiosCiudadesText';
let ip = '';
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  // dev code
  ip = 'https://localhost/guardarGuias';
} else {
  // production code
  ip = `${process.env.REACT_APP_URL}/guardarGuias`;
  //;

}

const useStyles = makeStyles((theme) => ({
  control: {
    padding: theme.spacing(1),
  },
  gridWidth: {
    width: 'auto'
  },
  buttonStyles: {
    marginTop: '5px'
  }

}));
/**
 * Renderiza los textInputs donde el usuario puede añadir los datos
 * del destinatario y el remitente.
 * @param {} props 
 */
const DatosRemitenteDestinatario = (props) => {
  const classes = useStyles();
  const [state, setState] = useState({
    nombreCompleto: '',
    direccion: '',
    correo: '',
    fijo: '',
    celular: '',
    ciudadRemitente: props.aditionalInfo.detalles.ciudadOrigen,
    barrio: '',
    nit : '',
    ordenDeCompra: ''
  });
  const [destinatario, setDestinatario] = useState({
    nombreCompleto: '',
    direccion: '',
    referencia : '',
    correo: '',
    fijo: '',
    celular: '',
    ciudadDestinatario: props.aditionalInfo.detalles.ciudadDestino,
    barrio: '',
    nit: ''
  })
  const [disableButton, setDisableButton] = useState(true);
  const [alert, setAlert] = useState('success')
  const [emailAlertDestinatario, setEmailAlertDestinatario] = useState(false);
  const [emailAlertRemitente, setEmailAlertRemitente] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const history = useHistory();

  const handleClick = () => {
    //GENERAR LA GUIA
    setSpinner(true);
    setTimeout(()=>{
      const getDivView = document.getElementById('spinnerId');
            if(getDivView !== null && getDivView !== undefined){
              getDivView.scrollIntoView({behavior: "smooth"});
            }
    }, 250)
    
    //establecer si la guia se debe generar con la api de envia o con la api de aveonline
    if(props.rowSelection.cotizarConApiEnvia){
      //se debe cotizar con la api de envia
         //la respuesta contiene los codigos dane del origen y el destino, se puede generar la guia
           let codCuenta  = "478";
           if(props.aditionalInfo.detalles.state.checkedA){
            codCuenta = "479";
           }
           else if(parseInt(props.valorRecaudo, 10) > 0){
            codCuenta = "479";
           }
                      
           const enviaGuiaBody = {
            "ciudad_origen": props.rowSelection.ciudadesDane.Origen,
            "ciudad_destino": props.rowSelection.ciudadesDane.Destino,
            "cod_formapago": 4,
            "cod_servicio": "12",
            "mca_nosabado": 1,
            "mca_docinternacional": 0,
            "cod_regional_cta": 17,
            "cod_oficina_cta": 1,
            "cod_cuenta": codCuenta,
            "con_cartaporte": "0",
            "info_cubicacion": [
                {
                    "cantidad": props.rowSelection.unidades,
                    "largo": props.aditionalInfo.dimensiones.largo,
                    "ancho": props.aditionalInfo.dimensiones.ancho,
                    "alto": props.aditionalInfo.dimensiones.alto,
                    "peso": props.rowSelection.kilos,
                    "declarado": parseInt(props.rowSelection.valoracion.split('.').join(""), 10)
                }
            ],
            "info_origen": {
                "nom_remitente": state.nombreCompleto,
                "dir_remitente": state.direccion,
                "tel_remitente": state.celular,
                "ced_remitente": state.nit
            },
            "info_destino": {
                "nom_destinatario": destinatario.nombreCompleto,
                "dir_destinatario": destinatario.direccion,
                "tel_destinatario": destinatario.celular,
                "ced_destinatario": destinatario.nit
            },
            "info_contenido": {
                "dice_contener": props.aditionalInfo.detalles.contenido,
                "texto_guia": "",
                "accion_notaguia": "",
                "num_documentos": "",
                "centrocosto": ""
            },
            "numero_guia": ""
        };
        //establecer si se agrega valorProducto o no y cuanto seria el total, esta es la parte mas importante
        if(codCuenta === "479"){
          //se debe agregar un valor producto
          let valorProducto = 0;
          //si solo se agrega el valor recaudo
          if(!props.aditionalInfo.detalles.state.checkedA && !props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== ""  ){
            //el total de la guia debe ser el valor recaudo
            valorProducto = parseInt(props.valorRecaudo.split('.').join(""), 10);
          }
          //si se elije valor recaudo y destinatario paga el flete
          else if(props.aditionalInfo.detalles.state.checkedA && !props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== "" ){
            //el total de la guia debe ser el valor recaudo mas el flete por kilo mas el manejo
            valorProducto = parseInt(props.valorRecaudo.split('.').join(""), 10) + parseInt(props.rowSelection.fletePorKilo.split('.').join(""), 10) + parseInt(props.rowSelection.manejo.split('.').join(""), 10);
          }
          //si elije valor recaudo y destinatario paga el recaudo
          else if(!props.aditionalInfo.detalles.state.checkedA && props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== "" ){
            //el valor de la guia debe ser valorRecaudo mas flete por recaudo
            valorProducto = parseInt(props.valorRecaudo.split('.').join(""), 10) + parseInt(props.rowSelection.fletePorRecaudo.split('.').join(""), 10);
          }
          
          //si elije valor recaudo, paga el flete y paga el recaudo
          else if(props.aditionalInfo.detalles.state.checkedA && props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== ""){
            //el total debe ser la suma de todos los valores
            const totalAgregar = parseInt(props.valorRecaudo.split('.').join(""), 10) + parseInt(props.rowSelection.fletePorRecaudo.split('.').join(""), 10) +  parseInt(props.rowSelection.fletePorKilo.split('.').join(""), 10) + parseInt(props.rowSelection.manejo.split('.').join(""), 10);
            valorProducto = totalAgregar;
          } 
          //si no agrega valor recaudo pero agrega paga flete
          else if(props.aditionalInfo.detalles.state.checkedA && props.valorRecaudo === ""){
            //el total debe ser flete por kilo mas manejo mas flete por recaudo
            valorProducto =  parseInt(props.rowSelection.fletePorRecaudo.split('.').join(""), 10) +  parseInt(props.rowSelection.fletePorKilo.split('.').join(""), 10) + parseInt(props.rowSelection.manejo.split('.').join(""), 10);

          }
          enviaGuiaBody.info_contenido.valorproducto = valorProducto;
        }
          const enviaPromise = generarGuiaEnvia(process.env.REACT_APP_ENVIA_HTTPS_BASE_URL + '/ServicioLiquidacionREST/Service1.svc/Generacion/',enviaGuiaBody);
          enviaPromise.then(answer=>{
            setSpinner(false);
            //si la respuesta no es un error
            if(answer.data.respuesta === ""){
              const numeroFacturaGuia = answer.data.guia;
            let resultado = answer.data;
            //se agrega la url de la guia al resultado para que tenga el mismo valor que el resultado
            //de aveonline
            resultado.rutaguia = answer.data.urlguia;
            const obtenerHoraActual = registrarHoraActual();
            const horaActualLegible = obtenerHoraActual.horaActualLegible;
            //total es un fiberNode por lo que se debe extraer el valor 
            let fiberNodeChildrenArray;
            //a veces props.rowSelection.total no tiene props, si es asi entonces no buscar en props.children
            const checkIfPropsExist = 'props' in props.rowSelection.total;
            if(checkIfPropsExist){
              fiberNodeChildrenArray = props.rowSelection.total.props.children; //.props.children; //antes tenia props.children ahora no
            } else {
              fiberNodeChildrenArray = props.rowSelection.total;
            }                         
            let totalValues;
            //revisar si en children del div hay un arreglo o un numero
            if(isNaN(fiberNodeChildrenArray) ){
              if(checkIfPropsExist){
                fiberNodeChildrenArray = props.rowSelection.total.props.children.props.children; //.props.children.props; //antes tenia props.children ahora no
              } else {
                fiberNodeChildrenArray = props.rowSelection.total;
              }
               totalValues = fiberNodeChildrenArray.map(el=>{ //antes era fiberNodeChildrenArray.children.map
                if(checkIfPropsExist){
                  return el.props.children; //antes tenia props.children ahora no
                } else {
                  return el;
                }
              });
            } else {
              totalValues = fiberNodeChildrenArray;
            }              
            let datosCotizacion = props.rowSelection;
            datosCotizacion.total = totalValues;
            datosCotizacion.recaudo = props.valorRecaudo;
            datosCotizacion.origenDestino = datosCotizacion.origen + ' / ' + datosCotizacion.destino;
            //formatear el nombre de transportadora del formato (Envia-nacional) a (Envia) para poder realizar la busqueda en relacion de envios
            datosCotizacion.transportadora = datosCotizacion.transportadora.split("-")[0];
            const data = {
              agentInfo: props.agentInfo,
              datosRemitente: state,
              datosDestinatario: destinatario,
              datosCotizacion: datosCotizacion,
              informacionAdicional: props.aditionalInfo,
              numeroGuia: numeroFacturaGuia,
              horaActual: horaActualLegible,
              horaActualFormatoOriginal: obtenerHoraActual.horaActual,
              rotulo : answer.data.urlguia,
              rutaGuia : answer.data.urlguia,
              estado : 'Generada',
              resultado : resultado
            }
            const newDataArray = [];
            newDataArray.push(data)
            //enviar la info a el servidor y devolver el nuevo objeto de la base de datos
            const options = {
              headers: { credentials: 'include' }
            };
              axios.put(ip, {
              Email: props.userInfo.Email,
              GuiasInfo: newDataArray,
              GuiasInfoRelacion: newDataArray
            }, options)
              .then(function (response) {
                //en vez de usar crear guias modificar userInfo con la nueva info que llega del servidor
                store.dispatch(userInfo('set_user_info', response.data.userInfo));
                store.dispatch(dataInfo('set_data_info', response.data.dataInfo))
                history.push('/Listado de Guías')
        
              })
              .catch(function (error) {
                console.log('hubo un error al comunicarse con el servidor local', error.response);
                setAlert('error');
                setErrorMessage('Hubo un error al guardar las guias, revisa tu conexión a internet o intenta más tarde.')
              });
            }
            else {
              //la respuesta es un error
              console.log('hubo un error al generar la guia', answer.data.respuesta);
              setAlert('error');
              setErrorMessage(answer.data.respuesta);
           
         
            }
            
          })
          .catch(err=>{
            console.log('hubo un error al generar la guia', err);
            setAlert('error');
            setErrorMessage('Hubo un error al guardar las guias, revisa tu conexión a internet o intenta más tarde.');
         
          })      
    }
    else {
      //se debe cotizar con aveonline
      //autenticacion
    const dataAuthBody = {
      tipo: 'auth',
      usuario: 'innovacion',
      clave: '019*'
    }
    const aveonlinePromiseAuth = aveonline('https://aveonline.co/api/comunes/v1.0/autenticarusuario.php', dataAuthBody);
    aveonlinePromiseAuth.then(resp => {
      let generacionGuiaBody;
      let valorTotalQueSeEnviaParaGenerarLaGuia;
      //1 no se paga nada
      if( !props.aditionalInfo.detalles.state.checkedA && !props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo === "" ){
        //es un envio normal asi que no se se envia un valor recaudo para generar la guia
        valorTotalQueSeEnviaParaGenerarLaGuia = 0;
      }
      //2 solo valor recaudo
      else if(!props.aditionalInfo.detalles.state.checkedA && !props.aditionalInfo.detalles.state.checkedB &&  props.valorRecaudo !== ""){
        //el valor de la guia debe ser unicamente el valorRecaudo 
        valorTotalQueSeEnviaParaGenerarLaGuia = props.valorRecaudo;
      }
      //3 valor recaudo con paga recaudo
      else if(!props.aditionalInfo.detalles.state.checkedA && props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== ""){
        //el valor de la guia debe ser valor recuado mas flete por recaudo 
        valorTotalQueSeEnviaParaGenerarLaGuia = parseInt(props.valorRecaudo, 10) + parseInt(props.rowSelection.fletePorRecaudo, 10);
      }
      //4 valor recaudo con paga flete
      else if(props.aditionalInfo.detalles.state.checkedA && !props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== "" ){
        //el valor de la guia debe ser el valorRecaudo mas flete por kilo y manejo, el flete por recaudo se le cobra al usuario de la app
        valorTotalQueSeEnviaParaGenerarLaGuia = parseInt(props.valorRecaudo, 10) + parseInt(props.rowSelection.fletePorKilo, 10) + parseInt(props.rowSelection.manejo);

      }
      //5 valor recaudo con paga recaudo y paga flete
      else if(props.aditionalInfo.detalles.state.checkedA && props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo !== ""){
        //el valor de la guia debe ser la suma de todos los valores porque el destinatario paga todo y el usuario nada
        valorTotalQueSeEnviaParaGenerarLaGuia = parseInt(props.valorRecaudo, 10) + parseInt(props.rowSelection.fletePorKilo, 10) + parseInt(props.rowSelection.manejo) + parseInt(props.rowSelection.fletePorRecaudo, 10);

      }
      //6 sin valor recaudo y paga flete
      else if(props.aditionalInfo.detalles.state.checkedA && !props.aditionalInfo.detalles.state.checkedB && props.valorRecaudo === ""){
        valorTotalQueSeEnviaParaGenerarLaGuia = parseInt(props.rowSelection.fletePorKilo, 10) + parseInt(props.rowSelection.manejo) + parseInt(props.rowSelection.fletePorRecaudo, 10);

      }
    

        generacionGuiaBody = {
          tipo: 'generarGuia',
          token: resp.data.token,
          idempresa: 6817,
          origen: props.rowSelection.origen,
          dsdirre: state.direccion,
          dsbarrioo: state.barrio,
          destino: props.rowSelection.destino,
          dsdir: destinatario.direccion,
          dsbarrio: destinatario.barrio,
          dsnitre: state.nit,
          dstelre: state.fijo,
          dscelularre: state.celular,
          dscorreopre: state.correo,
          dsnit: destinatario.nit,
          dsnombre: state.nombreCompleto,
          dsnombrecompleto: destinatario.nombreCompleto,
          dscorreop: destinatario.correo,
          dstel: destinatario.fijo,
          dscelular: destinatario.celular,
          idtransportador: props.rowSelection.id,
          idalto: props.aditionalInfo.dimensiones.alto,
          idancho: props.aditionalInfo.dimensiones.ancho,
          idlargo: props.aditionalInfo.dimensiones.largo,
          unidades: props.rowSelection.unidades,
          kilos: props.rowSelection.kilos,
          valordeclarado: parseInt(props.rowSelection.valoracion.split('.').join(""), 10),
          dscontenido: props.aditionalInfo.detalles.contenido,
          dscom: "",
          idasumecosto: 0,
          contraentrega: 0,
          valorrecaudo: valorTotalQueSeEnviaParaGenerarLaGuia, //props.recaudoCalculadoBool ? props.rowSelection.valorRecaudo : props.valorRecaudo, //si se recalcula valor recaudo o no
          idagente: "4099",
          dsreferencia: "",
          dsordendecompra: "",
          bloquegenerarguia: 1,
          relacion_envios: 1,
          enviarcorreos: 1, 
          guiahija: "",
          accesoila: "",
          cartaporte: ""
        }
          //solicitud a api de aveonline para generar la guia
          const aveonlineGenerarGuia = aveonline('https://aveonline.co/api/nal/v1.0/generarGuiaTransporteNacional.php', generacionGuiaBody);
          aveonlineGenerarGuia.then(answer=>{
            setSpinner(false);
            if(Object.keys(answer.data.resultado).length > 0) {
              const numeroFacturaGuia = answer.data.resultado.guia.numguia;
              const resultado = answer.data.resultado.guia;
              const obtenerHoraActual = registrarHoraActual();
              const horaActualLegible = obtenerHoraActual.horaActualLegible;
              //total es un fiberNode por lo que se debe extraer el valor 
              let fiberNodeChildrenArray;
              //a veces props.rowSelection.total no tiene props, si es asi entonces no buscar en props.children
              const checkIfPropsExist = 'props' in props.rowSelection.total;
              if(checkIfPropsExist){
                fiberNodeChildrenArray = props.rowSelection.total.props.children; //.props.children; //antes tenia props.children ahora no
              } else {
                fiberNodeChildrenArray = props.rowSelection.total;
              }                         
              let totalValues;
              //revisar si en children del div hay un arreglo o un numero
              if(isNaN(fiberNodeChildrenArray) ){
                if(checkIfPropsExist){
                  fiberNodeChildrenArray = props.rowSelection.total.props.children.props.children; //.props.children.props; //antes tenia props.children ahora no
                } else {
                  fiberNodeChildrenArray = props.rowSelection.total;
                }
                 totalValues = fiberNodeChildrenArray.map(el=>{ //antes era fiberNodeChildrenArray.children.map
                  if(checkIfPropsExist){
                    return el.props.children; //antes tenia props.children ahora no
                  } else {
                    return el;
                  }
                });
              } else {
                totalValues = fiberNodeChildrenArray;
              }              
              let datosCotizacion = props.rowSelection;
              datosCotizacion.total = totalValues;
              datosCotizacion.recaudo = props.valorRecaudo;
              datosCotizacion.origenDestino = datosCotizacion.origen + ' / ' + datosCotizacion.destino;
              //formatear el nombre de transportadora del formato (Envia-nacional) a (Envia) para poder realizar la busqueda en relacion de envios
              datosCotizacion.transportadora = datosCotizacion.transportadora.split("-")[0];
              const data = {
                agentInfo: props.agentInfo,
                datosRemitente: state,
                datosDestinatario: destinatario,
                datosCotizacion: datosCotizacion,
                informacionAdicional: props.aditionalInfo,
                numeroGuia: numeroFacturaGuia,
                horaActual: horaActualLegible,
                horaActualFormatoOriginal: obtenerHoraActual.horaActual,
                rotulo : answer.data.resultado.guia.rotulo,
                rutaGuia : answer.data.resultado.guia.rutaguia,
                estado : 'Generada',
                resultado : resultado
              }
              const newDataArray = [];
              newDataArray.push(data)
              //enviar la info a el servidor y devolver el nuevo objeto de la base de datos
              const options = {
                headers: { credentials: 'include' }
              };
                axios.put(ip, {
                Email: props.userInfo.Email,
                GuiasInfo: newDataArray,
                GuiasInfoRelacion: newDataArray
              }, options)
                .then(function (response) {
                  //en vez de usar crear guias modificar userInfo con la nueva info que llega del servidor
                  store.dispatch(userInfo('set_user_info', response.data.userInfo));
                  store.dispatch(dataInfo('set_data_info', response.data.dataInfo))
                  history.push('/Listado de Guías')
          
                })
                .catch(function (error) {
                  console.log('hubo un error al comunicarse con el servidor local', error.response);
                  setAlert('error');
                  setErrorMessage('Hubo un error al guardar las guias, revisa tu conexión a internet o intenta más tarde.')
                });
            }
            else {
              console.log('no es mayor a 0', answer);
            }
          })
          .catch(err=>{
            console.log('hubo un error al generar la guia', err);
            setAlert('error');
            setErrorMessage('Hubo un error al guardar las guias, revisa tu conexión a internet o intenta más tarde.');
          })
    })
      .catch(err => {
        console.log('hubo un error con la api de autenticacion', err);
      })
  
    }

    


  }

  useEffect(() => {
    setState({ ...state, ...props.agentInfo })
  }, [props.agentInfo])
  useEffect(() => {
    if (state.correo !== undefined && state.correo.length > 0 && state.correo !== "") {

      if (validateEmail(state.correo)) {
        setEmailAlertRemitente(false)
      } else {
        setEmailAlertRemitente(true);
      }
    }

  }, [state.correo])
  useEffect(() => {
    if (destinatario.correo !== undefined && destinatario.correo.length > 0 && destinatario.correo !== "") {

      if (validateEmail(destinatario.correo)) {
        setEmailAlertDestinatario(false)
      } else {
        setEmailAlertDestinatario(true);
      }
    }

  }, [destinatario.correo])
  useEffect(() => {
    const stateKeys = Object.keys(state);
    if (stateKeys.length >= 7 && state.nombreCompleto.length > 0 &&
      state.direccion.length > 0 &&
      state.correo.length > 0 &&
      state.ciudadRemitente.length > 0 &&
      state.fijo.length > 0 &&
      state.celular.length > 0 &&
      state.nit.length > 0 &&
      state.barrio.length > 0 &&
      validateEmail(state.correo) &&
      destinatario.nombreCompleto.length > 0 &&
      destinatario.direccion.length > 0 &&
      destinatario.correo.length > 0 &&
      destinatario.ciudadDestinatario.length > 0 &&
      destinatario.fijo.length > 0 &&
      destinatario.celular.length > 0 &&
      destinatario.barrio.length > 0 &&
      validateEmail(destinatario.correo) &&
      props.rowSelection !== undefined) {
      setDisableButton(false)

    } else {
      setDisableButton(true);
    }
  }, [state, destinatario, props.rowSelection]);
 
  let correoRemitenteDiv, correoDestinatarioDiv;
  if (!emailAlertRemitente) {
    correoRemitenteDiv =
      <TextField required autoComplete='email' id="emailRemitente" value={state.correo}
        onChange={ev => setState({ ...state, correo: ev.target.value })}
        label="Correo" variant="filled" className={classes.control} />;
  }
  if (emailAlertRemitente) {
    correoRemitenteDiv = <TextField required error autoComplete='email' id="emailRemitente"
      value={state.correo} onChange={ev => setState({ ...state, correo: ev.target.value })}
      label="Correo" variant="filled" className={classes.control}
      helperText="El correo está en un formato incorrecto."
    />;
  }
  if (!emailAlertDestinatario) {
    correoDestinatarioDiv =
      <TextField required autoComplete='email' id="emailDestinatario" value={destinatario.correo}
        label="Correo" variant="filled" className={classes.control}
        onChange={ev => setDestinatario({ ...destinatario, correo: ev.target.value })} />
  }
  if (emailAlertDestinatario) {
    correoDestinatarioDiv =
      <TextField required error autoComplete='email' id="emailDestinatario" value={destinatario.correo}
        onChange={ev => setDestinatario({ ...destinatario, correo: ev.target.value })}
        label="Correo" variant="filled" className={classes.control}
        helperText="El correo está en un formato incorrecto." />;
  }
  let errorDiv; 
  if (alert === 'error') {
   errorDiv = <Alert severity="error">
     {errorMessage}
     </Alert>

 }
  return (
    <div id="elid">
      <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
        <Typography variant="h6" gutterBottom>
          3. Datos del Remitente
      </Typography>
        <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
          <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
            <TextField required value={state.nombreCompleto} onChange={ev => setState({ ...state, nombreCompleto: ev.target.value })} label="Nombre Completo" variant="filled" className={classes.control} />
            <TextField required value={state.direccion} onChange={ev => setState({ ...state, direccion: ev.target.value })} label="Dirección" variant="filled" className={classes.control} />
            {correoRemitenteDiv}
            <TextField required disabled value={state.ciudadRemitente} label="Ciudad Remitente" variant="filled" className={classes.control} />


          </Grid>
          <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
            <TextField value={state.fijo} onChange={ev => setState({ ...state, fijo: ev.target.value })} label="Teléfono Fijo" type='number' variant="filled" className={classes.control} />
            <TextField required value={state.celular} onChange={ev => setState({ ...state, celular: ev.target.value })} label="Celular" type='number' variant="filled" className={classes.control} />
            <TextField required value={state.nit} onChange={ev => setState({ ...state, nit: ev.target.value })} label="C.C / Nit" type='number' variant="filled" className={classes.control} />
            <TextField required value={state.barrio} onChange={ev => setState({ ...state, barrio: ev.target.value })} label="Barrio" variant="filled" className={classes.control} />
            <TextField value={state.ordenDeCompra} onChange={ev => setState({ ...state, ordenDeCompra: ev.target.value })} label="Orden de Compra" variant="filled" className={classes.control} />
          </Grid>

          <Grid container direction='column' justify='center' alignItems='center' className={classes.gridWidth} >
            <Typography variant="h6" gutterBottom>
              4. Datos del Destinatario
      </Typography>
            <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
              <TextField required value={destinatario.nombreCompleto} onChange={ev => setDestinatario({ ...destinatario, nombreCompleto: ev.target.value })} label="Nombre Completo" variant="filled" className={classes.control} />
              <TextField required value={destinatario.direccion} onChange={ev => setDestinatario({ ...destinatario, direccion: ev.target.value })} label="Direccion" variant="filled" className={classes.control} />
              <TextField value={destinatario.referencia} onChange={ev => setDestinatario({ ...destinatario, referencia: ev.target.value })} label="Referencia Dirección" variant="filled" className={classes.control} />

              {correoDestinatarioDiv}
              <TextField required disabled value={destinatario.ciudadDestinatario} onChange={ev => setDestinatario({ ...destinatario, ciudadDestinatario: ev.target.value })} label="Ciudad Destinatario" variant="filled" className={classes.control} />


            </Grid>
            <Grid container justify='center' alignItems='center' className={classes.gridWidth}>
              <TextField value={destinatario.fijo} onChange={ev => setDestinatario({ ...destinatario, fijo: ev.target.value })} label="Teléfono Fijo" type='number' variant="filled" className={classes.control} />
              <TextField required value={destinatario.celular} onChange={ev => setDestinatario({ ...destinatario, celular: ev.target.value })} label="Celular" type='number' variant="filled" className={classes.control} />
              <TextField required value={destinatario.barrio} onChange={ev => setDestinatario({ ...destinatario, barrio: ev.target.value })} label="Barrio" variant="filled" className={classes.control} />
              <TextField  value={destinatario.nit} onChange={ev => setDestinatario({ ...destinatario, nit: ev.target.value })} label="C.C / Nit" type='number' variant="filled" className={classes.control} />

            </Grid>
          </Grid>
          <Grid container justify='center' alignItems='center' >

            <Button justify='center' alignItems='center' disabled={disableButton} variant="contained" color="primary" onClick={handleClick} className={classes.buttonStyles}>
              Generar Guía
</Button>
          </Grid>
        </Grid>
      </Grid>
      {spinner ? <Spinner /> : null}
      <div id="spinnerId" style={{marginTop : 30}}></div>
      <div id='datosRemitenteViewId'></div>
      {errorDiv}
    </div>
  )
}

function mapStateToProps(state) {
  return {
    userInfo: state.setUserInfo.userInfo
  };
};
export default connect(mapStateToProps)(DatosRemitenteDestinatario)
