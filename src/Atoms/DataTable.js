import React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { makeStyles } from '@material-ui/core/styles';
import './DataTable.css';

/**
 * '& .MuiDataGrid-colCellWrapper.scroll' : {
      minWidth: '100%!important'
    }
 */
const useStyles = makeStyles({
  root: {
    fontSize : 'small',
    '& .MuiDataGrid-colCell-draggable' :{
      width : 'auto'

    }
    
  },
  
});
    const widths = {
      minWidth : 40,
      medianWidth : 90,
      maxWidths : 140,
      maxHiguerWidths : 180,
    }

   const DataTable = (props)=> {
    const classes = useStyles();
    const columns = [
        /*{ field: 'id', headerName: 'ID', width: widths.minWidth },*/
        { field: 'transportadora', headerName: 'TRANSP', width: widths.medianWidth },
        { field: 'origenDestino', headerName: 'ORIGEN/DESTINO', width: widths.maxHiguerWidths },
        {
          field: 'unidades',
          headerName: 'UNID',
          type: 'number',
          width: widths.medianWidth,
        },
        {
          field: 'kilos',
          headerName: 'KILOS',
          type : 'number',
          sortable: false,
          width: widths.medianWidth,/*
          valueGetter: (params) =>
            `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,*/
        },
        {
            field: 'valoracion',
            headerName: 'VALORACIÓN',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'tipoEnvio',
            headerName: 'TIPO DE ENVÍO',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'fletePorKilo',
            headerName: 'FLETE X KILO',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'manejo',
            headerName: 'MANEJO',
            type: 'number',
            width: widths.maxWidths,
          },
          {
            field: 'fletePorRecuado',
            headerName: 'FLETE X RECAUDO',
            type: 'number',
            width: widths.maxHiguerWidths,
          },
          {
            field: 'total',
            headerName: 'TOTAL',
            type: 'number',
            width: widths.maxWidths,
          }
      ];
      
      const rows = [
        { id: 1, transportadora: 'Snow', origenDestino: 'Jon', unidades: 35, kilos : 1, 
        valoracion : 140000, tipoEnvio : 'Nacional', fletePorKilo : 13500, manejo : 1000, 
        fletePorRecuado : 10000, total : 40000  },
    
      ];
      
    return (
      <div style={{ height: 400, width: '100%' }}>
       
        <DataGrid rows={rows} columns={columns} pageSize={5} checkboxSelection className={classes.root}
        />
      </div>
    );
  }
  

  
  export default DataTable;

  