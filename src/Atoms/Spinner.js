import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
   marginTop : 20
  },
}));

export default function CircularIndeterminate() {
  const classes = useStyles();

  return (

    <Grid container justify="center" className={classes.root}>
      <CircularProgress />
      <div id="spinnerId" style={{marginTop : 20}}></div>
    </Grid>
  );
}
