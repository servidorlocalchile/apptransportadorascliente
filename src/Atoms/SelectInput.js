import React, {useState, useEffect} from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import { uuid } from 'uuidv4';
import {connect} from 'react-redux';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 120,
    paddingRight: theme.spacing(1),
    marginLeft: '8px',
    marginBottom : '8px'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  headerStyles : {
      marginTop : '8px',
  },
  iconFilled : {
      right : '0px'
  }
}));
  const SelectInput = (props) =>{
  const classes = useStyles();
  const [agent, setAgent] = useState([
  ]);
  const [selected, setSelected] = useState('')

  const handleChange = (event) => {
    setSelected(event.target.value);
    const agentSelected = agent.filter(el=>el.nombreCompleto === event.target.value);
    props.selectInputChange(agentSelected[0]);
    };
  useEffect(()=>{
    //did mount
    setAgent(props.agentes)
  },[]);
useEffect(()=>{
 //los agentes cambiaron
 setAgent(props.agentes)
}, [props.agentes]);
  let menuItems = [];
  if(agent.length > 0){
    //ya se cargaron los agentes desde el store
   menuItems = agent.map(el=><MenuItem key={uuid()} value={el.nombreCompleto}>{el.nombreCompleto}</MenuItem>)
  }
  return (
      <FormControl variant="filled" className={classes.formControl} >
        <InputLabel id="demo-simple-select-filled-label">Seleccionar Agente</InputLabel>
        <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          value={selected}
          onChange={handleChange}
          className={classes.headerStyles}
        >
              <MenuItem value=''>
            <em>Sin Agente</em>
          </MenuItem>
          {menuItems}
        </Select>

      </FormControl>
        );
}


function mapStateToProps(state) { 
  return {
     agentes : state.setUserInfo.userInfo.Agentes
  };
};
export default connect(mapStateToProps)(SelectInput)

