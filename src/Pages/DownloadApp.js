import React from 'react';
import {Link} from "react-router-dom";
const DownloadApp = props=>{
    return (
        <Link to="/files/external.apk" target="_blank" download>Download</Link>
    )
}

export default DownloadApp;