import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import Copyright from '../Atoms/Copyright';
import {validateEmail} from '../Utils/useFullFunctions';
let ip = '';
 if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  // dev code
  ip = 'https://localhost/registro';
} else {
  // production code
  ip = `${process.env.REACT_APP_URL}/registro`;
  //;

}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignUp() {
  const classes = useStyles();
  const [data, setData] = useState({
    nombre: '',
    apellido: '',
    email : '',
    celular : '',
    password : ''
});
const [alert, setAlert] = useState('empty');
const [alertMessage, setAlertMessage] = useState(<div></div>)
const [disable, setDisable] = useState(true);
const [emailAlert, setEmailAlert] = useState(false);
useEffect(()=>{
 
  if(data.nombre.length > 0 && data.apellido.length > 0 && data.email.length > 0
    && data.celular.length > 0 && data.password.length > 0 && validateEmail(data.email)){
      setDisable(false);
  } else {
    setDisable(true);
  }
}, [data])
useEffect(()=>{
  if(data.email.length > 0 && data.email !== ""){
    
    if(validateEmail(data.email)){
      setEmailAlert(false)
    } else {
      setEmailAlert(true);
    }
  }
  
},[data.email])
/**
     * Registrar el usuario en la base de datos si no existe */
  const handleSubmit = (event) => {
    event.preventDefault()
    const options = {
      headers: {credentials: 'include'}
    };
    axios.post(ip, {
      Nombre: data.nombre,
      Apellido: data.apellido,
      Email : data.email,
      Celular : data.celular,
      Password: data.password,
      GuiasInfo : [],
      GuiasInfoRelacion : [],
      Agentes : [],
      CuentaHabilitada : false
      
    }, options)
    .then(function (response) {
      setAlert('success');
      setAlertMessage(<Alert severity="success">{response.data}</Alert>);
      
    })
    .catch(function (error) {
      if(error.message === "Network Error"){
        setAlert('error');
        setAlertMessage(<Alert severity="error">El servidor está desconectado</Alert>);
        return
      }
      setAlert('error');
      setAlertMessage(<Alert severity="error">
        {error.response.data}. Status Code : {error.response.status}
      </Alert>);
      
    });
  }
  const handleChange = (event) => {
   setData({
       ...data,
       [event.target.name] : event.target.value
   })
}
let formDisplay = null;
if(alert === 'empty' || alert === 'error'){
  const button = disable ?
<Button
type="submit"
fullWidth
variant="contained"
color="primary"
className={classes.submit}
disabled

>
Registrarse
</Button> : 
<Button
type="submit"
fullWidth
variant="contained"
color="primary"
className={classes.submit}

>
Registrarse
</Button>;
const emailDiv = emailAlert ?
<TextField
        error
        autoComplete='email'
        variant="outlined"
        required
        fullWidth
        id="email"
        label="Error"
        name="email"
        onChange={handleChange}
        helperText="El correo está en un formato incorrecto."
      /> :
      <TextField
        autoComplete='email'
        variant="outlined"
        required
        fullWidth
        id="email"
        label="Email"
        name="email"
        onChange={handleChange}


      />;
formDisplay =  <div className={classes.paper}>
<Avatar className={classes.avatar}>
  <LockOutlinedIcon />
</Avatar>
<Typography component="h1" variant="h5">
  Registrarse
</Typography>
<form id='signup' className={classes.form} onSubmit={handleSubmit} noValidate>
  <Grid container spacing={2}>
    <Grid item xs={12} sm={6}>
      <TextField
        autoComplete="given-name"
        name="nombre"
        variant="outlined"
        required
        fullWidth
        id="nombre"
        label="Nombre"
        autoFocus
        onChange={handleChange}
      />
    </Grid>
    <Grid item xs={12} sm={6}>
      <TextField
        autoComplete='family-name'
        variant="outlined"
        required
        fullWidth
        id="apellido"
        label="Apellido"
        name="apellido"
        onChange={handleChange}

      />
    </Grid>
    <Grid item xs={12}>
      {emailDiv}
    </Grid>
    <Grid item xs={12}>
      <TextField
        variant="outlined"
        required
        fullWidth
        id="celular"
        label="Celular"
        name="celular"
        type='number'
        onChange={handleChange}
      />
    </Grid>
    <Grid item xs={12}>
      <TextField
      autoComplete='new-password'
        variant="outlined"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        onChange={handleChange}

      />
    </Grid>
    {/*  <Grid item xs={12}>
      <FormControlLabel
        control={<Checkbox value="allowExtraEmails" color="primary" />}
        label="I want to receive inspiration, marketing promotions and updates via email."
      />
    </Grid>*/}
  </Grid>
 {button}
  

  
</form>
</div>;
} else {
  formDisplay = null;
}

  return (
    <Grid container justify='center'>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
     <Grid container justify="center">
            {formDisplay}
            <Grid item>
           
            {alertMessage}
            </Grid>
          </Grid>
          <Grid container justify="flex-end">
    <Grid item>
      <Link href="ingresar" variant="body2">
      ¿Ya tienes cuenta? Ingresa
      </Link>
    </Grid>
  </Grid>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
    </Grid>
  );
}

export default SignUp;