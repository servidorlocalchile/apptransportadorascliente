import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Inicio from '../Resources/Images/Inicio.jpg';
import Copyright from '../Atoms/Copyright';
import Box from '@material-ui/core/Box';

function Home() {
    return (
      <Grid container justify="center">
            <Grid item>
            <Typography variant="h6" align='center' gutterBottom>
            ¿En tu negocio compras y vendes productos regularmente?
      </Typography>
      <Typography variant="subtitle1" align='center'  gutterBottom>
Hacemos todo el proceso logístico por ti
      </Typography>
      <Grid container justify='center' direction='column' alignItems='center'>
     {<img src={Inicio} width='100%' alt='Inicio Horenvios' />}
      <Box mt={5}>
        <Copyright />
      </Box>
      </Grid>
            </Grid>
          </Grid>
    )
  }
  
  export default Home;