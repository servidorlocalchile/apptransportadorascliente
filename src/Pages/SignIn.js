import React, {useState, useEffect} from 'react';
import axios from 'axios';
import store from '../Redux/store';
import {userInfo, dataInfo} from '../Redux/actions';
import { useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import  {login} from '../Utils/index';
import Copyright from '../Atoms/Copyright';
import {validateEmail} from '../Utils/useFullFunctions';
let ip = '';
let recoverPasswordIp = '';
 if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {  
  // dev code
  ip = 'https://localhost/ingresar';
  recoverPasswordIp = 'https://localhost/recuperar';
} else {
  console.log('prod', process.env.NODE_ENV);
  // production code
  ip = `${process.env.REACT_APP_URL}/ingresar`;
  //;
  recoverPasswordIp = `${process.env.REACT_APP_URL}/recuperar`;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

 function SignIn(props) {
  const classes = useStyles();
  const history = useHistory();
const [alertMessage, setAlertMessage] = useState(<div></div>) 
  const [data, setData] = useState({
    email : '',
    password : ''
});

const [disable, setDisable] = useState(true);
const [emailAlert, setEmailAlert] = useState(false);

useEffect(()=>{
  if(data.email.length > 0 && data.password.length > 0 && validateEmail(data.email)){
    setDisable(false);
    setEmailAlert(false);
  } else {
    setDisable(true);
  }
},[data])
useEffect(()=>{
  if(data.email.length > 0 && data.email !== ""){
    
    if(validateEmail(data.email)){
      setEmailAlert(false);
    } else {
      setEmailAlert(true);

    }
  }
}, [data.email])

  const handleSubmit = (event) => {
    event.preventDefault()
    const options = {
      headers: {credentials: 'include'}
    };

    axios.post(ip, {
      Email : data.email,
      Password: data.password
    }, options)
    .then(function (response) {
     // debugger;
      if(response.data.data !== undefined){
        store.dispatch(userInfo('set_user_info', response.data.userData));
        store.dispatch(dataInfo('set_data_info', response.data.data));
      } else {
        //solo agregar la informaciond e las comisiones a dataInfo
        store.dispatch(userInfo('set_user_info', response.data.userData));
        const comisionsOnly = response.data.comisionsOnly;
        store.dispatch(dataInfo('set_data_info', {comisiones : comisionsOnly}));
      }
      login();
      history.push("/Mi cuenta");
      
    })
    .catch(function (error) {
      if(error.message === "Network Error"){
        setAlertMessage(<Alert severity="error">El servidor está desconectado</Alert>);
        return
      }
      if(error.response.status === 500){
        //el error fue en el servidor
      setAlertMessage(<Alert severity="error">{error.response.statusText} Status: {error.response.status}</Alert>);
      } else {
      setAlertMessage(<Alert severity="error">{error.response.data}</Alert>);
      }
      
    });
  }
  const handleChange = (event) => {
   setData({
       ...data,
       [event.target.name] : event.target.value
   })
}
const recoverPassword = ()=>{
  //send password to the user email
  const options = {
    headers: {credentials: 'include'}
  };
  axios.post(recoverPasswordIp, {
    Email : data.email
  }, options)
  .then(function (response) {
   // debugger;
   console.log(response.data);    
   setAlertMessage(<Alert severity="success">{response.data}</Alert>);

  })
  .catch(function (error) {
    console.log(error);
    console.log(error.message);
    if(error.message === "Network Error"){
      setAlertMessage(<Alert severity="error">El servidor está desconectado</Alert>);
      return
    }
    if(error.response.status === 500 || error.response.status === 400){
      //el error fue en el servidor
    setAlertMessage(<Alert severity="error">{error.response.statusText} Status: {error.response.status}</Alert>);
    } else {
    setAlertMessage(<Alert severity="error">{error.response.data}</Alert>);
    }
    
  });

};
let button, emailTextField;
if(disable){
  button = <Button
type="submit"
fullWidth
variant="contained"
color="primary"
className={classes.submit}
disabled
>
Ingresar
</Button>;
} 
if(!disable){
  button = <Button
type="submit"
fullWidth
variant="contained"
color="primary"
className={classes.submit}
>
Ingresar
</Button>;
}
if(emailAlert){
  emailTextField = 
<TextField
error
variant="outlined"
margin="normal"
required
fullWidth
id="email"
label="Error"
name="email"
autoComplete="email"
autoFocus
onChange={handleChange}
helperText="El correo debe estar en un formato correcto."
/>;
}
if(!emailAlert){
  emailTextField = 
  <TextField
  variant="outlined"
  margin="normal"
  required
  fullWidth
  id="email"
  label="Email"
  name="email"
  autoComplete="email"
  autoFocus
  onChange={handleChange}
  />;
}

  return (
    <Grid container justify='center'>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Ingresar
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit} noValidate>
         {emailTextField}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={handleChange}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Recordarme"
          />
         {button}
          <Grid container>
            <Grid item xs>
              <Link onClick={recoverPassword} style={{cursor : 'pointer'}} variant="body2">
              ¿Olvidaste la contraseña?
              </Link>
            </Grid>
            <Grid item>
              <Link href="registro" variant="body2">
                {'¿No tienes cuenta? Regístrate'}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Grid container justify="center">
            {/*formDisplay*/}
            <Grid item>
           
            {alertMessage}
            </Grid>
          </Grid>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
    </Grid>
  );
}

export default SignIn;